# Introduction
---
This repo holds my programming practices in C.

# Structure
---
The structure for this repo is as follows:

* codechef: this directory contains problems solved in [codechef](https://www.codechef.com)
* kattis: this directory contains problems solved in [kattis](https://open.kattis.com/problems)
* sort_algorithm: this directory contains popular sorting algorithms.
* sort_comparison: this directory contains popular sorting algorithms comparison.
* miscellaneous: this directory contains my experiments or interested topics.

# Convention
---
Here are some conventions I have used in C:

## Variables
---
Variables are named with lower case. For longer variable names, underscores should be used to ease the reading.

**Example:**
```
#!c
int lo_idx;
int hi_idx;
int pivot;
```

## Private Functions
---
Private function are in Pascal case.

**Example:**
```
#!c
PrintIntArr(int *arr, int size) {
    // code
}
```

## Public Functions
---
Pubic functions are lower case. For longer function names, underscores shoudl be used to ease the reading

**Example:**
```
#!c
insert(Node *node, Tree *tree) {
    // code
}

insertion_sort(int *arr, int size) {
    // code
}
```