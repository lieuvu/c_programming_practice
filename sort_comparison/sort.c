/***************************************************************************
 Author: Lieu Vu

 An implementation of API "sort.h". It must be compiled to
 an object file to link with the program compiled from
 file "sort_performance.c"

 Compilation: gcc -Wall -c-o <ouput_name> sort.c
 Example:     gcc -Wall -c -o sort.o sort.c

****************************************************************************/

#include "sort.h"
#include <stdlib.h>
#include <string.h>

#define BASE 10     // the base of calculation

// -----------------------------
// Private functions declaration
// -----------------------------

static void RadixSortHelper(int *, int);
static void CountingSortHelper(int *, int, int);
static void QuickSortHelper(int *, int, int);
static void MergeSortHelper(int *, int, int, int *);
static void MergeIntArr(int *, int, int, int, int *);
static void BuildMaxHeap(int *, int);
static void MaxHeapify(int *, int, int);
static void HeapSortExchange(int *, int, int);

// -------------------------------
// Public functions implementation
// -------------------------------

/**
 Sort an array of integer numbers
 using bublesort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void bubble_sort(int *arr, int size) {
    do {
        int new_size = 0; // the new size

        for (int i = 1; i < size; ++i) {
            if (arr[i] < arr[i-1]) {
                int temp = arr[i];
                arr[i] = arr[i-1];
                arr[i-1] = temp;
                new_size = i;
            }
        }

        size = new_size;
    } while (size > 0);
}

/**
 Sort an array of integer numbers
 using insertionsort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void insertion_sort(int *arr, int size) {
    for (int i = 1; i < size; ++i) {
        for (int j = i; j > 0 && arr[j] < arr[j-1]; --j) {
            int temp = arr[j];
            arr[j] = arr[j-1];
            arr[j-1] = temp;
        }
    }
}

/**
 Sort an array of integer numbers
 using insertionsort optimized algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void insertion_sort_optimized(int *arr, int size) {
    int s_idx = 0;      // smallest item index

    // find the smallest item index
    for (int i = 0; i < size; ++i) {
        if (arr[i] < arr[s_idx]) {
            s_idx = i;
        }
    }

    int sentinel = arr[s_idx];

    // shift all the elements before s_idx to the right
    // by 1 index -> avoid unstable sort
    for (int i = s_idx; i > 0 ; --i) {
        arr[i] = arr[i-1];
    }

    //put sentinel to the begining of the array
    arr[0] = sentinel;

    for (int i = 2; i < size; ++i) {
        int pivot = arr[i];
        int j = i;

        while (arr[j-1] > pivot) {
            arr[j] = arr[j-1];
            j--;
        }

        arr[j] = pivot;
    }
}

/**
 Sort an array of integer numbers
 using shellsort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void shell_sort(int *arr, int size) {
    // 3h + 1 increment sequences: 1, 4, 13, 40, 121, 364, 1093, ...
    int  h = 1;
    while (h < size/3) {
        h =  3*h + 1;
    }

    while (h >= 1) {
        // h-sort the array
        for (int i = h; i < size; ++i) {
            for (int j = i; j >= h && arr[j] < arr[j-h]; j -= h) {
                int temp = arr[j];
                arr[j] = arr[j-h];
                arr[j-h] = temp;
            }
        }
        h /= 3;
    }
}

/**
 Sort an array of integer numbers
 using counting sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void counting_sort(int *arr, int size) {
    int max = arr[0];
    int *count_arr;
    int *temp;

    // find max in the array
    for (int i = 1; i < size; ++i) {
        if (max < arr[i]) {
            max = arr[i];
        }
    }

    // allocate memory for counting array
    count_arr = calloc(max+1, sizeof(*count_arr));

    // allocate memory for temp array
    temp = calloc(size, sizeof(*temp));

    // form count_arr by counting elements in arr
    for (int i = 0; i < size; ++i) {
        count_arr[arr[i]]++;
    }


    // update count_arr so that count_arr[i] contains
    // number of elements less than or equal to i
    for (int i = 1; i < max+1; ++i) {
        count_arr[i] += count_arr[i-1];
    }

    // put the element to temp
    for (int i = size-1; i >= 0; --i) {
        if (count_arr[arr[i]] > 0) {
            temp[count_arr[arr[i]]-1] = arr[i];
            count_arr[arr[i]]--;
        }
    }

    memcpy(arr, temp, sizeof(*arr)*size);

    // free array
    free(temp);
    free(count_arr);
}

/**
 Sort an array of integers numbers
 using radix sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void radix_sort(int *arr, int size) {
    int neg_num_count = 0; // count of negative number in the array

    // count negative number in the array
    for (int i =0; i < size; ++i) {
        if (arr[i] < 0) {
            neg_num_count++;
        }
    }

    // if array has negative numbers
    if (neg_num_count > 0) {
        // allocate memory for negative and positive
        // integer array
        int *neg_arr = malloc(sizeof(*neg_arr) * neg_num_count);
        int *pos_arr = malloc(sizeof(*pos_arr) * size-neg_num_count);

        int neg_idx = 0;    // negative array index
        int pos_idx = 0;    // positive array index

        // put numbers from origin array to negative
        // and positive array. Negative array will hold
        // absolute values of negative numbers
        for (int i = 0; i < size; ++i) {
            if (arr[i] < 0) {
                neg_arr[neg_idx++] = -arr[i];
            } else {
                pos_arr[pos_idx++] = arr[i];
            }
        }

        RadixSortHelper(neg_arr, neg_num_count);
        RadixSortHelper(pos_arr, size-neg_num_count);

        // put back nunbers from negative and positive
        // array to the origin array
        neg_idx = neg_idx-1;   // get the last index of neg_arr
        pos_idx = 0;
        for (int i = 0; i < size; ++i) {
            if (neg_idx >= 0) {
                arr[i] = -neg_arr[neg_idx--];
            } else {
                arr[i] = pos_arr[pos_idx++];
            }
        }

        // free negative and positive
        // dynamic array
        free(neg_arr);
        free(pos_arr);

    } else {
      RadixSortHelper(arr, size);
    }
}

/**
 Sort an array of integer numbers
 using quicksort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void quick_sort(int *arr, int size) {
    QuickSortHelper(arr, 0, size-1);
}

/**
 Sort an array of integer numbers
 using mergesort algorithm.

 @param arr  The array of integer.
 @param size The size of the array.
 */
void merge_sort(int *arr, int size) {
    int *aux = malloc(sizeof(*aux) * size);
    MergeSortHelper(arr, 0, size-1, aux);
    free(aux);
}

/**
 Sort an array of integer numbers
 using heapsort algorithm.

 @param arr  The array of integer.
 @param size The size of the array.
 */
void heap_sort(int *arr, int size) {
    BuildMaxHeap(arr, size);              // construct the max-heap
    while (size > 1) {
        HeapSortExchange(arr, 1, size);   // put the largest element to the end
        size--;                           // update size of array
        MaxHeapify(arr, 1, size);         // maintain the max-heap property
    }
}

// --------------------------------
// Private functions implementation
// --------------------------------

/**
 A helper to radix sort. It will perform
 the real sorting.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
static void RadixSortHelper(int *arr, int size) {
    int max = arr[0];

    // find max in the array to know
    // the number of digits
    for (int i = 1; i < size; ++i) {
        if (max < arr[i]) {
            max = arr[i];
        }
    }

    // perform counting sort for every digits
    for (int exp = 1; max/exp > 0; exp *= BASE) {
        CountingSortHelper(arr, size, exp);
    }
}

/**
 Sort an array of positive integer numbers
 using counting sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 @param exp  The exponent to indiciate the digit number.
 */
static void CountingSortHelper(int *arr, int size, int exp) {
    int count_arr[BASE] = {0};
    int *temp;

    // allocate memory for temp array used to
    // hold sorted elements
    temp = calloc(size, sizeof(*temp));

    // form count_arr by counting elements in arr
    for (int i = 0; i < size; ++i) {
        count_arr[(arr[i]/exp)%BASE]++;
    }

    // update count_arr so that count_arr[i] contains
    // number of elements less than or equal to i
    for (int i = 1; i < BASE; ++i) {
        count_arr[i] += count_arr[i-1];
    }

    // put the element to temp
    for (int i = size-1; i >= 0; --i) {
        if (count_arr[(arr[i]/exp)%BASE] > 0) {
            temp[count_arr[(arr[i]/exp)%BASE]-1] = arr[i];
            count_arr[(arr[i]/exp)%BASE]--;
        }
    }

    // copy temp to arr
    memcpy(arr, temp, sizeof(*arr)*size);

    // free array
    free(temp);
}

/**
 A helper to quicksort. This
 will do the real sort of an array and
 be called recursively.

 @param arr The aray of integer.
 @param lo  The low index of the array.
 @param hi  The hi index of the array.
 */
static void QuickSortHelper(int *arr, int lo, int hi) {
    // an array has one item or low index larger than hi index
    // -> return
    if (lo >= hi) return;

    // an array has more than one item
    if (lo < hi) {
        // choose the pivot as a first item
        int pivot = arr[lo];
        int pivot_idx = lo;
        // find the right index for pivot
        // after putting all less items to the
        // left and all greater items to the right
        for (int i = lo+1; i <= hi; ++i) {
            // if item is less than or equal pivot
            if (arr[i] <= pivot) {
                // increase pivot index
                pivot_idx++;
                // put the item to the pivot index
                int temp = arr[pivot_idx];
                arr[pivot_idx] = arr[i];
                arr[i] = temp;
            }
        }

        // put the pivot to the right pos
        arr[lo] = arr[pivot_idx];
        arr[pivot_idx] = pivot;

        // do the sort recursively for left and right
        // array of the pivot
        QuickSortHelper(arr, lo, pivot_idx-1);
        QuickSortHelper(arr, pivot_idx+1, hi);
    }
}

/**
 A helper to mergersort. It will be called
 recursively

 @param arr The array of integer.
 @param lo  The low index of the array
 @param hi  The hi index onf the array
 @param aux The temporary array used to merge
*/
static void MergeSortHelper(int *arr, int lo, int hi, int *aux) {
    // array has one item or low index
    // greater than hi index -> return
    if (lo >= hi) return;

    // array has more than one item
    if (lo < hi) {
        int mid = lo + (hi - lo) / 2;
        MergeSortHelper(arr, lo, mid, aux);
        MergeSortHelper(arr, mid+1, hi, aux);
        MergeIntArr(arr, lo, hi, mid, aux);
    }
}

/**
 Merge two int arrays that are close
 to each other

 @param arr The aray of integer.
 @param lo  The low index of the two array.
 @param hi  The hi index of the two array.
 @oaram mid The middle index of two arrays
 @para aux  The temporary array used to merge.
 */
static void MergeIntArr(int *arr, int lo, int hi, int mid, int *aux) {
    // two array has one item -> return
    if (lo == hi) return;

    // two array has more than one item
    if (lo < hi) {
        for (int i = lo; i <= hi; ++i) {
            aux[i] = arr[i];
        }

        int l_lo = lo;       // left array lo index
        int r_lo = mid+1;    // right array lo index

        for (int i = lo; i <= hi; ++i) {
            if (l_lo > mid)                    arr[i] = aux[r_lo++];
            else if (r_lo > hi)                arr[i] = aux[l_lo++];
            else if (aux[r_lo] < aux[l_lo])    arr[i] = aux[r_lo++];
            else                               arr[i] = aux[l_lo++];
        }
    }
}

/**
 Build max heap structure of the array.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
static void BuildMaxHeap(int *arr, int size) {
    for (int p = size/2; p >= 1; --p) {
        MaxHeapify(arr, p, size);
    }
}

/**
 Maintain the max-heap property of the array.
 Indices are "off-by-one" to support 1-based
 indexing.

 @param arr  The array of integer.
 @param p    The position of the node.
 @param size The size of the integer array.
 */
static void MaxHeapify(int *arr, int p, int size) {
    while (2*p <= size) {
        int lp = p*2;    // the largest element pos

        // if the largest element is inbound and
        // the left node is less than the right node
        if (lp < size && arr[lp-1] < arr[lp]) {
            lp++;         // update the largest element pos
        }

        // if the node is the largest element
        if (arr[p-1] > arr[lp-1]) {
            break;      // stop maintaining max-heap property
        }

        // if the node is not the largest element
        // exchange it with the largest element
        HeapSortExchange(arr, p, lp);

        p = lp;   // update the current node pos to continue maxheapify
    }
}

/**
 Exchange element at position k with element at
 position j in the array. Indices are "off-by-one"
 to support 1-based indexing. It is used as a helper
 to heap_sort

 @param arr The array of integer.
 @param k   The position of first element.
 @param j   The index of second element.
 */
static void HeapSortExchange(int *arr, int k, int j) {
    int temp = arr[k-1];
    arr[k-1] = arr[j-1];
    arr[j-1] = temp;
}