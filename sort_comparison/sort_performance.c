/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> sort_performance.c <linked>
 Example:     gcc -Wall -o sort_performance sort_performance.c sort.o

 Execution:

 A program to compare sort performance. The program
 needs to link with object file compiled from
 file "sort.c".

 Tested on machine
    OS: Windows 7 64-bit
    Memory: 8 GB
    CPU: Intel(R) Core(TM) i7-4770S

 Note:
    * The array must be positive to test for counting_sort.
    * counting_sort for array which has few extreme large elements compared
      with its size is very poor.
    * radix_sort is a very fast sorting algorithm but it is dependent on
      counting_sort and hence, is not flexible and used for certain types of
      data such as integers. It can not apply to string, double or struct.

 Size <= 1000000   quick_sort is superior
 Size > 10000000   radix_sort and merge_sort are superior
 Size > 60000000   quick_sort ~ shellsort

****************************************************************************/

#include "sort.h"

#include <limits.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int, bool);
static bool isSorted(int *, int);
static void PrintIntArr(int *, int);
static void ClearArr(int *, int);
static void CopyArr(int *, int *, int);
static float Average(float *, int);
static void SortMeasure(void (*)(int *, int), int *, int, int);

// -------------
// Main function
// -------------

int main () {

#if 0
    /* This used to debug */
    const int START_SIZE = 10;      // the size of sequence to test
    const int N = 1;                // number of tests
    const int MEASURE_NUM = 30;     // number of measurment
    const bool HAS_NEGATIVE = true; // the test array has negative numbers

    srand(time(NULL));

    printf("/* Debug */");
    for (int i = 1; i <= N; ++i) {
        int size = START_SIZE * i;
        printf("/*---------------------*/\n");
        printf("Array size: %d\n", size);
        int *test_arr = GenerateIntArray(size, HAS_NEGATIVE);

        printf("Buble Sort:");
        SortMeasure(&bubble_sort, test_arr, size, MEASURE_NUM);

        printf("Insertion Sort:");
        SortMeasure(&insertion_sort, test_arr, size, MEASURE_NUM);

        free(test_arr);
    }

#elif 0
    /* Test all sorting algorithm performance */

    const int START_SIZE = 10000;       // the size of sequence to test
    const int N = 7;                    // number of tests
    const int MEASURE_NUM = 10;         // number of measurment
    const bool HAS_NEGATIVE = false;    // the test array is positive

    srand(time(NULL));

    printf("/* Test all sorting algorithm performance */\n");
    for (int i = 1; i <= N; ++i) {
        int size = START_SIZE * i;
        printf("/*---------------------*/\n");
        printf("Array size: %d\n", size);
        int *test_arr = GenerateIntArray(size, HAS_NEGATIVE);

        printf("Buble Sort\t\t\t:");
        SortMeasure(&bubble_sort, test_arr, size, MEASURE_NUM);

        printf("Insertion Sort\t\t\t:");
        SortMeasure(&insertion_sort, test_arr, size, MEASURE_NUM);

        printf("Insertion Sort Optimized\t:");
        SortMeasure(&insertion_sort_optimized, test_arr, size, MEASURE_NUM);

        printf("Shell Sort\t\t\t:");
        SortMeasure(&shell_sort, test_arr, size, MEASURE_NUM);

        printf("Counting Sort\t\t\t:");
        SortMeasure(&counting_sort, test_arr, size, MEASURE_NUM);

        printf("Quick Sort\t\t\t:");
        SortMeasure(&quick_sort, test_arr, size, MEASURE_NUM);

        printf("Merge Sort\t\t\t:");
        SortMeasure(&merge_sort, test_arr, size, MEASURE_NUM);

        printf("Heap Sort\t\t\t:");
        SortMeasure(&heap_sort, test_arr, size, MEASURE_NUM);

        free(test_arr);
    }

#elif 0
    /* Test sorting algorithm performance except counting sort */

    const int START_SIZE = 10000;       // the size of sequence to test
    const int N = 7;                    // number of tests
    const int MEASURE_NUM = 10;         // number of measurment
    const bool HAS_NEGATIVE = true;    // the test array is positive

    srand(time(NULL));

    printf("/* Test sorting algorithm performance except counting sort */\n");
    for (int i = 1; i <= N; ++i) {
        int size = START_SIZE * i;
        printf("/*---------------------*/\n");
        printf("Array size: %d\n", size);
        int *test_arr = GenerateIntArray(size, HAS_NEGATIVE);

        printf("Buble Sort\t\t\t:");
        SortMeasure(&bubble_sort, test_arr, size, MEASURE_NUM);

        printf("Insertion Sort\t\t\t:");
        SortMeasure(&insertion_sort, test_arr, size, MEASURE_NUM);

        printf("Insertion Sort Optimized\t:");
        SortMeasure(&insertion_sort_optimized, test_arr, size, MEASURE_NUM);

        printf("Shell Sort\t\t\t:");
        SortMeasure(&shell_sort, test_arr, size, MEASURE_NUM);

        printf("Radix Sort\t\t\t:");
        SortMeasure(&radix_sort, test_arr, size, MEASURE_NUM);

        printf("Quick Sort\t\t\t:");
        SortMeasure(&quick_sort, test_arr, size, MEASURE_NUM);

        printf("Merge Sort\t\t\t:");
        SortMeasure(&merge_sort, test_arr, size, MEASURE_NUM);

        printf("Heap Sort\t\t\t:");
        SortMeasure(&heap_sort, test_arr, size, MEASURE_NUM);

        free(test_arr);
    }

#elif 1
    /* Test specific sorting algorithm performance with large sequence */

    const int START_SIZE = 1000000;     // the size of sequence to test
    const int N = 7;                    // number of tests
    const int MEASURE_NUM = 10;         // number of measurment
    const bool HAS_NEGATIVE = false;     // the test array is positive

    srand(time(NULL));

    printf("/* Test specific sorting performance with large sequence */\n");
    for (int i = 1; i <= N; ++i) {
        int size = START_SIZE * i;
        printf("/*---------------------*/\n");
        printf("Array size: %d\n", size);
        int *test_arr = GenerateIntArray(size, HAS_NEGATIVE);

        printf("Shell Sort\t\t\t:");
        SortMeasure(&shell_sort, test_arr, size, MEASURE_NUM);

        printf("Radix Sort\t\t\t:");
        SortMeasure(&radix_sort, test_arr, size, MEASURE_NUM);

        printf("Quick Sort\t\t\t:");
        SortMeasure(&quick_sort, test_arr, size, MEASURE_NUM);

        printf("Merge Sort\t\t\t:");
        SortMeasure(&merge_sort, test_arr, size, MEASURE_NUM);

        printf("Heap Sort\t\t\t:");
        SortMeasure(&heap_sort, test_arr, size, MEASURE_NUM);

        free(test_arr);
    }

#elif 1
    /* Test specific sorting algorithm performance with special sequence */

    const int MEASURE_NUM = 10;         // number of measurment
    const int size = 7;
    int test_arr[7] = {1000000000, 15, 16, 10 ,3, 0, 1};

    printf("/* Test specific sorting algorithm performance with special sequence */\n");
    printf("/*---------------------*/\n");
    printf("Array size: %d\n", size);

    printf("Counting Sort\t\t\t:");
    SortMeasure(&counting_sort, test_arr, size, MEASURE_NUM);

    printf("Quick Sort\t\t\t:");
    SortMeasure(&quick_sort, test_arr, size, MEASURE_NUM);

    printf("Merge Sort\t\t\t:");
    SortMeasure(&merge_sort, test_arr, size, MEASURE_NUM);

    printf("Heap Sort\t\t\t:");
    SortMeasure(&heap_sort, test_arr, size, MEASURE_NUM);

#endif

    return 0;
}

// ----------------------------------
// Implementation of private functions
// ----------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.
 @param has_negative Flag to indicate whether the
 array contains negative number.

 @return The random integer array.
*/
static int * GenerateIntArray(int size, bool has_negative) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1 && has_negative == true) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Clear the array and set all values to zero.

 @param arr The array to clear.
 @param size The size of the array.
*/

static void ClearArr(int *arr, int size) {
    for (int i = 0; i < size; ++i) {
        arr[i] = 0;
    }
}

/**
 Copy content of one array to another array.

 @param arr_src The source array.
 @param arr_des The destination array.
 @param size The size of the array.
*/

static void CopyArr(int *arr_src, int *arr_des, int size) {
    for (int i = 0; i < size; i++) {
        arr_des[i] = arr_src[i];
    }
}

/**
 Take the average of measurements.

 @param measurements The array of measurement.
 @param measure_num The number of measurements.

 @return The average of measurements.
*/
static float Average(float *measurements, int measure_num) {
    float sum = 0;
    for (int i = 0; i < measure_num; ++i) {
        sum += measurements[i];
    }
    return (sum / measure_num);
}

/**
 Measure the sort algorithm performance.

 @param sort_func The sort algorithm to measure.
 @param test_arr The array to sort.
 @param size The size of arrays created during test.
 @param measure_num The number of measurements.
*/
static void SortMeasure(void (*sort_func)(int *, int), int *test_arr, int size,
                 int measure_num) {
    int *test_arr_clone = malloc(sizeof(*test_arr_clone) * size);
    float measurements[measure_num];
    int pass_count = 0;

    for (int i = 0; i < measure_num; ++i) {
        // clear the clone array
        ClearArr(test_arr_clone, size);

        // copy contents from test array to clone array
        CopyArr(test_arr, test_arr_clone, size);

        // measure sorting performance
        clock_t start = clock();
        sort_func(test_arr_clone, size);
        clock_t end = clock();

        // record the mesurement only if the clone
        // array is sorted
        if (isSorted(test_arr_clone, size)) {
            measurements[i] = (float)(end - start) / CLOCKS_PER_SEC * 1000;
            pass_count += 1;
        }
    }

    free(test_arr_clone);

    // if the test failed, do not print the result
    if (pass_count < measure_num) {
        printf("Not passed\n");
        return;
    }

    // print the mesurement results
    printf("\tTime taken: %.2f ms\n", Average(measurements, measure_num));
    // PrintIntArr(test_arr, size); // check if test_arr is the same for all test
}