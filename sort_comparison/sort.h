/***************************************************************************
 Author: Lieu Vu

 API used in sort_performance.c

 ***************************************************************************/

/**
 Sort an array of integer numbers
 using bublesort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void bubble_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using insertionsort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void insertion_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using insertionsort optimized algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void insertion_sort_optimized(int *arr, int size);

/**
 Sort an array of integer numbers
 using shellsort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void shell_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using counting sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void counting_sort(int *arr, int size);

/**
 Sort an array of integers numbers
 using radix sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void radix_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using quicksort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
void quick_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using mergesort algorithm.

 @param arr  The array of integer.
 @param size The size of the array.
 */
void merge_sort(int *arr, int size);

/**
 Sort an array of integer numbers
 using heapsort algorithm.

 @param arr  The array of integer.
 @param size The size of the array.
 */
void heap_sort(int *arr, int size);