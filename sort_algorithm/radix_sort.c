/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> counting_sort.c
 Example:     gcc -Wall -o prog counting_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using radix sort with counting sort as a
 helper

 Complexity
    * Worst-case: O(d(n+k))
      where n is the number of integers.
            k is base.
            d the most digits of intgers.

 Stability:
    * Stable

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#define BASE 10     // base for calculation

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void RadixSort(int *, int);
static void RadixSortHelper(int *, int);
static void CountingSortHelper(int *, int, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // RadixSort(test, 8);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);

        RadixSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);

        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }

        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integers numbers
 using radix sort algorithm.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void RadixSort(int *arr, int size) {
    int neg_num_count = 0; // count of negative number in the array

    // count negative number in the array
    for (int i =0; i < size; ++i) {
        if (arr[i] < 0) {
            neg_num_count++;
        }
    }

    // if array has negative numbers
    if (neg_num_count > 0) {
        // allocate memory for negative and positive
        // integer array
        int *neg_arr = malloc(sizeof(*neg_arr) * neg_num_count);
        int *pos_arr = malloc(sizeof(*pos_arr) * size-neg_num_count);

        int neg_idx = 0;    // negative array index
        int pos_idx = 0;    // positive array index

        // put numbers from origin array to negative
        // and positive array. Negative array will hold
        // absolute values of negative numbers
        for (int i = 0; i < size; ++i) {
            if (arr[i] < 0) {
                neg_arr[neg_idx++] = -arr[i];
            } else {
                pos_arr[pos_idx++] = arr[i];
            }
        }

        RadixSortHelper(neg_arr, neg_num_count);
        RadixSortHelper(pos_arr, size-neg_num_count);

        // put back nunbers from negative and positive
        // array to the origin array
        neg_idx = neg_idx-1;   // get the last index of neg_arr
        pos_idx = 0;
        for (int i = 0; i < size; ++i) {
            if (neg_idx >= 0) {
                arr[i] = -neg_arr[neg_idx--];
            } else {
                arr[i] = pos_arr[pos_idx++];
            }
        }

        // free negative and positive
        // dynamic array
        free(neg_arr);
        free(pos_arr);

    } else {
      RadixSortHelper(arr, size);
    }
}

/**
 A helper to radix sort. It will perform
 the real sorting.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void RadixSortHelper(int *arr, int size) {
    int max = arr[0];

    // find max in the array to know
    // the number of digits
    for (int i = 1; i < size; ++i) {
        if (max < arr[i]) {
            max = arr[i];
        }
    }

    // perform counting sort for every digits
    for (int exp = 1; max/exp > 0; exp *= BASE) {
        CountingSortHelper(arr, size, exp);
    }
}

/**
 Sort an array of positive integer numbers
 using counting sort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 @param exp  The exponent to indiciate the digit number.
 */
static void CountingSortHelper(int *arr, int size, int exp) {
    int count_arr[BASE] = {0};
    int *temp;

    // allocate memory for temp array used to
    // hold sorted elements
    temp = calloc(size, sizeof(*temp));

    // form count_arr by counting elements in arr
    for (int i = 0; i < size; ++i) {
        count_arr[(arr[i]/exp)%BASE]++;
    }

    // update count_arr so that count_arr[i] contains
    // number of elements less than or equal to i
    for (int i = 1; i < BASE; ++i) {
        count_arr[i] += count_arr[i-1];
    }

    // put the element to temp
    for (int i = size-1; i >= 0; --i) {
        if (count_arr[(arr[i]/exp)%BASE] > 0) {
            temp[count_arr[(arr[i]/exp)%BASE]-1] = arr[i];
            count_arr[(arr[i]/exp)%BASE]--;
        }
    }

    // copy temp to arr
    memcpy(arr, temp, sizeof(*arr)*size);

    // free array
    free(temp);
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}