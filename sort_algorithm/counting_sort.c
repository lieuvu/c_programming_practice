/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> counting_sort.c
 Example:     gcc -Wall -o prog counting_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using counting sort. It is good to sort a
 sequence that the max element is trivial to the number of its elements and
 the max element must not be so large. It is often used as helper for
 better sorting such as radix sort.

 Complexity
    * Worst-case: O(n+k)
    * Average: O(n+k)
    * Best-case: O(n+k)
    where k is the max of items' range

 Stability:
    * Stable

 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void CountingSort(int *, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // CountingSort(test, 8);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);

        CountingSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);

        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }

        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        arr[i] = rand() % SHRT_MAX;
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using counting sort algorithm.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void CountingSort(int *arr, int size) {
    int max = arr[0];
    int *count_arr;
    int *temp;

    // find max in the array
    for (int i = 1; i < size; ++i) {
        if (max < arr[i]) {
            max = arr[i];
        }
    }

    // allocate memory for counting array
    count_arr = calloc(max+1, sizeof(*count_arr));

    // allocate memory for temp array used to
    // hold sorted elements
    temp = calloc(size, sizeof(*temp));

    // form count_arr by counting elements in arr
    for (int i = 0; i < size; ++i) {
        count_arr[arr[i]]++;
    }

    // update count_arr so that count_arr[i] contains
    // number of elements less than or equal to i
    for (int i = 1; i < max+1; ++i) {
        count_arr[i] += count_arr[i-1];
    }

    // put the element to temp
    for (int i = size-1; i >= 0; --i) {
        if (count_arr[arr[i]] > 0) {
            temp[count_arr[arr[i]]-1] = arr[i];
            count_arr[arr[i]]--;
        }
    }

    // copy temp to arr
    memcpy(arr, temp, sizeof(*arr)*size);

    // free array
    free(temp);
    free(count_arr);
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}