/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> shell_sort.c
 Example:     gcc -Wall -o prog shell_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using shellsort

 Use increment sequence proposed by Pratt(1971) (1, 4, 13, 40, ...)

 Complexity
    * Worst-case: O(n*(logn)^2)
    * Average: depends on gap sequence
    * Best-case: O(n*logn)

 Stability:
    * Unstable.
    * For example: [(5,"A"),(2,""),(3,""),(5,"B"),(1,""),(7,"")]
    * After sort: [(1,""),(2,""),(3,""),(5,"B"),(5,"A"),(7,"")]

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void ShellSort(int *, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // ShellSort(test, 8);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);
        ShellSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);
        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }
        free(testArr);
    }

    return 0;
}

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using shellsort algorithm.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void ShellSort(int *arr, int size) {
    // 3h + 1 increment sequences: 1, 4, 13, 40, 121, 364, 1093, ...
    int  h = 1;
    while (h < size/3) {
        h =  3*h + 1;
    }

    while (h >= 1) {
        // h-sort the array
        for (int i = h; i < size; ++i) {
            for (int j = i; j >= h && arr[j] < arr[j-h]; j -= h) {
                int temp = arr[j];
                arr[j] = arr[j-h];
                arr[j-h] = temp;
            }
        }
        h /= 3;
    }
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}