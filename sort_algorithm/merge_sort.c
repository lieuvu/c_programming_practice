/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> merge_sort.c
 Example:     gcc -Wall -o prog merge_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using mergesort

 Complexity
    * Worst-case: O(n*logn)
    * Average: O(n*logn)
    * Best-case: O(n*logn) or O(n)

 Stability:
    * Stable

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void MergeSort(int *, int);
static void MergeSortHelper(int *, int, int, int *);
static void MergeIntArr(int *, int, int, int, int *);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // int aux[8];
    // MergeSort(test, 0, 7, aux);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);
        MergeSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);
        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }
        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using mergesort algorithm.

 @param arr The array of integer.
 @param size The size of the array.
 */
static void MergeSort(int *arr, int size) {
    int *aux = malloc(sizeof(*aux) * size);
    MergeSortHelper(arr, 0, size-1, aux);
    free(aux);
}

/**
 A helper to mergersort. It will be called
 recursively

 @param arr The array of integer.
 @param lo The low index of the array
 @param hi The hi index onf the array
 @param aux The temporary array used to merge
*/
static void MergeSortHelper(int *arr, int lo, int hi, int *aux) {
    // array has one item or low index
    // greater than hi index -> return
    if (lo >= hi) return;

    // array has more than one item
    if (lo < hi) {
        int mid = lo + (hi - lo) / 2;
        MergeSortHelper(arr, lo, mid, aux);
        MergeSortHelper(arr, mid+1, hi, aux);
        MergeIntArr(arr, lo, hi, mid, aux);
    }
}

/**
 Merge two int arrays that are close
 to each other

 @param arr The aray of integer.
 @param lo The low index of the two array.
 @param hi The hi index of the two array.
 @oaram mid The middle index of two arrays
 @para aux The temporary array used to merge.
 */
static void MergeIntArr(int *arr, int lo, int hi, int mid, int *aux) {
    // two array has one item -> return
    if (lo == hi) return;

    // two array has more than one item
    if (lo < hi) {
        for (int i = lo; i <= hi; ++i) {
            aux[i] = arr[i];
        }

        int l_lo = lo;       // left array lo index
        int r_lo = mid+1;    // right array lo index

        for (int i = lo; i <= hi; ++i) {
            if (l_lo > mid)                    arr[i] = aux[r_lo++];
            else if (r_lo > hi)                arr[i] = aux[l_lo++];
            else if (aux[r_lo] < aux[l_lo])    arr[i] = aux[r_lo++];
            else                               arr[i] = aux[l_lo++];
        }
    }
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size -1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}