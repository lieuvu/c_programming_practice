/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> heap_sort.c
 Example:     gcc -Wall -o prog heap_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using bubblesort

 Complexity
    * Worst-case: O(nlogn)
    * Average:  O(nlogn)
    * Best-case: O(nlogn)

 Stability:
    * Stable

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void HeapSort(int *, int);
static void BuildMaxHeap(int *, int);
static void MaxHeapify(int *, int, int);
static void Exchange(int *, int, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[10] = {4,1,3,2,16,9,10,14,8,7};
    // HeapSort(test, 10);
    // PrintIntArr(test, 10);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);

        HeapSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);

        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }

        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using heapsort algorithm.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
static void HeapSort(int *arr, int size) {
    BuildMaxHeap(arr, size);        // construct the max-heap
    while (size > 1) {
        Exchange(arr, 1, size);     // put the largest element to the end
        size--;                     // update size of array
        MaxHeapify(arr, 1, size);   // maintain the max-heap property
    }
}

/**
 Build max heap structure of the array.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
static void BuildMaxHeap(int *arr, int size) {
    for (int p = size/2; p >= 1; --p) {
        MaxHeapify(arr, p, size);
    }
}

/**
 Maintain the max-heap property of the array.
 Indices are "off-by-one" to support 1-based
 indexing.

 @param arr  The array of integer.
 @param p    The position of the node.
 @param size The size of the integer array.
 */
static void MaxHeapify(int *arr, int p, int size) {
    while (2*p <= size) {
        int lp = p*2;    // the largest element pos

        // if the largest element is inbound and
        // the left node is less than the right node
        if (lp < size && arr[lp-1] < arr[lp]) {
            lp++;         // update the largest element pos
        }

        // if the node is the largest element
        if (arr[p-1] > arr[lp-1]) {
            break;      // stop maintaining max-heap property
        }

        // if the node is not the largest element
        // exchange it with the largest element
        Exchange(arr, p, lp);

        p = lp;   // update the current node pos to continue maxheapify
    }
}

/**
 Exchange element at position k with element at
 position j in the array. Indices are "off-by-one"
 to support 1-based indexing.

 @param arr The array of integer.
 @param k   The position of first element.
 @param j   The index of second element.
 */
static void Exchange(int *arr, int k, int j) {
    int temp = arr[k-1];
    arr[k-1] = arr[j-1];
    arr[j-1] = temp;
}

/**
 Print integer array.

 @param arr  The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Test if an integer array is sorted.

 @param arr  The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}