/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> quick_sort.c
 Example:     gcc -Wall -o prog quick_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using quicksort

 Complexity:
    * Worst-case: O(n^2)
    * Average: O(n*log(n))
    * Best-case: O(n*logn)

 Stability:
    * Unstable
    * For example: [(5,"A"),(2,""),(3,""),(5,"B"),(1,""),(7,"")]
    * After sort: [(1,""),(2,""),(3,""),(5,"B"),(5,"A"),(7,"")]

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void QuickSort(int *, int);
static void QuickSortHelper(int *, int, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // QuickSort(test, 8);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);
        QuickSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);
        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }
        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using quicksort algorithm.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void QuickSort(int *arr, int size) {
    QuickSortHelper(arr, 0, size-1);
}

/**
 A helper to quicksort. This
 will do the real sort of an array and
 be called recursively.

 @param arr The aray of integer.
 @param lo The low index of the array.
 @param hi The hi index of the array.
 */
static void QuickSortHelper(int *arr, int lo, int hi) {
    // an array has one item or low index larger than hi index
    // -> return
    if (lo >= hi) return;

    // an array has more than one item
    if (lo < hi) {
        // choose the pivot as a first item
        int pivot = arr[lo];
        int pivot_idx = lo;
        // find the right index for pivot
        // after putting all less items to the
        // left and all greater items to the right
        for (int i = lo+1; i <= hi; ++i) {
            // if item is less than pivot
            if (arr[i] <= pivot) {
                // increase pivot index
                pivot_idx++;
                // put the item to the pivot index
                int temp = arr[pivot_idx];
                arr[pivot_idx] = arr[i];
                arr[i] = temp;
            }
        }

        // put the pivot to the right pos
        arr[lo] = arr[pivot_idx];
        arr[pivot_idx] = pivot;

        // do the sort recursively for left and right
        // array of the pivot
        QuickSortHelper(arr, lo, pivot_idx-1);
        QuickSortHelper(arr, pivot_idx+1, hi);
    }
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size -1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}


/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}