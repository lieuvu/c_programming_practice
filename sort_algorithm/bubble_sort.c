/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <ouput_name> buble_sort.c
 Example:     gcc -Wall -o prog buble_sort.c

 Execution: <output_name>
 Example: prog

 Sorts a sequence of integer using bubblesort

 Complexity
    * Worst-case: O(n^2)
    * Average: O(n^2)
    * Best-case: O(n)

 Stability:
    * Stable

 ***************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

// --------------------------------
// Declaration of private functions
// --------------------------------

static int * GenerateIntArray(int);
static void BubbleSort(int *, int);
static void PrintIntArr(int *, int);
static bool isSorted(int *, int);

// -------------
// Main function
// -------------

int main () {
    const int SIZE = 10;     // size of array to test
    const int N = 7;        // number of tests

    srand(time(NULL));

    // int test[8] = {8,9,2,1,3,5,1,3};
    // BubbleSort(test, 8);
    // PrintIntArr(test, 8);

    for (int i = 0; i < N; ++i) {
        int *testArr = GenerateIntArray(SIZE);

        BubbleSort(testArr, SIZE);
        PrintIntArr(testArr, SIZE);

        if (isSorted(testArr, SIZE)) {
            printf("Pass!\n");
        } else {
            printf("Not Pass!\n");
        }

        free(testArr);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
//------------------------------------

/**
 Generate random array of integer with
 given size.

 @param size The size of the array.

 @return The random integer array.
 */
static int * GenerateIntArray(int size) {
    int *arr = malloc(sizeof(*arr) * size);

    for (int i = 0; i < size; ++i) {
        int rand_n = rand();
        if (rand_n % 2 == 1) {
            arr[i] = -(rand_n % SHRT_MAX);
        } else {
            arr[i] = rand_n % SHRT_MAX;
        }
    }

    return arr;
}

/**
 Sort an array of integer numbers
 using bublesort algorithm.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void BubbleSort(int *arr, int size) {
    do {
        int new_size = 0; // the new size

        for (int i = 1; i < size; ++i) {
            if (arr[i] < arr[i-1]) {
                int temp = arr[i];
                arr[i] = arr[i-1];
                arr[i-1] = temp;
                new_size = i;
            }
        }

        size = new_size;
    } while (size > 0);
}

/**
 Print integer array.

 @param arr The array of integer.
 @param size The size of the integer array.
 */
static void PrintIntArr(int *arr, int size) {
    printf("[");
    for (int i = 0; i < size; ++i) {
        if (i < size-1) {
         printf("%d,", arr[i]);
        } else {
            printf("%d", arr[i]);
        }
    }
    printf("]\n");
}

/**
 Test if an integer array is sorted.

 @param arr The array of integer to test.
 @param size The size of the array.

 @return true if the array is sorted, otherwise false.
 */
static bool isSorted(int *arr, int size) {
    for (int i = 0; i < size-1; ++i) {
        if (arr[i] > arr[i+1]) return false;
    }

    return true;
}