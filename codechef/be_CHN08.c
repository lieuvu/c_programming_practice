/***************************************************************************
 Author: Lieu vu

 Compilation: gcc -Wall -o <output_file> be_CHN08.c
 Example:     gcc -Wall -o prog be_CHN08.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: codechef

 Malvika was getting bored of the usual Fibonacci problems, and decided to
 add a little twist to it. She defined a new function f() with the following
 properties:

 She'll give you two integers, A and B. f(1) is defined to be A and f(2) i
 B. And for all integers x ≥ 2, f(x) = f(x-1) + f(x+1).

 She'll give an integer N, and you have to find out what f(N) is. Output the
 answers modulo 10^9+7.

 Input:
    * The first line of input contains a single integer T denoting number of
    test cases.
    * The only line of each test case contains three integers: A, B and N,
    denoting f(1), f(2) and the query.


 Output:
    * For each test case, output a line which contains a single integer,
    corresponding to f(N) for the given input.

 Constraints:
    1 ≤ T ≤ 10^5
    -10^9 ≤ A , B ≤ 10^9
    1 ≤ N ≤ 10^9

 Example:
    Input:
    2
    10 17 3
    23 17 3


    Output:
    7
    1000000001

 Explanation:
    * In the first test case, f(3) = 7, and so that is the output.
    * In the second test case, f(3) = -6 and the answer modulo 10^9+7 is
    1000000001.

****************************************************************************/

/*****************************************
Solution:
    * We have:
        f(N-1) = f(N-2) + f(N)
        f(N-2) = f(N-3) + f(N-1)
    * Adding two above equations, we have:
        f(N-1) + f(N-2) = f(N-1) + f(N-2)
                          + f(N-3) + f(N)
        =>  f(N) = -f(N-3)
    * Hence, f(4) = -f(1), f(5) = -f(2),
    f(6) = -f(3), ...
    * We conclude:
        f(3*n)   = (-1)^(n)*f(0)   (n>=0)
        f(3*n+1) = (-1)^n*f(1)     (n>=0)
        f(3*n+2) = (-1)^n*f(2)     (n>=0)

*****************************************/

#include <stdio.h>

/* Functions macro */
#define func(x, f) ((x) % 2 == 1) ? (-f) : (f)

int main () {
    const int M = 1000000007;           /* constant 10^9+7 */
    int num_test;                       /* the test numbers */
    int f0;
    int f1;
    int f2;
    int n;
    int fn = 0;
    int result;

    scanf("%d", &num_test);          /* get test numbers */

    while (num_test > 0) {
        scanf("%d %d %d", &f1, &f2, &n);  /* get f1, f2, n */

        if (n >= 0) {
            f0 = f1 - f2;

            switch (n % 3) {
                case 0:
                    fn = func(n/3, f0);
                    break;
                case 1:
                    fn = func(n/3, f1);
                    break;
                case 2:
                    fn = func(n/3, f2);
                    break;
            }
        }

        result = fn % M;

        if (result < 0) {
            printf("%u\n", result + M);
        } else {
            printf("%u\n", result);
        }

        num_test -= 1;  /* decrement test case by 1 */
    }

    return 0;
}