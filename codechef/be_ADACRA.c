/***************************************************************************
 Author: Lieu vu

 Compilation: gcc -Wall -o <output_file> be_ADACRA.c
 Example:     gcc -Wall -o prog be_ADACRA.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: codechef

 Ada has an array of N crayons, some crayons are pointing upwards and
 some downwards. Ada thinks that an array of crayons is beautiful if all
 the crayons are pointing in the same direction.

 In one step you can flip any segment of consecutive crayons. After flipping
 a segment, all crayons pointing downwards will point upwards and visce
 versa.

 What is the minimum number of steps to make the array of crayons beautiful?

 Input:
    * The first line of the input contains T the number of test cases. Each
    test case is described in one line containing a string S of N characters,
    thE i-th character is 'U' if the i-th crayon is pointing upwards and 'D'
    if it is pointing downwards.

 Output:
    * For each test case, output a single line containing the minimum number
    of flips needed to make all crayons point to the same direction.

 Constraints:
    1 ≤ T ≤ 3000
    1 ≤ N ≤ 50


 Example:
    Input:
    1
    UUDDDUUU

    Output:
    1

 Explanation:
    * Example case 1. In one step we can flip all the crayons pointing
    downwards

****************************************************************************/

#include <stdio.h>
#include <string.h>

int main () {
    const int STR_LENGTH = 55;      /* string length */
    int num_test;                   /* the test numbers */

    scanf("%d", &num_test);      /* get test numbers */

    while (num_test > 0) {
        char str[STR_LENGTH];
        int i;
        int flipUp = 0;         /* number of flips up */
        int flipDown = 0;      /* number of flip down */

        scanf("%s", str);   /* get the string */

        for (i = 0; i < strlen(str); ++i) {

            /* when char 'U' to 'D' or 'U' to '\0'
               increase segment 'U' */
            if (str[i] == 'U') {
                if (str[i+1] == 'D' || str[i+1] == '\0') {
                    flipUp++;
                }
            }

            /* when char 'D' to 'U' or 'D' to '\0'
               increase segment 'D' */
            if (str[i] == 'D') {
                if (str[i+1] == 'U' || str[i+1] == '\0') {
                    flipDown++;
                }
            }
        }

        if (flipUp <= flipDown) {
            printf("%d\n", flipUp);
        } else {
            printf("%d\n", flipDown);
        }

        num_test -= 1;  /* decrement test case by 1 */
    }

    return 0;
}