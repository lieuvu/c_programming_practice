/***************************************************************************
 Author: Lieu vu

 Compilation: gcc -Wall -o <output_file> template.c
 Example:     gcc -Wall -o prog template.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: codechef

 All submissions for this problem are available.

 An array a is called beautiful if for every pair of numbers ai, aj,
 (i ≠ j), there exists an ak such that ak = ai * aj. Note that k can be
 equal to i or j too.

 Find out whether the given array a is beautiful or not!

 Input:
    * First line of the input contains an integer T denoting the number
    of test cases. T test cases follow.
    * First line of each test case contains an integer n denoting number
    of elements in a.
    * Next line contains n space separated integers denoting the array a.

 Output:
    * For each test case, output a single line containing "yes" or "no"
    (without quotes) corresponding to the answer of the problem.

 Constraints:
    1 ≤ T ≤ 10^6
    1 ≤ n ≤ 10^5
    Sum of n over all the test cases ≤ 106
    -10^9 ≤ ai ≤ 10^9

 Example:
    Input:
    3
    2
    0 1
    2
    1 2
    2
    5 6

    Output:
    yes
    yes
    no


 Explanation:
    * Test case 1. If you multiply 0 with 1, you get 0, we see that a0 = 0.
    So, the array is beautiful.
    * Test case 3. If you multiply 5 with 6, you get 30, there does not exist
    an k such that ak = 30. So, the array is not beautiful.
****************************************************************************/

/****************************************************************************
 Discussion:
    * We only need to check the array for 0, 1, -1.
    * If the array has size of 1 => array is beautiful (vacuous truth).
    * If the array has number -1
        * the array is beautiful if it does not accept any number whose
        absolute value is larger than 1.
        * if the array has more than one number -1, the array is only
        beautiful if it has at least one number 1.
    * If the array does not have -1, the array is beautiful if it
    does not accept more than one number whose absolute value is larger
    than 1.

 ****************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define MAX_COUNT 1   /* max number its absolute value is larger than 1 */

int main () {
    int num_test;               /* num of test case */
    int num_int;                /* number of integers in an array */
    bool is_beautiful;          /* flag for beautiful array */
    int one;
    int minus_one;
    int count;                  /* count */
    int check_num;              /* number to be check */
    int i;

    scanf("%d", &num_test);

    while (num_test) {
        scanf("%d", &num_int);

        is_beautiful = true;         /* suppose the array is beautiful */
        one = 0;                     /* the array does not have 1 */
        minus_one = 0;               /* the array does not have -1 */
        count = 0;

        for (i = 0; i < num_int; ++i) {
            scanf("%d", &check_num);

            /* if number whose absolute value is larger than one */
            if (abs(check_num) > 1) {
                count++;    /* increase the count */
            } else {
                /* count if number is 1 or -1 */
                if (check_num == 1) one++;
                if (check_num == -1) minus_one++;
            }

            /* if has -1 */
            if (minus_one >= 1) {
                /* if has number whose absolute
                   value is larger than 1 */
                if (count == 1) {
                    is_beautiful = false;   /* array is not beautiful */
                } else if (minus_one >= 2 && one < 1 && i == num_int-1) {
                    /* if has two -1 and no 1 */
                    is_beautiful = false;   /* array is not beautiful */
                }
            } else if (count > MAX_COUNT) {
                /* if count is larger than MAX_COUNT
                   array is not beautiful */
                is_beautiful = false;
            }

            /* if array is not beautiful
               read number without checking */
            if (!is_beautiful && i < num_int-1) {
                for(++i; i < num_int; ++i) {
                    scanf("%d", &check_num);
                }
            }
        }

        if (is_beautiful) {
            printf("yes\n");
        } else {
            printf("no\n");
        }

        num_test -= 1;  /* decrement test case by 1 */
    }

    return 0;
}