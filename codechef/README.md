# Introduction
---
This directory holds solved problems in [codechef](https://www.codechef.com).

# Structure
---
Files in this directory are named with the format: `<category>_<problem_code>.c` where

* category: the category of the problem
    * `be`: beginner
    * `ez`: easy
    * `h`: hard
    * `chlg`: challenge
    * `peer`: peer

* problem_code: the problem code in codechef for reference.

**Examples:**
    peer_TEST.c: the problem with peer category and problem code TEST.

**Notes:**
The problem in category beginner may be easy or hard.

