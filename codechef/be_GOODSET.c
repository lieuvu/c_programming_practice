/***************************************************************************
 Author: Lieu vu

 Compilation: gcc -Wall -o <output_file> be_GOODSET.c
 Example:     gcc -Wall -o prog be_GOODSET.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: codechef

 A set of integers is called good if there does not exist three distinct
 elements a, b, c in it such that a + b = c.

 Your task is simple. Just output any good set of n integers. All the
 elements in this set should be distinct and should lie between 1 and 500,
 both inclusive.

 Input:
    * The first line of the input contains an integer T denoting number of
    test cases. The descriptions of T test cases follow.
    * The only line of each test case contains an integer n, denoting the
    size of the needed good set.

 Output:
    * For each test case, output a single line containing n integers
    denoting the elements of the good set, in any order. There can be more
    than one possible good set, and you can output any one of them.

 Constraints:
    1 ≤ T, n ≤ 100

 Subtasks:
    * Subtask #1 (50 points): 1 ≤ T, n ≤ 10
    * Subtask #2 (50 points): original constraints


 Example:
    Input
    5
    1
    2
    3
    4
    5

    Output
    7
    1 2
    1 2 4
    1 2 4 16
    3 2 15 6 10

 Explanation:
    * Example 1 and 2. Any set of size less than or equal to 2 is good by
    definition.
    * Example 3 onwards. For each pair of elements in the set, you can see
    that their sum doesn't exist in the set.

****************************************************************************/

/********************************************************
 Solutions:
    * Each pair of number will elimiate two numbers that
    are their sum or difference in the range. For
    example the sequence has 3, 4. It can not contain
    1 and 7. Therefore, n numbers will need to eliminate
    n!/((n-2)!*2!)*2 numbers. If n is 20, the numbers need
    to be elimimated is 380, which results very few
    available numbers can be chosen.
    * Therefore, it is quite difficult to generate random
    numbers without any patterns. Generating a sequence of
    numbers following certain patterns will reduce the
    elimination since its difference is the same, which
    reduce the amount eliminated numbers by half. The sum
    of some pairs of elements in a sequence will be probably
    equal, which make the elminated amount of numbers even
    less.
    * For example, generate the odd sequence numbers
    2*k+1 (k >= 0), [1,3,5,7,...]. We see that only number
    2 can not be in the sequence and number 8=1+7=3+5 can
    not be in the sequence. 4 numbers but only two numbers
    are dropped out from the available sequence.
    Furthermore, we can be sure that the sum of any two
    numbers (2*k+1)+(2*t+1)=2(k+t)+2 != 2*i+1, will
    not be in the sequence.
    * We can choose any sequence to follow such as
    2*k+1, 3*k+2, 4*k+3,...

*********************************************************/

#include <stdio.h>
#include <stdbool.h>

int main () {
    int num_test;           /* the test numbers */
    int size;               /* the size of sequence */
    int i;

    scanf("%d", &num_test);  /* get test numbers */

    while (num_test > 0) {
        scanf("%d", &size);   /* get the size of sequence */

        /* generate sequence */
        for (i = 0; i < size; ++i) {
           printf("%d ", 5*i+1);
        }
        printf("\n");

        num_test -= 1;  /* decrement test case by 1 */
    }

    return 0;
}