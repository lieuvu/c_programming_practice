/***************************************************************************
 Author: Lieu u

 Compilation: gcc -Wall -o <output_file> tri_REVERSEBINARY.c
 Example:     gcc -Wall -o prog tri_REVERSEBINARY.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Yi has moved to Sweden and now goes to school here. The first years of
 schooling she got in China, and the curricula do not match completely
 in the two countries. Yi likes mathematics, but now... The teacher explains
 the algorithm for subtraction on the board, and Yi is bored. Maybe it is
 possible to perform the same calculations on the numbers corresponding to
 the reversed binary representations of the numbers on the board? Yi dreams
 away and starts constructing a program that reverses the binary
 representation, in her mind. As soon as the lecture ends, she will go home
 and write it on her computer.

 Your task will be to write a program for reversing numbers in binary.
 For instance, the binary representation of 13 is 1101, and reversing it
 gives 1011, which corresponds to number 11.

 Input:
    * The input contains a single line with an integer N, 1≤N≤1000000000.

 Output:
    * Output one line with one integer, the number we get by reversing the
      binary representation of N.

 Example:
    Input:
    13

    Output:
    11

    Input:
    47

    Output:
    61

****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_BIT   32
#define B_BASE    2     // binary base

// --------------------------------
// Declaration of private functions
// --------------------------------

static void DecimalToBinaryArray(int, int *, int *);

// -------------
// Main function
// -------------

int main () {
    int num;
    int binary_arr[MAX_BIT];
    int result = 0;
    int num_bits = 0;

    scanf("%d", &num);

    // fill the array with -1
    for (int i = 0; i < MAX_BIT; ++i) {
        binary_arr[i] = -1;
    }

    DecimalToBinaryArray(num, binary_arr, &num_bits);

    for (int i = 0; i < num_bits; ++i) {
        if (binary_arr[i] == -1) {
            break;
        }
        result += binary_arr[i] * (int)pow(B_BASE, num_bits-i-1);
    }


    printf("%d", result);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Convert a decimal number to a binary array

 @param dec The decimal number.
 @param binary_arr The binary array reprensting the number.
 @param num_bits The number of bits after convesion.
 */
static void DecimalToBinaryArray(int dec, int *binary_arr, int *num_bits) {
    int i = 0;

    while (dec) {
        binary_arr[i] = dec % B_BASE;
        dec = dec / B_BASE;
        i++;
    }

    *num_bits = i;
}