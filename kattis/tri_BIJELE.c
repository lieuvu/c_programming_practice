/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_BIJELE.c
 Example:     gcc -Wall -o prog tri_BIJELE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.2

 Mirko has found an old chessboard and a set of pieces in his attic.
 Unfortunately, the set contains only white pieces, and apparently an
 incorrect number of them. A set of pieces should contain:
    * One king
    * One queen
    * Two rocks
    * Two bishops
    * Two knights
    * Eight pawns

 Mirko would like to know how many pieces of each type he should add or
 remove to make a valid set.

 Input:
    * The input consists of 6 integers on a single line, each between 0
      and 10 (inclusive). The numbers are, in order, the numbers of kings,
      queens, rooks, bishops, knights and pawns in the set Mirko found.

 Output:
    * Output should consist of 6 integers on a single line; the number
      of pieces of each type Mirko should add or remove. If a number is
      positive, Mirko needs to add that many pieces. If a number is
      negative, Mirko needs to remove pieces.

 Example:
    Input:
    0 1 2 2 2 7

    Output:
    1 0 0 0 0 1

    Input.
    2 1 2 1 2 1

    Output:
    -1 0 0 1 0 7

****************************************************************************/

#include <stdio.h>

#define SET_P   6   // number of pieces in set
#define MAX_K   1   // max king
#define MAX_Q   1   // max queen
#define MAX_R   2   // max rock
#define MAX_B   2   // max bishop
#define MAX_KN  2   // max knight
#define MAX_P   8   // max pawn

enum {K, Q, R, B, KN, P};

int main () {
    int check_set[SET_P] = {0};
    int result[SET_P] = {0};

    for (int i = 0; i < SET_P; ++i) {
        scanf("%d", (check_set+i));
        switch (i) {
            case K:
                result[i] = MAX_K - check_set[i];
                break;
            case Q:
                result[i] = MAX_Q - check_set[i];
                break;
            case R:
                result[i] = MAX_R - check_set[i];
                break;
            case B:
                result[i] = MAX_B - check_set[i];
                break;
            case KN:
                result[i] = MAX_KN - check_set[i];
                break;
            case P:
                result[i] = MAX_P - check_set[i];
                break;
        }
    }

    for (int i = 0; i < SET_P; ++i) {
        printf("%d ", result[i]);
    }

    return 0;
}