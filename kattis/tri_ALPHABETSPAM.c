/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_ALPHABETSPAM.c.c
 Example:     gcc -Wall -o prog tri_ALPHABETSPAM.c.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 About 90 percent of the 300 billion emails sent every day are spam. Thus, a
 fast spam detection mechanism is crucial for large email providers. Lots of
 spammers try to circumvent spam detection by rewriting words like “M0n3y”,
 “Ca$h”, or “Lo||ery”.

 A very simple and fast spam detection mechanism is based on the ratios
 between whitespace characters, lowercase letters, uppercase letters, and
 symbols. Symbols are defined as characters that do not fall in one of the
 first three groups.

 Input:
    * The input consists of one line with a string consisting of at least 1
      and at most 100000 characters.
    * A preprocessing step has already transformed all whitespace characters
      to underscores (_), and the line will consist solely of characters
      with ASCII codes between 33 and 126 (inclusive).

 Output:
    * Output four lines, containing the ratios of whitespace characters,
      lowercase letters, uppercase letters, and symbols (in that order).
      Your answer should have an absolute or relative error of at most 10−6.

 Example:
    Input:
    Welcome_NWERC_participants!

    Output:
    0.0740740740740741
    0.666666666666667
    0.222222222222222
    0.0370370370370370

    Input:
    \/\/in_US$100000_in_our_Ca$h_Lo||ery!!!

    Output:
    0.128205128205128
    0.333333333333333
    0.102564102564103
    0.435897435897436

****************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_CHAR    100000      // max chars in input string
#define UNDERSCORE '_'          // underscore char

int main () {
    char str[MAX_CHAR] = {0};   // input string
    int spaces = 0;              // white space letter
    int lowers = 0;              // lower case letter
    int uppers = 0;              // upper clase letter
    int symbols = 0;             // symbol
    int str_len;

    scanf("%s", str);
    str_len = strlen(str);

    for (int i = 0; i < str_len; ++i) {
        if (str[i] == UNDERSCORE) spaces++;
        if(islower(str[i])) lowers++;
        if (isupper(str[i])) uppers++;
    }
    symbols = str_len - spaces - lowers - uppers;

    printf("%.16f\n", (double)spaces/str_len);
    printf("%.16f\n", (double)lowers/str_len);
    printf("%.16f\n", (double)uppers/str_len);
    printf("%.16f\n", (double)symbols/str_len);

    return 0;
}
