/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> template.c
 Example:     gcc -Wall -o prog template.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Some people say ‘The shortest distance between two points is a straight
 line.’ However, this depends on the distance metric employed. Between
 points (x1,y1) and (x2,y2), the Euclidean (aka straight-line) distance
 is:
                    ((x₁−x₂)²+(y₁−y₂)²)^(1/2)

 However, other distance metrics are often useful. For instance, in a city
 full of buildings, it is often impossible to travel in a straight line
 between two points, since buildings are in the way. In this case, the
 so-called Manhattan (or city-block) distance is the most useful:

                        |x₁−x₂|+|y₁−y₂|

 Both Euclidean and city-block distance are specific instances of what is
 more generally called the family of p-norms. The distance according to norm
 p is given by

                    (|x₁−x₂|ᵖ+|y₁−y₂|ᵖ)^(1/p)

 If we look at Euclidean and Manhattan distances, these are both just
 specific instances of p=2 and p=1, respectively.

 For p<1 this distance measure is not actually a metric, but it may still be
 interesting sometimes. For this problem, write a program to compute the
 p-norm distance between pairs of points, for a given value of p.

 Input:
    * The input file contains up to 1000 test cases, each of which contains
      five real numbers, x₁ y₁ x₂ y₂ p, each of which have at most 10 digits
      past the decimal point. All coordinates are in the range (0,100] and p
      is in the range [0.1,10]. The last test case is followed by a line
      containing a single zero.

 Output:
    * For each test case output the p-norm distance between the two points
      (x₁,y₁) and (x₂,y₂). The answer should be accurate within 0.0001
      error.

 Example:
    Input:
    1.0 1.0 2.0 2.0 2.0
    1.0 1.0 2.0 2.0 1.0
    1.0 1.0 20.0 20.0 10.0
    0

    Output:
    1.4142135624
    2.0000000000
    20.3636957882

****************************************************************************/

#include <math.h>
#include <stdio.h>

int main () {
    double x1 = 0;
    double y1 = 0;
    double x2 = 0;
    double y2 = 0;
    double p = 0;
    double result = 0;

    scanf("%lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &p);

    while (x1 != 0) {
        result = pow((pow(fabs(x1-x2), p) + pow(fabs(y1-y2), p)), 1/p);

        printf("%.5f\n", result);

        scanf("%lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &p);
    }

    return 0;
}