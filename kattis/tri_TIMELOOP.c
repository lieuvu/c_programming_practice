/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_TIMELOOP.c
 Example:     gcc -Wall -o prog ez_TIMELOOP.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Last night when you went to sleep, you had a strange feeling that you may
 see the same day again. And your strange feeling came to be when you woke
 up, everyone seemed to think that it was yesterday morning! The same
 strange feeling came back in the evening.

 When this pattern continued for days, you looked for help from a time
 wizard. The wizard says he can break you out of the time loop, but you
 must chant his spell. The wizard gives you a magic number, and you must
 count up to that number, starting at 1, saying “Abracadabra” each time.

 Input:
    * Input consists of a single integer N (1≤N≤100).

 Output:
    * Output the entire wizard’s spell by counting from 1 to N, giving one
      number and “Abracadabra” per line.

 Example:
    Input:
    5

    Output:
    1 Abracadabra
    2 Abracadabra
    3 Abracadabra
    4 Abracadabra
    5 Abracadabra

****************************************************************************/

#include <stdio.h>

int main () {
    const char *spell = "Abracadabra";
    int num_of_spell;

    if (scanf("%d", &num_of_spell) == 1) {
        for (int i = 1; i <= num_of_spell; ++i) {
            printf("%d %s\n", i, spell);
        }

    } else {
        printf("Read error!");
        return 1;
    }

	return 0;
}