/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_PTICE.c
 Example:     gcc -Wall -o prog tri_PTICE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Adrian, Bruno and Goran wanted to join the bird lovers’ club. However, they
 did not know that all applicants must pass an entrance exam. The exam
 consists of N questions, each with three possible answers: A, B and C.

 Unfortunately, they couldn’t tell a bird from a whale so they are trying to
 guess the correct answers. Each of the three boys has a theory of what set
 of answers will work best:

 Adrian claims that the best sequence is: A, B, C, A, B, C, A, B, C, A, B,
 C ...

 Bruno is convinced that this is better: B, A, B, C, B, A, B, C, B, A, B,
 C ...

 Goran laughs at them and will use this sequence: C, C, A, A, B, B, C, C, A,
 A, B, B ...

 Write a program that, given the correct answers to the exam, determines who
 of the three was right – whose sequence contains the most correct answers.

 Input:
    * The first line contains an integer N (1≤N≤100), the number of
      questions on the exam.
    * The second line contains a string of N letters ‘A’, ‘B’ and ‘C’. These
      are, in order, the correct answers to the questions in the exam.

 Output:
    * On the first line, output M, the largest number of correct answers one
      of the three boys gets.
    * After that, output the names of the boys (in alphabetical order) whose
      sequences result in M correct answers.

 Example:
    Input:
    5
    BAACC

    Output:
    3
    Bruno

    Input:
    9
    AAAABBBBB

    Output:
    4
    Adrian
    Bruno
    Goran

****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

#define NAME_MAX        10  // max characters of name
#define ANS_PAT_MAX     6   // max answer pattern characters
#define APPL_MAX        3   // max applicants
#define Q_MAX           100 // max exam questions

// -------------------
// Definition of data
// -------------------

/**
 @struct Applicant data structure

 @field name        The name of an applicant.
 @field ans_pat     The answer pattern of the applicant.
 @field correct_ans The number of correct answer of the applicant.
 */
typedef struct Applicant {
    char name[NAME_MAX];
    char ans_pat[Q_MAX];
    int correct_ans;
} Applicant;

// --------------------------------
// Declaration of private functions
// --------------------------------

static int GetNumberOfCorrectAnswer(Applicant, char *, int);

// -------------
// Main function
// -------------

int main () {
    // initialize application information
    Applicant appls[APPL_MAX] = { {"Adrian", "ABC", 0}, {"Bruno", "BABC", 0},
                                  {"Goran", "CCAABB", 0} };
    int q_num;                // number of questions
    char ans[Q_MAX] = {0};    // answers
    int correct_ans_max = 0;  // max correct answer
    scanf("%d %s", &q_num, ans);

    for (int i = 0; i < APPL_MAX; ++i) {
        int correct_ans = GetNumberOfCorrectAnswer(appls[i], ans, q_num);

        // record correct answers of applicant
        appls[i].correct_ans = correct_ans;

        // update max correct answers
        if (correct_ans_max < correct_ans) {
            correct_ans_max = correct_ans;
        }
    }

    printf("%d\n", correct_ans_max);
    for (int i = 0; i < APPL_MAX; ++i) {
        if (appls[i].correct_ans == correct_ans_max) {
            printf("%s\n", appls[i].name);
        }
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Get a number of correct answers of an applicant.

 @param appl  The applicant.
 @param ans   The answers.
 @param q_num The number of questions.

 @return The number of correct answers of the applicant.
 */
static int GetNumberOfCorrectAnswer(Applicant appl, char *ans, int q_num) {
    int correct_ans = 0;
    int ans_pat_len = strlen(appl.ans_pat); // answer pattern length

    for (int i = 0; i < q_num; ++i) {
        if (ans[i] == appl.ans_pat[i%ans_pat_len]) {
            correct_ans++;
        }
    }

    return correct_ans;
}
