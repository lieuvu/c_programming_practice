/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_DETAILEDDIFFERENCES.c
 Example:     gcc -Wall -o prog tri_DETAILEDDIFFERENCES.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 One of the most basic problems in information processing is identifying
 differences between data. This is useful when comparing files, for example.
 For this problem, write a program which identifies the differences between
 pairs of strings to make it easier for humans to see the differences.

 Your program should identify those characters which differ between the two
 given strings in a visually striking way. Output the two input strings on
 two lines, and then identify the differences on the line below using
 periods (for identical characters) and asterisks (for differing
 characters). For example:

    ATCCGCTTAGAGGGATT
    GTCCGTTTAGAAGGTTT
    *....*.....*..*..

 Input:
    * The first line of input contains an integer 1≤n≤500, indicating the
      number of test cases that follow. Each test case is a pair of lines of
      the same length, 1 to 50 characters. Each string contains only letters
      (a-z, A-Z) or digits (0-9).

 Output:
    * For each test case, output the two lines in the order they appear in
      the input. Output a third line indicating similarities and differences
      as described above. Finally, output a blank line after each test case.

 Example:
    Input:
    3
    ATCCGCTTAGAGGGATT
    GTCCGTTTAGAAGGTTT
    abcdefghijklmnopqrstuvwxyz
    bcdefghijklmnopqrstuvwxyza
    abcdefghijklmnopqrstuvwxyz0123456789
    abcdefghijklmnopqrstuvwxyz0123456789

    Output:
    ATCCGCTTAGAGGGATT
    GTCCGTTTAGAAGGTTT
    *....*.....*..*..

    abcdefghijklmnopqrstuvwxyz
    bcdefghijklmnopqrstuvwxyza
    **************************

    abcdefghijklmnopqrstuvwxyz0123456789
    abcdefghijklmnopqrstuvwxyz0123456789
    ....................................


****************************************************************************/

#include <stdio.h>

#define MAX     55  // max character

int main () {
    int test_num;           // number of test case

    scanf("%d", &test_num);

    while (test_num) {
        char str1[MAX] = {0};
        char str2[MAX] = {0};
        char output[MAX] = {0};

        scanf("%s %s", str1, str2);

        for (int i = 0; i < MAX; ++i) {
            if (str1[i] == '\0') {
                break;
            } else if (str1[i] != str2[i]) {
                output[i] = '*';
            } else {
                output[i] = '.';
            }
        }

        printf("%s\n", str1);
        printf("%s\n", str2);
        printf("%s\n", output);
        printf("\n");

        test_num -= 1;  // decrease test number
    }

    return 0;
}
