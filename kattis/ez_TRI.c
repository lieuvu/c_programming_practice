/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_TRI.c
 Example:     gcc -Wall -o prog ez_TRI.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Little Mirko wrote into his math notebook an equation containing three
 positive integers, the equals sign and one of the four basic arithmetic
 operations (addition, subtraction, multiplication and division).

 During another class, his friend Slavko erased the equals sign and the
 operations from Mirko’s notebook. Help Mirko by reconstructing the equation
 from the three integers.

 Input:
    * The first line of input contains three integers less than 100,
      separated by spaces. The input data will guarantee that a solution,
      although not necessarily unique, will always exist.

 Output:
    * On a single line, output a valid equation containing the three
      integers (in the same order), an equals sign and one of the four
      operations. If there are multiple solutions, output any of them.

 Example:
    Input:
    5 3 8

    Output:
    5+3=8

    Input:
    5 15 3

    Output:
    5=15/3

****************************************************************************/

#include <stdio.h>
#include <stdbool.h>

#define MAX_OPERAND     3               // max operands
#define MAX_OPERATION   MAX_OPERAND-1   // max operations

// ------------------
// Definition of data
// ------------------
/**
 @enum Operation position

 @constant first  The operation is at first pos.
 @constant second The operation is at second pos.
 */
enum OperationPos {
    first,
    second
};

// --------------------------------
// Declaration of private functions
// --------------------------------

static void PrintMissingOperation(int *);
static void FindArithmeticOperation(int, int, int, char *);

// -------------
// Main function
// -------------

int main () {
    int operands[MAX_OPERAND] = {0};

    for (int i = 0; i < MAX_OPERAND; ++i) {
        scanf("%d", operands+i);
    }

    PrintMissingOperation(operands);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Print missing operation of three
 integer operands in order.

 @param operands    The array of three operands.
 */
static void PrintMissingOperation(int *operands) {
    char operations[MAX_OPERATION] = {0};
    int a = operands[0];
    int b = operands[1];
    int c = operands[2];

    // set the equal sign at first pos.
    operations[first] = '=';

    // find the arithmetic operation
    // when equal sign appears first
    FindArithmeticOperation(b, c, a, operations);

    // if can not find the arithmetic operation
    // for the second pos.
    if (!operations[second]) {
        operations[first] = '\0';   // clear the sign at first pos
        operations[second] = '=';   // set the equal sign at second pos

        // find the arithmetic operation
        // when equal sign appears second
        FindArithmeticOperation(a, b, c, operations);
    }

    // print result
    for (int i = 0; i < MAX_OPERAND; ++i) {
        printf("%d", operands[i]);
        if (i < MAX_OPERATION) {
            printf("%c", operations[i]);
        }
    }
    printf("\n");
}

/**
 Find arithmetic operation of three
 integers in order.

 @param a           The first integer.
 @param b           The second integer.
 @param c           The thrid integer.
 @param operations  The array of operations.
 */
static void FindArithmeticOperation(int a, int b, int c, char *operations) {
    char op = '\0';     // finding operation

    if (a + b == c) {
        op = '+';
    } else if (a - b == c) {
        op = '-';
    } else if (a * b == c) {
        op = '*';
    } else if (a / b == c) {
        op = '/';
    }

    /* if the first position is empty */
    if (!operations[first]) {
        operations[first] = op;     // write the finding op to first pos

    /* otherwise, the second position is empty */
    } else {
        operations[second] = op;    // write the finnding op to second pos
    }
}
