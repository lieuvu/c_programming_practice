/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_ESTIMATINGTHEAREAOFACIRCLE.c
 Example:     gcc -Wall -o prog ez_ESTIMATINGTHEAREAOFACIRCLE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Note:
    * See pic_problems/ez_ESTIMATINGTHEAREAOFACIRCLE.pdf for visual
      presentation of the problem.

 One way to estimate the area of a circle is to draw a square that just
 encompasses the circle and mark points randomly (with uniform probability)
 with a marker. Then, when you get tired of marking points, count up the
 number of points that you marked that landed in the circle and divide it by
 the total number of points that you marked. That gives you an idea of how
 large the circle is relative to the square. Your job is to judge how
 accurate this is for given circles and experiment outcomes.

 Input:
    * Input contains up to 1000 test cases, one test case per line. Each
      line has three space-separated numbers: r m c, where 0<r≤1000 is a
      real number with at most 5 digits past the decimal indicating the true
      radius of the circle, 1≤m≤100000 is an integer indicating the total
      number of marked points, and 0≤c≤m is an integer indicating the number
      of marked points that fall in the circle. Input ends with a line
      containing three zeros, which should not be processed.

 Output:
    * For each test case, print a line containing two numbers: the true area
      of the circle and the estimate according to the experiment. Both
      numbers may have a relative error of at most 10⁻⁵.

 Example:
    Input:
    1.0 100 75
    10.0 5000 4023
    3.0 21 17
    0 0 0

    Output:
    3.141592654 3
    314.1592654 321.84
    28.27433388 29.14285714

****************************************************************************/

#include <math.h>
#include <stdio.h>

#ifndef M_PI
#define M_PI acos(-1)
#endif

int main () {
    double r = 0;           // radius of the circle
    int m = 0;              // total number of marked points
    int c = 0;              // number of marked points in the circle
    double area = 0;        // area of the circle
    double es_area = 0;     // estimated area of the circle

    scanf("%lf %d %d", &r, &m, &c);

    while (r != 0) {
        area = M_PI*r*r;
        es_area = (2*r)*(2*r)*c/m;

        printf("%.5f %.5f\n", area, es_area);

        scanf("%lf %d %d", &r, &m, &c);
    }

    return 0;
}