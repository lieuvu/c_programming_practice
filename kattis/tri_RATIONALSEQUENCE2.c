/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_RATIONALSEQUENCE2.c
 Example:     gcc -Wall -o prog ez_RATIONALSEQUENCE2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Note:
    * See pic_problems/ez_RATIONALSEQUENCE2.pdf for visual presentation of
      the problem.

 A sequence of positive rational numbers is defined as follows:

 An infinite full binary tree labeled by positive rational numbers is
 defined by:
    * The label of the root is 1/1.
    * The left child of label p/q is p/(p+q).
    * The right child of label p/q is (p+q)/q.

 The sequence is defined by doing a level order (breadth first) traversal of
 the tree (indicated by the light dashed line). So that:
         F(1)=1/1,F(2)=1/2,F(3)=2/1,F(4)=1/3,F(5)=3/2,F(6)=2/3,…

 Write a program which finds the value of n for which F(n) is p/q for inputs
 p and q.

 Input:
    * The first line of input contains a single integer P, (1≤P≤1000), which
      is the number of data sets that follow. Each data set should be
      processed identically and independently. Each data set consists of a
      single line of input. It contains the data set number, K, a single
      space, the numerator, p, a forward slash (/) and the denominator, q,
      of the desired fraction.

 Output:
    * For each data set there is a single line of output. It contains the
      data set number, K, followed by a single space which is then followed
      by the value of n for which F(n) is p/q. Inputs will be chosen so n
      will fit in a 32-bit integer.

 Example:
    Input:
    4
    1 1/1
    2 1/3
    3 5/2
    4 2178309/1346269

    Output:
    1 1
    2 4
    3 11
    4 1431655765

****************************************************************************/

#include <stdio.h>

// -----------------
// Prototype of data
// -----------------

/** @typedef Fraction */
typedef struct Fraction Fraction;

#define ROOT        (Fraction){1, 1}
#define ROOT_ID     1

// ------------------
// Definition of data
// ------------------

/**
 @struct Fraction data structure

 @field n The numerator.
 @field d The denominator.
 */
struct Fraction {
    int n;
    int d;
};

// --------------------------------
// Declaration of private functions
// --------------------------------

static long FindNodeId(Fraction);

//--------------
// Main function
// -------------

int main () {
    int test_num;               // number of test

    scanf("%d", &test_num);

    while (test_num) {
        Fraction fr;
        int test_case;
        long node_id;

        scanf("%d", &test_case);
        scanf("%d/%d", &fr.n, &fr.d );

        node_id = FindNodeId(fr);

        if (node_id < ROOT_ID) {
            printf("The fraction %d/%d not belong to tree!\n", fr.n, fr.d);
        } else {
            printf("%d %ld\n", test_case, node_id);
        }

        test_num -= 1;
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Find node id of a given fraction.

 @param fr The given fraction to find node id.

 @return The id of the node in the tree, otherwise -1 if not found.
 */
static long FindNodeId(Fraction fr) {
    Fraction curr_fr = fr;      // current fraction
    int path = 0;               // array of bits, 0-left, 1-right
    int path_len = 0;           // number of evaluated bits in path

    // back trace to root if the
    // current fraction is not rood
    while (curr_fr.n != ROOT.n || curr_fr.d != ROOT.d) {
        // if the current fraction not belongs to tree
        if (curr_fr.n < ROOT.n || curr_fr.d < ROOT.d) {
            return -1;
        }

        // if numerator is less than denominator
        if (curr_fr.n < curr_fr.d) {
            curr_fr.d -= curr_fr.n;     // go up from the left
        } else {
            // if numerator is larger than denominator
            curr_fr.n -= curr_fr.d;     // go up from the right
            path |= (1 << path_len);    // record the path
        }

        path_len++;    // increase path_len;
    }

    int node_id = 1;   // the id of node, start from root

    // go from the highest bit to lowest bit in the path
    // the index of bit equals number of bits minus 1
    for (int i = path_len - 1; i >= 0; --i) {
        // if meets bit 1, go to right
        if (path & (1 << i)) {
            node_id = (node_id << 1) + 1; // multiply node_id by 2 and plus 1

        // otherwise meets bit 0, go to left
        } else  {
            node_id <<= 1;                // multiply node_id by 2
        }
    }

    return node_id;
}
