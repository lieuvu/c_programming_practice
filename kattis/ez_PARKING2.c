/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_PARKING2.c
 Example:     gcc -Wall -o prog ez_PARKING2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 When shopping on Long Street, Michael usually parks his car at some random
 location, and then walks to the stores he needs. Can you help Michael
 choose a place to park which minimises the distance he needs to walk on his
 shopping round?

 Long Street is a straight line, where all positions are integer. You pay
 for parking in a specific slot, which is an integer position on Long
 Street. Michael does not want to pay for more than one parking though. He
 is very strong, and does not mind carrying all the bags around.

 Input:
    * The first line of input gives the number of test cases, 1≤t≤100. There
      are two lines for each test case. The first gives the number of stores
      Michael wants to visit, 1≤n≤20, and the second gives their n integer
      positions on Long Street, 0≤xᵢ≤99.

 Output:
    * Output for each test case a line with the minimal distance Michael
      must walk given optimal parking.

 Example:
    Input:
    2
    4
    24 13 89 37
    6
    7 30 41 14 39 42

    Output:
    152
    70

****************************************************************************/

#include <stdio.h>

#define MAX_STORE       20      // max number of stores
#define MIN_STORE_LOC   0       // min store location
#define MAX_STORE_LOC   99      // max store location

int main () {
    int test_num = 0;           // number of tests

    scanf("%d", &test_num);

    while (test_num) {
        int stores_num = 0;                 // number of stores
        int min_store_loc = MAX_STORE_LOC;  // min store location
        int max_store_loc = MIN_STORE_LOC;  // max store location

        scanf("%d", &stores_num);

        // read locations of stores
        for (int i = 0; i < stores_num; ++i) {
            int tmp;
            scanf("%d", &tmp);

            // find min store location
            if (tmp < min_store_loc) {
                min_store_loc = tmp;
            }

            // find max store location
            if (tmp > max_store_loc) {
                max_store_loc = tmp;
            }
        }

        printf("%d\n", (max_store_loc-min_store_loc)*2);

        test_num -= 1;
    }

    return 0;
}
