/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_GRASSSEED.c
 Example:     gcc -Wall -o prog tri_GRASSSEED.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Many years ago after another unfruitful day in Cubicle Land, banging her
 head against yet another cutting edge, marketing buzzword-filled JavaScript
 framework, Janice the engineer looked out of the window and decided that
 time was ripe for a change.

 So swapping her keyboard and mouse for a fork and a spade, she started her
 own gardening company.

 After years of hard outdoor work Janice now has biceps like Van Damme and
 owns the premiere landscaping company in the whole of the South West, and
 has just been lucky enough to broker a large contract to sow lawns for
 landed gentry.

 Each contract details the size of the lawns that need to be seeded, and the
 cost of seed per square metre. How much do you need to spend on seed?

 Input:
    * One line containing a real number C (0<C≤100), the cost of seed to sow
      one square metre of lawn.
    * One line containing an integer L(0<L≤100), the number of lawns to sow.
    * L lines, each containing two positive real numbers: wi (0≤wi≤100),
      the width of the lawn, and li (0≤li≤100), the length of the lawn.
    * All real numbers are given with at most 8 decimals after the decimal
      point.

 Output:
    * One line containing a real number: the cost to sow all of the lawns.
    * All output must be accurate to an absolute or relative error of at
      most 10−6.

 Example:
    Input:
    2
    3
    2 3
    4 5
    5 6

    Output:
    112.0000000

    Input:
    0.75
    2
    2 3.333
    3.41 4.567

    Output:
    16.6796025

****************************************************************************/

/****************************************************************************
 Discussion:
    * the variable of type float always promote to double before printf
    receive it. Hence %f and %lf are no difference.
    * the variable of type float and double must be specified for scanf
    and fscanf.. input function. It needs to be specified explicilty whether
    a float or double will be received. Hence %f for float and %lf for
    double

 ****************************************************************************/

#include <stdio.h>

int main () {
    double cost;            // the cost of seed per meter square
    int num_lawn;           // the number of lawns to sow
    double area = 0.0;      // the area of the contract

    scanf("%lf %d", &cost, &num_lawn);

    for (int i = 0; i < num_lawn; ++i) {
        double w;   // width of the lawn
        double l;   // length of the lawn
        scanf("%lf %lf", &w, &l);
        area += w*l;
    }

    printf("%.7f", cost*area);

    return 0;
}