/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_YINYANGSTONES.c
 Example:     gcc -Wall -o prog ez_YINYANGSTONES.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 A mysterious circular arrangement of black stones and white stones has
 appeared. Ming has been tasked with balancing the stones so that only one
 black and one white stone remain.

 Ming has two operations for balancing the stones:

 1. Take some consecutive sequence of stones where there is exactly one more
    black stone than a white stone and replace the stones with a single
    black stone

 2. Take some consecutive sequence of stones where there is exactly one
    more white stone than black stone and replace the stones with a single
    white stone

 Given a circular arrangement, determine if it is possible for Ming to
 balance the stones.

 Input:
    * Each input will consist of a single test case. Note that your program
      may be run multiple times on different inputs. The input will consist
      of a single string s (1≤|s|≤10⁵), with only the characters capital ‘B’
      and ‘W’. The stones are arranged in a circle, so the first stone and
      the last stone are adjacent.

 Output:
    * Output 1 if it is possible for Ming to balance the stones with his
      rules. Otherwise, output 0.

 Example:
    Input:
    WWBWBB

    Output:
    1

    Input:
    WWWWBBW

    Output:
    0

    Input:
    WBBBBBWWBW

    Output:
    0

****************************************************************************/

/***************************************************************************
 Discussion:
    * It is easy to prove that Ming's method only works if the number of
      black stones equals the number of white stones so that the left
      stones will be BW or WB.

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_CHAR    100001 // max chars of input string (null-terminating char)

int main () {
    char str[MAX_CHAR] = {0};   // input string
    int w_stone = 0;            // number of white stones
    int b_stone = 0;            // number of black stones
    size_t len;                 // length of input string

    scanf("%s", str);

    len = strlen(str);
    for (size_t i = 0; i < len; ++i) {
        if (str[i] == 'W') {
            w_stone++;
        } else {
            b_stone++;
        }
    }

    printf("%d\n", w_stone == b_stone);

    return 0;
}
