/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_TSP.c
 Example:     gcc -Wall -o prog ez_TSP.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 Input:
    * Your program should take as input a single TSP instance. It will be
      run several times, once for every test case. The time limit is per
      test case.
    * The first line of standard input contains an integer 1≤N≤1000, the
      number of points. The following N lines each contain a pair of real
      numbers x,y giving the coordinates of the N points. All numbers in the
      input have absolute value bounded by 10⁶.
    * The distance between two points is computed as the Euclidean distance
      between the two points, rounded to the nearest integer.

 Output:
    * Standard output should consist of N lines, the i’th of which should
      contain the (0-based) index of the i’th point visited.

 Score:
    * Let Opt be the length of an optimal tour, Val be the length of the
      tour found by your solution, and Naive be the length of the tour found
      by the algorithm below. Define x=(Val−Opt)/(Naive−Opt). (In the
      special case that Naive=Opt, define x=0 if Val=Opt, and x=∞ if
      Val>Opt.)
    * The score on a test case is 0.02x. Thus, if your tour is optimal, you
      will get 1 point on this test case. If your score is halfway between
      Naive and Opt, you get roughly 0.14 points. The total score of your
      submission is the sum of your score over all test cases. There will be
      a total of 50 test cases. Thus, an implementation of the algorithm
      below should give a score of at least 1 (it will get 0.02 points on
      most test cases, and 1.0 points on some easy cases where it manages to
      find an optimal solution).
    * The algorithm giving the value Naive is as follows:
        GreedyTour
            tour[0] = 0
            used[0] = true
                for i = 1 to n-1
                    best = -1
                    for j = 0 to n-1
                        if not used[j] and (best = -1 or
                        dist(tour[i-1],j) < dist(tour[i-1],best))
                            best = j
                        tour[i] = best
                        used[best] = true
                return tour
    * The sample output gives the tour found by GreedyTour on the sample
      input. The length of the tour is 323.

 Example:
    Input:
    10
    95.0129 61.5432
    23.1139 79.1937
    60.6843 92.1813
    48.5982 73.8207
    89.1299 17.6266
    76.2097 40.5706
    45.6468 93.5470
    1.8504 91.6904
    82.1407 41.0270
    44.4703 89.3650

    Output:
    0
    8
    5
    4
    3
    9
    6
    2
    1
    7

****************************************************************************/

#include <math.h>
#include <stdio.h>

#define MAX_POINT   1000    // max points of input

// ------------------
// Definition of data
// ------------------

/**
 @struct Point data structure.

 @field x The horizontal coordinate.
 @field y The vertical coordinate.
 */
typedef struct Point {
    double x;
    double y;
} Point;


// --------------------------------
// Declaration of private functions
// --------------------------------

static double Distance(Point, Point);
static void NearestNeighborTour(Point *, int *, int);
static void PrintTour(int *, int);
static void PrintTourLength(Point *, int *, int);

// -------------
// Main function
// -------------

int main () {
    Point points[MAX_POINT] = {0};      // points
    int tour[MAX_POINT] = {0};          // tour
    int p_num = 0;                      // number of points

    scanf("%d", &p_num);

    for (int i = 0; i < p_num; ++i) {
        scanf("%lf %lf", &points[i].x, &points[i].y);
    }

    NearestNeighborTour(points, tour, p_num);
    PrintTour(tour, p_num);
    PrintTourLength(points, tour, p_num);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Calculate distance between two points.

 @param p1 The first point.
 @param p2 The second point.

 @return The distance of two points, rounded
 to the nearest integer.
 */
static double Distance(Point p1, Point p2) {
    double dist_x = p1.x - p2.x;
    double dist_y = p1.y - p2.y;
    return round(sqrt(dist_x*dist_x + dist_y*dist_y));
}

/**
 Find tour using nearest neighour algorithm.

 @param points  The array of points.
 @param tour    The tour.
 @param size    The number of points in tour.
 */
static void NearestNeighborTour(Point *points, int *tour, int size) {
    int used[MAX_POINT] = {0};      // used point in tour

    used[0] = 1;                    // init used

    // fill in the tour
    for (int i = 1; i < size; ++i) {
        int best = -1;

        // iterate through each point
        for (int j = 0; j < size; ++j) {
            // if a point is not used
            if (used[j] == 0) {
                // if best is not found
                if (best == -1) {
                    best = j;   // set best to current point index

                // otherwise best is found
                } else {
                    // if a shorter distance found
                    if (Distance(points[tour[i-1]],points[j]) <
                        Distance(points[tour[i-1]],points[best])) {
                        best = j;       // update best to current point index
                    }
                }
            }

        }

        tour[i] = best;     // update tour
        used[best] = 1;     // update used
    }
}

/**
 Print tour.

 @param tour The tour.
 @param size The number of points in tour.
 */
static void PrintTour(int *tour, int size) {
    for (int i = 0; i < size; ++i) {
        printf("%d\n", tour[i]);
    }
}

/**
 Print the tour length.

 @param points  The array of points.
 @param tour    The tour.
 @param size    The number of points in tour.
 */
static void PrintTourLength(Point *points, int *tour, int size) {
    double length = 0;

    for (int i = 1; i < size; ++i) {
        if (i == size-1) {
            length += Distance(points[tour[i]], points[tour[0]]);
        } else {
            length += Distance(points[tour[i]], points[tour[i+1]]);
        }
    }

    printf("Total tour length: %.2f\n", length);
}
