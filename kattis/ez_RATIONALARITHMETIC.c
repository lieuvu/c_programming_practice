/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -Wformat -posix -o <output_file> med_RATIONALARITHMETIC.c
 Example:     gcc -Wall -Wformat -posix -o prog med_RATIONALARITHMETIC.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 2.9

 Input:
    * The first line of input contains one integer, giving the number of
      operations to perform.
    * Then follow the operations, one per line, each of the form x1 y1 op
      x2 y2. Here, −10^9≤x1,y1,x2,y2<10^9 are integers, indicating that the
      operands are x1/y1 and x2/y2. The operator op is one of ’+’, ’−’, ’∗’,
      ’/’, indicating which operation to perform.
    * You may assume that y1≠0, y2≠0, and that x2≠0 for division operations.

 Output:
    * For each operation in each test case, output the result of performing
      the indicated operation, in shortest terms, in the form indicated.

 Example:
    Input:
    4
    1 3 + 1 2
    1 3 - 1 2
    123 287 / 81 -82
    12 -3 * -1 -1

    Output:
    5 / 6
    -1 / 6
    -82 / 189
    -4 / 1

****************************************************************************/

#include <stdio.h>
#include <inttypes.h>

// -------------------
// Definition of data
// -------------------

/**
 @struct Fraction data structure

 @field n The numerator
 @field d The denominator
 */
typedef struct Fraction {
    int64_t n;
    int64_t d;
} Fraction;

// --------------------------------
// Declaration of private functions
// --------------------------------

static Fraction AddTwoFracs(Fraction, Fraction);
static Fraction SubstractTwoFracs(Fraction, Fraction);
static Fraction MultiplyTwoFracs(Fraction, Fraction);
static Fraction DivideTwoFracs(Fraction, Fraction);
static void ReduceFrac(Fraction *);
static void FormatFrac(Fraction *);
static void PrintFrac(Fraction *);

// -------------
// Main function
// -------------

int main () {
    int num_of_op;
    int64_t n1, d1, n2, d2;
    char op;
    Fraction result;

    if (scanf("%d", &num_of_op) == 1) {
        while (num_of_op) {
            if (scanf("%ld %ld %c %ld %ld", &n1, &d1, &op, &n2, &d2) == 5) {
                Fraction frac1 = {.n = n1, .d = d1};
                Fraction frac2 = {.n = n2, .d = d2};

                switch (op) {
                    case '+':
                        result = AddTwoFracs(frac1, frac2);
                        break;
                    case '-':
                        result = SubstractTwoFracs(frac1, frac2);
                        break;
                    case '*':
                        result = MultiplyTwoFracs(frac1, frac2);
                        break;
                    case '/':
                        result = DivideTwoFracs(frac1, frac2);
                        break;
                }

                ReduceFrac(&result);
                FormatFrac(&result);
                PrintFrac(&result);

                num_of_op -= 1;

            } else {
                printf("Read error!");
                return 1;
            }
        }

    } else {
        printf("Read error!");
        return 2;
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Add two fractions

 @param frac1 The first fraction.
 @param frac2 The second fraction.

 @return The fraction that is the sum of two fractions.
 */
static Fraction AddTwoFracs(Fraction frac1, Fraction frac2) {
    Fraction result;

    result.n = frac1.n * frac2.d + frac2.n * frac1.d;
    result.d = frac1.d * frac2.d;

    return result;
}

/**
 Substract two fractions

 @param frac1 The first fraction.
 @param frac2 The second fraction.

 @return The fraction that is the difference of frac1 and frac2.
 */
static Fraction SubstractTwoFracs(Fraction frac1, Fraction frac2) {
    Fraction result;

    result.n = frac1.n * frac2.d - frac2.n * frac1.d;
    result.d = frac1.d * frac2.d;

    return result;
}

/**
 Multiply two fractions

 @param frac1 The frist fraction.
 @param frac2 The second fraction.

 @return The fraction that is the multiplication of frac1 and frac2.
 */
static Fraction MultiplyTwoFracs(Fraction frac1, Fraction frac2) {
    Fraction result;

    result.n = frac1.n * frac2.n;
    result.d = frac1.d * frac2.d;

    return result;
}

/**
 Divide two fractions

 @param frac1 The first fraction.
 @param frac2 The second fraction.

 @return The fraction that is the division of frac1 and frac2.
 */
static Fraction DivideTwoFracs(Fraction frac1, Fraction frac2) {
    Fraction result;

    result.n = frac1.n * frac2.d;
    result.d = frac1.d * frac2.n;

    return result;
}

/**
 Reduce a fraction to the simplest form

 @param frac The pointer to the fraction to reduce.
 */
static void ReduceFrac(Fraction *frac) {
    int64_t a = (frac->n < 0) ? -(frac->n) : frac->n;
    int64_t b = (frac->d < 0) ? -(frac->d) : frac->d;

    if (a != 0 && b != 0) {
        while (a % b) {
            int64_t temp = a % b;
            a = b;
            b = temp;
        }

        frac->n = frac->n / b;
        frac->d = frac->d / b;
    } else if (a == 0 || b ==0) {
        frac->n = 0;
        frac->d = 1;
    }
}

/**
 Format the fraction to the problem setting format

 @param frac The pointer to the fraction to format.
 */
static void FormatFrac(Fraction *frac) {
    int64_t n = frac->n;
    int64_t d = frac->d;
    if ((n * d < 0 && d < 0) || (n * d > 0 && d < 0)) {
        frac->n = -n;
        frac->d = -d;
    }
}

/**
 Print fraction. This function is used for debugging

 @param frac The pointer to the fraction to print.
 */
static void PrintFrac(Fraction *frac) {
    printf("%" PRId64 "/" "%" PRId64 "\n", frac->n, frac->d);
}