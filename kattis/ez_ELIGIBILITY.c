/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> template.c
 Example:     gcc -Wall -o prog template.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Note:
    * See pic_problems/tri_ELIGIBILITY.pdf for visual presentation of the
      problem.

 Every year, students across the world participate in the ACM ICPC¹. In
 order to participate in this contest, a student must be eligible to
 compete. In this problem, you will be given information about students and
 you will write a program to determine their eligibility to participate in
 the ICPC.

 We will start by assuming that each student meets the “Basic Requirements”
 as specified in the ICPC rules—the student is willing to compete at the
 World Finals, is a registered student with at least half-time load,
 competes for only one institution in a contest year, and has not competed
 in two world finals or five regional contests.

 The rules to decide if a student is eligible to compete in the contest year
 2014–2015 are as follows:

    1. if the student first began post-secondary studies in 2010 or later,
       the student is eligible;
    2. if the student is born in 1991 or later, the student is eligible;
    3. if none of the above applies, and the student has completed more than
       an equivalent of 8 semesters of full-time study, the student is
       ineligible;
    4. if none of the above applies, the coach may petition for an extension
       of eligibility by providing the student’s academic and work history.

 For “equivalent of 8 semesters of full-time study,” we consider each
 semester of full-time study to be equivalent to a student completing 5
 courses. Thus, a student who has completed 41 courses or more is considered
 to have more than 8 semesters of full-time study.

 Input:
    * The input consists of a number of cases. The first line contains a
      positive integer, indicating the number of cases to follow. Each of
      the cases is specified in one line in the following format

            name YYYY/MM/DD YYYY/MM/DD courses

      where name is the name of the student (up to 30 alphabetic
      characters), the first date given is the date the student first began
      post-secondary studies, and the second date given is the student’s
      date of birth. All dates are given in the format above with 4-digit
      year and 2-digit month and day. courses is a non-negative integer
      indicating the number of courses that the student has completed.

    * There are at most 1 000 cases.

 Output:
    * For each line of output, print the student’s name, followed by a
      space, followed by one of the strings eligible, ineligible, and coach
      petitions as appropriate.

 Example:
    Input:
    3
    EligibleContestant 2013/09/01 1995/03/12 10
    IneligibleContestant 2009/09/01 1990/12/12 50
    PetitionContestant 2009/09/01 1990/12/12 35

    Output:
    EligibleContestant eligible
    IneligibleContestant ineligible
    PetitionContestant coach petitions

 Footnotes:
    1. This may be the only problem statement in which these acronyms expand
       to Association for Computing Machinery International Collegiate
       Programming Contest.

****************************************************************************/

#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#define MAX_NAME    31      // max chars of name plus null-terminating char
#define MAX_DATE    11      // max chars of date plus null-terminating char
#define BASE        10      // base of conversion from string to int
#define S_YEAR      2010    // study year
#define B_YEAR      1991    // birth year
#define MAX_CS      40      // max courses
#define ELG         "eligible"
#define IELG        "ineligible"
#define C_P         "coach petitions"

// -------------------
// Definition of data
// -------------------

/**
 @struct Date data structure

 @field y The year.
 @field m The month.
 @field d The day.
 */
typedef struct Date {
    int y;
    int m;
    int d;
} Date;

/**
 @struct Student data structure

 @field name   The student's name.
 @field s_date The studies day.
 @field b_date The birthday.
 @field cs     The completed courses.
 */
typedef struct Student {
    char name[MAX_NAME];
    Date s_date;
    Date b_date;
    int cs;
} Student;

// --------------------------------
// Declaration of private functions
// --------------------------------

static Date DateFromString(char *);
static Student CreateStudent(char *, char *, char *, int);

// -------------
// Main function
// -------------

int main () {
    int test_num;           // number of test case

    scanf("%d", &test_num);

    while (test_num) {
        char name[MAX_NAME] = {0};        // student's name
        char s_date_str[MAX_DATE] = {0};  // study day string
        char b_date_str[MAX_DATE] = {0};  // birthday string
        int cs = 0;                       // number of completed courses
        Student st;                       // student

        scanf("%s %s %s %d", name, s_date_str, b_date_str, &cs);

        st = CreateStudent(name, s_date_str, b_date_str, cs);

        /* print output */
        printf("%s ", st.name);
        // if study year is from 2010 onwards
        if (st.s_date.y >= S_YEAR) {
            printf(ELG);    // eligible
        } else if (st.b_date.y >= B_YEAR) {
            // if birth year is from 1991 onwards
            printf(ELG);    // eligible
        } else if (st.cs > MAX_CS) {
            // if completed more than 41 courses
            printf(IELG);   // ineligible
        } else {
            printf(C_P);
        }
        printf("\n");

        test_num -= 1;
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Create a date from string with format YYYY/MM/DD.

 @param date_str The date string in format YYYY/MM/DD.

 @return The date from the string.
 */
static Date DateFromString(char *date_str) {
   char y_str[MAX_DATE] = {0};  // year string
   char m_str[MAX_DATE] = {0};  // month string
   char d_str[MAX_DATE] = {0};  // day string
   char *endptr;
   Date date;

   strncpy(y_str, date_str, 4);     // read year string from date string
   strncpy(m_str, date_str+5, 2);   // read month string from date string
   strncpy(d_str, date_str+8, 2);   // read day string from date string

   date.y = strtoimax(y_str, &endptr, BASE);
   date.m = strtoimax(m_str, &endptr, BASE);
   date.d = strtoimax(d_str, &endptr, BASE);

   return date;
}

/**
 Create student with given information.

 @param name        The student's name.
 @param s_date_str  The study day string.
 @param b_date_str  The birthday string.
 @param cs          The completed course.

 @return The student with given information.
 */
static Student CreateStudent(char *name, char *s_date_str,
                             char *b_date_str, int cs) {
    Student st;

    strcpy(st.name, name);
    st.s_date = DateFromString(s_date_str);
    st.b_date = DateFromString(b_date_str);
    st.cs = cs;

    return st;
}
