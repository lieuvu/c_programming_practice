/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_MIXEDFRACTIONS.c
 Example:     gcc -Wall -o prog tri_MIXEDFRACTIONS.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 You are part of a team developing software to help students learn basic
 mathematics. You are to write one part of that software, which is to
 display possibly improper fractions as mixed fractions. A proper fraction
 is one where the numerator is less than the denominator; a mixed fraction
 is a whole number followed by a proper fraction. For example the improper
 fraction 27/12 is equivalent to the mixed fraction 2 3/12. You should not
 reduce the fraction (i.e. don’t change 3/12 to 1/4).

 Input:
    * Input has one test case per line. Each test case contains two integers
      in the range [1,2³¹−1]. The first number is the numerator and the
      second is the denominator. A line containing 0 0 will follow the last
      test case.

 Output:
    * For each test case, display the resulting mixed fraction as a whole
      number followed by a proper fraction, using whitespace to separate the
      output tokens.

 Example:
    Input:
    27 12
    2460000 98400
    3 4000
    0 0

    Output:
    2 3 / 12
    25 0 / 98400
    0 3 / 4000

****************************************************************************/

#include <stdio.h>

int main () {
    int n;  // numerator
    int d;  // denominator
    int wn; // whole number
    int re; // remainder

    // read first input
    scanf("%d %d", &n, &d);

    while (n != 0 && d != 0) {
        wn = n/d;   // calculate whole number
        re = n%d;   // calculate remainder

        // print output
        printf("%d %d / %d\n", wn, re, d);

        // read next input
        scanf("%d %d", &n, &d);
    }

    return 0;
}
