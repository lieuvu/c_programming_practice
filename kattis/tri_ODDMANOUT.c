/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_ODDMANOUT.c
 Example:     gcc -Wall -o prog tri_ODDMANOUT.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 You are hosting a party with G guests and notice that there is an odd
 number of guests! When planning the party you deliberately invited only
 couples and gave each couple a unique number C on their invitation. You
 would like to single out whoever came alone by asking all of the guests for
 their invitation numbers.

 Input:
    * The first line of input gives the number of cases, N. N test cases
      follow. For each test case there will be:
        + One line containing the value G the number of guests.
        + One line containing a space-separated list of G
          integers. Each integer C indicates the invitation code of a
          guest.
    * You may assume that 1≤N≤50,0<C<231,3≤G<1000.

 Output:
    * For each test case, output one line containing “Case #x: ” followed by
      the number C of the guest who is alone.

 Example:
    Input:

    Output:


****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main () {
    int test_num;            // number of test
    int g_num;               // number of guests
    unsigned int *ivt_codes; // invitation code array
    int case_num = 0;        // case number

    scanf("%d", &test_num);

    while (test_num) {
        case_num++; // increase case number

        scanf("%d", &g_num);

        // allocate memory for invitation code array
        ivt_codes = malloc(sizeof(*ivt_codes)*g_num);

        // fill in the array of codes
        for (int i = 0; i < g_num; ++i) {
            scanf("%u", (ivt_codes+i));
        }

        // mark pair code with zero, leave the odd
        // code in the code array
        for (int i = 0; i < g_num-1; ++i) {
            if (ivt_codes[i] != 0) {
                for (int j = i+1; j < g_num; ++j) {
                    if (ivt_codes[i] == ivt_codes[j]) {
                        ivt_codes[i] = 0;
                        ivt_codes[j] = 0;
                        break;
                    }
                }
            }
        }

        // print output
        printf("Case #%d:", case_num);
        for (int i = 0; i < g_num; ++i) {
            if (ivt_codes[i] != 0) {
                printf(" %d", ivt_codes[i]);
            }
        }
        printf("\n");

        free(ivt_codes);

        test_num -= 1;  // decrease test num
    }

    return 0;
}
