/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_FILIP.c
 Example:     gcc -Wall -o prog tri_FILIP.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Mirko has a younger brother, FILIP, who just started going to school and is
 having trouble with numbers. To help him get the hang for number scale, his
 teacher writes two three-digit numbers. She asks FILIP to compare those
 numbers, but instead of interpreting them with the leftmost most
 significant digit, he needs to interpret them the other way around, with
 the most significant digit being the rightmost one. He than has to tell the
 teacher the larger of the two numbers.

 Write a program that will check FILIP’s answers.

 Input:
    * The first and only line of input contains two three-digit numbers, A
      and B. A and B will not be equal and will not contain any zeroes.

 Output:
    * The first and only line of output should contain the larger of the
      numbers in the input, compared as described in the task. The number
      should be written reversed, to display to FILIP how he should read it.

 Example:
    Input:
    734 893

    Output:
    437

    Input:
    221 231

    Output:
    132

    Input:
    839 237

    Output:
    938

****************************************************************************/

#include <stdio.h>
#include <math.h>

#define MAX_NUM     2
#define MAX_DIGIT   3
#define BASE        10

int main () {
    int nums[MAX_NUM];      // array of two numbers
    int rev_nums[MAX_NUM];  // array of two reverse number
    int result = 0;         // the result of numbers

    scanf("%d %d", nums, nums+1);

    for (int i = 0; i < MAX_NUM; i++) {
        int temp = nums[i];         // take a number from nums
        int exp = MAX_DIGIT-1;      // the max exponent of three digits
        rev_nums[i] = 0;

        while (temp > 0) {
            rev_nums[i] += (temp % BASE) * pow(BASE, exp);
            exp--;
            temp /= BASE;
        }

        if (result < rev_nums[i]) {
            result = rev_nums[i];
        }
    }

    printf("%d\n", result);

    return 0;
}
