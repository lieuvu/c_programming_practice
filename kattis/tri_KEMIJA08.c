/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_KEMIJA08.c
 Example:     gcc -Wall -o prog tri_KEMIJA08.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Luka is fooling around in chemistry class again! Instead of balancing
 equations he is writing coded sentences on a piece of paper. Luka modifies
 every word in a sentence by adding, after each vowel (letters ’a’, ’e’,
 ’i’, ’o’ and ’u’), the letter ’p’ and then that same vowel again. For
 example, the word “kemija” becomes “kepemipijapa” and the word “paprika”
 becomes “papapripikapa”. The teacher took Luka’s paper with the coded
 sentences and wants to decode them.

 Write a program that decodes Luka’s sentence.

 Input:
    * The coded sentence will be given on a single line. The sentence
      consists only of lowercase letters of the English alphabet and spaces.
      The words will be separated by exactly one space and there will be no
      leading or trailing spaces. The total number of character will be at
      most 100.

 Output:
    * Output the decoded sentence on a single line.

 Example:
    Input:
    zepelepenapa papapripikapa

    Output:
    zelena paprika

    Input:
    bapas jepe doposapadnapa opovapa kepemipijapa

    Output:
    bas je dosadna ova kemija

****************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX_CHAR    101     // extra character for null-character

// --------------------------------
// Declaration of private functions
// --------------------------------

static bool IsVowel(char);

// -------------
// Main function
// -------------

int main () {
    char str[MAX_CHAR] = {0};           // encoded string
    char decoded_str[MAX_CHAR] = {0};   // decoded string

    // copy string from stdin up to MAX_CHAR-1 characters
    fgets(str, MAX_CHAR, stdin);

    int i = 0;                          // index of encoded string
    int j = 0;                          // index of decoded string
    while (str[i] != '\0') {
        // if meet a vowel
        if (IsVowel(str[i])) {
            decoded_str[j] = str[i];    // copy the vowel to decoded string
            i += 3;                     // skip two letters
            j++;                        // go to next index in decoded string
        } else {
            // if not meet a vowel
            decoded_str[j] = str[i];    // copy the consonant to decoded string
            i++;                        // go to next index in encoded string
            j++;                        // go to next index in decoded string
        }
    }

    printf("%s\n", decoded_str);

}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Check if a character is a vowel.

 @param c The character to check.

 @return true if the character is vowel,
 otherwise false.
 */
static bool IsVowel(char c) {
    char tmp = tolower(c);  // convert to lower case letter

    if (tmp == 'a' || tmp == 'e' || tmp == 'i' || tmp == 'o' || tmp == 'u') {
        return true;
    }

    return false;
}
