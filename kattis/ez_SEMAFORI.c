/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_SEMAFORI.c
 Example:     gcc -Wall -o prog ez_SEMAFORI.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty:2.0

 Luka is driving his truck along a long straight road with many traffic
 lights. For each traffic light he knows how long the red and green lights
 will be on (the cycle repeating endlessly).

 When Luka starts his journey, all traffic lights are red and just started
 their cycle. Luka moves one distance unit per second. When a traffic light
 is red, he stops and waits until it turns green.

 Write a program that determines how much time Luka needs to reach the end
 of the road. The start of the road is at distance zero, the end at
 distance L.

 Input:
    * The first line contains two integers N and L (1≤N≤100, 1≤L≤1000), the
      number of traffic lights on the road and the length of the road.
    * Each of the next N lines contains three integers D, R and G,
      describing one traffic light (1≤D<L, 1≤R≤100, 1≤G≤100). D is the
      distance of the traffic light from the start of the road. R and G
      denote how long the red and green lights are on, respectively.
    * The traffic lights will be ordered in increasing order of D. No two
      traffic lights will share the same position.

 Output:
    * Output the time (in seconds) Luka needs to reach the end of the road.

 Example:
    Input:
    2 10
    3 5 5
    5 2 2

    Output:
    12

    Input:
    4 30
    7 13 5
    14 4 4
    15 3 10
    25 1 1

    Output:
    36

****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define R 0             // Red
#define G 1             // Green

// -------------------
// Definition of data
// -------------------

/**
 @struct TrafficLight data structure.

 @field tfl_dist The distance between start and the traffic light.
 @field r_timer The red timer.
 @field g_timer The green timer.
 */
typedef struct TrafficLight {
    int tfl_dist;
    int r_timer;
    int g_timer;
} TrafficLight;

/**
 @struct LightInfo data structure

 @field l_color The color of the light (R or G)
 @field l_time The time for the light to be switched.
 */
typedef struct LightInfo {
    int l_color;
    int l_time;
} LightInfo;

// -------------------------------
// Declaration of private function
// -------------------------------

static LightInfo CurrentLightAtTrafficLights(TrafficLight, int);

// -------------
// Main function
// -------------

int main () {
    int num_tfl;            // number of traffic lights
    int road_len;           // the road length
    TrafficLight *tfl_arr;  // a pointer to TrafficLight

    // get number of traffic lights and road length
    scanf("%d %d", &num_tfl, &road_len);

    // allocate memory for tfl_arr
    tfl_arr = malloc(sizeof(*tfl_arr) * num_tfl);

    // get traffic lights info
    for (int i = 0; i < num_tfl; ++i) {
        int tfl_dist;
        int r_timer;
        int g_timer;

        scanf("%d %d %d", &tfl_dist, &r_timer, &g_timer);
        TrafficLight tfl = {tfl_dist, r_timer, g_timer};
        tfl_arr[i] = tfl;
    }

    int dist = 0;           // distance moved
    int tfl_passed = 0;     // the number of traffic light passed
    int time = 0;           // the time taken to move at distance

    // move to the first traffic light
    dist = tfl_arr[tfl_passed].tfl_dist;
    time = dist;

    while (dist != road_len) {
        LightInfo l_info;

        l_info = CurrentLightAtTrafficLights(tfl_arr[tfl_passed], time);

        if (l_info.l_color == R) {
            // if red -> wait till green
            time += l_info.l_time;
        }

        // pass a traffic light
        tfl_passed++;

        if (tfl_passed < num_tfl) {
            // if still have traffic light
            // move to next traffic light
            time += tfl_arr[tfl_passed].tfl_dist - dist;
            dist = tfl_arr[tfl_passed].tfl_dist;
        } else {
            // if no traffic light ahead
            // move to destination
            time += road_len - dist;
            dist = road_len;
        }
    }

    free(tfl_arr);  // deallocate memory for tfl_arr

    printf("%d", time);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Get the light info at a traffic light.

 @param self The struct TrafficLight.
 @param time The current time.

 @return The light info at the traffic light.
 */
static LightInfo CurrentLightAtTrafficLights(TrafficLight tf_light, int time) {
    int r_timer = tf_light.r_timer;
    int g_timer = tf_light.g_timer;
    int c_timer = r_timer + g_timer;    // cycle timer
    int l_timer = time % c_timer;       // light timer
    LightInfo l_info;                   // light info

    // if timer is less than r_timer
    if (l_timer < r_timer) {
        // the light is red
        l_info.l_color = R;
        // calculate the availbe red time
        l_info.l_time = r_timer - l_timer;
        return l_info;
    }

    // if timer is more than r_timer
    // the light is green
    l_info.l_color = G;
    // calculate the available green time
    l_info.l_time = c_timer - l_timer;
    return l_info;
}