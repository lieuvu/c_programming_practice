/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_CETVRTA.c
 Example:     gcc -Wall -o prog tri_CETVRTA.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Mirko needs to choose four points in the plane so that they form a
 rectangle with sides parallel to the axes. He has already chosen three
 points and is confident that he hasn’t made a mistake, but is having
 trouble locating the last point. Help him.

 Input:
    * Each of the three points already chosen will be given on a separate
      line. All coordinates will be integers between 1 and 1000.

 Output:
    * Output the coordinates of the fourth vertex of the rectangle.

 Example:
    Input:
    5 5
    5 7
    7 5

    Output:
    7 7

    Input:
    30 20
    10 10
    10 20

    Output:
    30 10

****************************************************************************/

#include <stdio.h>
#include <stdbool.h>

#define MAX_C   2   // max coordinates of x and y
#define MAX_V   3   // max given vertices

// -------------------
// Definition of data
// -------------------

/**
 @struct Vertex data structure

 @field x The x coordinate
 @field y The y coordinate
 */
typedef struct Vertex {
    int x;
    int y;
} Vertex;


// --------------------------------
// Declaration of private functions
// --------------------------------

static void AddCoordinate(int, int, int *, int *, int);
static bool HasVertex(Vertex *, Vertex);

// -------------
// Main function
// -------------

int main () {
    int xs[MAX_C];      // array of x coordinates
    int ys[MAX_C];      // array of y coordinates
    Vertex vs[MAX_V];   // array of vertices

    for (int i = 0; i < 3; ++i) {
        scanf("%d %d", &(vs[i].x), &(vs[i].y));
        AddCoordinate(vs[i].x, vs[i].y, xs, ys, i);
    }

    for (int i = 0; i < MAX_C; ++i) {
        for (int j = 0; j < MAX_C; ++j) {
            Vertex tmp = {xs[i], ys[j]};
            if (!HasVertex(vs, tmp)) {
                printf("%d %d\n", tmp.x, tmp.y);
            }
        }
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Add coordinate to x coordinate array and y coordinate array.

 @param x The x coordinate to add.
 @param y The y coordinate to add.
 @param xs The x coordinate array.
 @param ys The y cooridnate array.
 @param time The flag to indicate if the first coordinates
 are added.
 */
static void AddCoordinate(int x, int y, int *xs, int *ys, int time) {
    if (time == 0) {
        xs[0] = x;
        ys[0] = y;
    }

    if (xs[0] != x) {
        xs[1] = x;
    }

    if (ys[0] != y) {
        ys[1] = y;
    }
}

/**
 Check if an array of vertices has a vertex.

 @param vs The array of vertices.
 @param v  The vertex to check.

 @return true if found a vertex in the array, otherwise false.
 */
static bool HasVertex(Vertex *vs, Vertex v) {
    for (int i = 0; i < MAX_V; ++i) {
        if (vs[i].x == v.x && vs[i].y == v.y) {
            return true;
        }
    }

    return false;
}