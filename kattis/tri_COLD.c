/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_COLD.c
 Example:     gcc -Wall -o prog tri_COLD.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.2

 We’re not going to sugar-coat it: Chicago’s winters can be rough. The
 temperatures sometimes dip to uncomfortable levels and, after last year’s
 “polar vortex”, the University of Chicago Weather Service wants to find out
 exactly how bad the winter was. More specifically, they are interested in
 knowing the total number of days in which the temperature was below zero
 degrees Celsius.

 Input:
    * The input is composed of two lines. The first line contains a single
      positive integer n (1≤n≤100) that specifies the number of temperatures
      collected by the University of Chicago Weather Service. The second
      line contains n temperatures, each separated by a single space. Each
      temperature is represented by an integer t (−1000000≤t≤1000000).

 Output:
    * You must print a single integer: the number of temperatures strictly
      less than zero.

 Example:
    Input:
    3
    5 -10 15

    Output:
    1

    Input:
    5
    -14 -5 -39 -5 -7

    Output:
    5

****************************************************************************/

#include <stdio.h>

int main () {
    int num_temp;       // number of temperature observations
    int check_temp;     // a temperature to check
    int count = 0;      // count temperature less than 0

    scanf("%d", &num_temp);

    while (num_temp) {
        scanf("%d", &check_temp);

        // if checked temperature is less than zero
        // increase the count
        if (check_temp < 0) count++;

        num_temp -= 1;  // decrease the number of observations
    }

    printf("%d", count);

    return 0;
}