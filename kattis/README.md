# Introduction
---
This directory holds solved problems in [kattis](https://open.kattis.com).

# Structure
---
Files in this directory are named with the format: `<level>_<problem_code>.c` where

* level: the level of the problem
    * `tri`: trivial [1..1.5)
    * `ez`: easy [1.5..3)
    * `med`: medium [3..5.9)
    * `hd`: hard [5.9..10]

* problem_code: the problem code in codechef for reference.

**Examples:**
    ez_TEST.c: the problem with difficulty is [1.5..3) and problem code TEST.

