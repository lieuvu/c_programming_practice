/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_SERVER.c
 Example:     gcc -Wall -o prog ez_SERVER.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 You are in charge of a server that needs to run some submitted tasks on a
 first-come, first-served basis. Each day, you can dedicate the server to
 run these tasks for at most T minutes. Given the time each task takes, you
 want to know how many of them will be finished today.

 Consider the following example. Assume T=180 and the tasks take 45,30,55,
 20,80, and 20 minutes (in order they are submitted). Then, only four tasks
 can be completed. The first four tasks can be completed because they take
 150 minutes, but not the first five, because they take 230 minutes which is
 greater than 180. Notice that although there is enough time to perform the
 sixth task (which takes 20 minutes) after completing the fourth task, you
 cannot do that because the fifth task is not done yet.


 Input:
    * The input consists of a single test case. The first line contains two
      integers n and T where 1≤n≤50 is the number of tasks and 1≤T≤500. The
      next line contains n positive integers no more than 100 indicating how
      long each task takes in order they are submitted.

 Output:
    * Display the number of tasks that can be completed in T minutes on a
      first-come, first-served basis.

 Example:
    Input:
    6 180
    45 30 55 20 80 20

    Output:
    4

    Input:
    10 60
    20 7 10 8 10 27 2 3 10 5

    Output:
    5

****************************************************************************/

#include <stdio.h>

int main () {
    int task_num = 0;   // number of tasks
    int time = 0;       // time constraint

    scanf("%d %d", &task_num, &time);

    // receiving tasks
    int i;
    for (i = 0; i < task_num; ++i) {
        int task_time = 0;      // time taken by current task

        scanf("%d", &task_time);
        time -= task_time;

        // if time left is negative
        // stop receiving tasks
        if (time < 0) break;
    }

    printf("%d\n", i);  // print number of task completed

    return 0;
}
