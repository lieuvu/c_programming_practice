/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_DICECUP.c
 Example:     gcc -Wall -o prog tri_DICECUP.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Note:
    * See pic_problems/tri_DICECUP.pdf for visual presentation of the
      problem.

 In many table-top games it is common to use different dice to simulate
 random events. A “d” or “D” is used to indicate a die with a specific
 number of faces, d4 indicating a four-sided die, for example. If several
 dice of the same type are to be rolled, this is indicated by a leading
 number specifying the number of dice. Hence, 2d6 means the player should
 roll two six-sided dice and sum the result face values.

 Write a program to compute the most likely outcomes for the sum of two
 dice rolls. Assume each die has numbered faces starting at 1 and that
 each face has equal roll probability.

 Input:
    * The input consists of a single line with two integer numbers, N,M,
      specifying the number of faces of the two dice.
    * 4≤N,M≤20 Number of faces.

 Output:
    * A line with the most likely outcome for the sum; in case of several
      outcomes with the same probability, they must be listed from lowest
      to highest value in separate lines.

 Example:
    Input:
    6 6

    Output:
    7

    Input:
    6 4

    Output:
    5
    6
    7

    Input:
    12 20

    Output:
    3
    14
    15
    16
    17
    18
    19
    20
    21

****************************************************************************/

#include <stdio.h>

#define MAX_F 41            // max faces
#define START 2             // start point of the frequency table

int main () {
    int freq[MAX_F] = {0};  // frequency table
    int d1;                 // number of face of dice 1
    int d2;                 // number of face of dice 2
    int max_fq;             // max frequencies

    scanf("%d %d", &d1, &d2);

    // from frequency table
    for (int i = 1; i <= d1; ++i) {
        for (int j = 1; j <= d2; ++j) {
            freq[i+j]++;
        }
    }

    // find max frequency in frequency table
    max_fq = freq[START];
    for (int i = START+1; i < MAX_F; ++i) {
        if (freq[i] > max_fq) {
            max_fq = freq[i];
        }
    }

    // list all numbers with max frequency in frequency table
    for (int i = 2; i < MAX_F; ++i) {
        if (freq[i] == max_fq) {
            printf("%d\n", i);
        }
    }

    return 0;
}