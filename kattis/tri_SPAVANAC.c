/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_SPAVANAC.c
 Example:     gcc -Wall -o prog tri_SPAVANAC.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Every school morning Mirko is woken up by the sound of his alarm clock.
 Since he is a bit forgetful, quite often he leaves the alarm on on Saturday
 morning too. That’s not too bad tough, since he feels good when he realizes
 he doesn’t have to get up from his warm and cozy bed.

 He likes that so much, that he would like to experience that on other days
 of the week too! His friend Slavko offered this simple solution: set his
 alarm clock 45 minutes early, and he can enjoy the comfort of his bed, fully
 awake, for 45 minutes each day.

 Mirko decided to heed his advice, however his alarm clock uses 24-hour
 notation and he has issues with adjusting the time. Help Mirko and write a
 program that will take one time stamp, in 24-hour notation, and print out a
 new time stamp, 45 minutes earlier, also in 24-hour notation.

 If you are unfamiliar with 24-hour time notation yourself, you might be
 interested to know it starts with 0:00 (midnight) and ends with 23:59
 (one minute before midnight).

 Input:
    * The first and only line of input will contain exactly two integers H
      and M (0≤H≤23,0≤M≤59) separated by a single space, the input time in
      24-hour notation. H denotes hours and M minutes.

 Output:
    * The first and only line of output should contain exactly two integers,
      the time 45 minutes before input time.

 Example:
    Input:
    10 10

    Output:
    9 25

    Input:
    0 30

    Output:
    23 45

    Input:
    23 40

    Output:
    22 55

****************************************************************************/

#include <stdio.h>

#define BASE_H 24   // base hour
#define BASE_M 60   // base minute

// -------------------
// Definition of data
// -------------------

/**
 @struct Struct Time24

 @field h The hour.
 @field m The minute.
 */
typedef struct Time24 {
    int h;  // hour
    int m;  // minute
} Time24;

// --------------------------------
// Declaration of private functions
// --------------------------------

static Time24 MinusTwoTimeStamps(Time24, Time24);

// -------------
// Main function
// -------------

int main () {
    Time24 time;
    Time24 alarm_timer = {0, 45};
    Time24 result;

    scanf("%d %d", &time.h, &time.m);

    result = MinusTwoTimeStamps(time, alarm_timer);

    printf("%d %d", result.h, result.m);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Minus two time stamps time1 and time2.

 @param time1 Time stamp 1.
 @param time2 Time stamp 2.

 @return The different time.
 */
static Time24 MinusTwoTimeStamps(Time24 time1, Time24 time2) {
    Time24 result;
    int carry = 0;

    if (time1.m < time2.m) {
        result.m = time1.m + BASE_M - time2.m;
        carry++;
    } else {
        result.m = time1.m - time2.m;
    }

    if (time1.h < (time2.h + carry)) {
        result.h = time1.h + BASE_H - (time2.h + carry);
    } else {
        result.h = time1.h - (time2.h + carry);
    }

    return result;
}