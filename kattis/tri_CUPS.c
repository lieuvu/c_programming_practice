/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_CUPS.c
 Example:     gcc -Wall -o prog tri_CUPS.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 You are programming a cup stacking module for your robot. This robot is
 equiped with several sensors that can accurately determine the radius and
 color of a cup. The problem is that there is a glitch in the robot’s core
 routine that processess sensor inputs so the radius is doubled, if the
 result of the color sensor arrives to the routine after the radius.

 For instance, for a red cup with a radius of 5 units, your module will
 receive either “red 5” or “10 red” message.

 Given a list of messages from the core routine, each describing a different
 cup, can you put the cups in order of the smallest to the largest?

 Input:
    * The first line of the input file contains an integer N, the number of
      cups (1≤N≤20). Next N lines will contain two tokens each, either as
      “color radius” or “diameter color”. The radius of a cup R will be a
      positive integer less than 1000. The color of a cup C will be a
      non-empty string of lower case English letters of length at most 20.
      All cups will be different in both size and color.

 Output:
    * Output colors of cups, one color per line, in order of increasing
      radius.

 Example:
    Input:
    3
    red 10
    10 blue
    green 7

    Output:
    blue
    green
    red

****************************************************************************/

#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#define CHAR_MAX    20  // max characters of color string
#define BASE        10
#define CUP_MAX     20  // max number of cups

// -------------------
// Definition of data
// -------------------

/**
 @struct Cup data structure

 @field color The color of the cup.
 @field r     The radius of the cup.
 */
typedef struct Cup {
    char color[CHAR_MAX];
    double r;
} Cup;

// -------------------------------
// Declaration of private function
// -------------------------------

static void SortCup(Cup *, int);

// -------------
// Main function
// -------------

int main () {
    int cups_num;       // number of cups
    Cup cups[CUP_MAX];  // array of cups

    scanf("%d", &cups_num);

    for (int i = 0; i < cups_num; ++i) {
        char str1[CHAR_MAX] = {0};
        char str2[CHAR_MAX] = {0};
        char *endptr;
        Cup cup;

        scanf("%s %s", str1, str2);

        // if str2 can be converted to integer number
        if (strtoimax(str2, &endptr, BASE) != 0) {
            // str1 is color, str2 is radius
            strcpy(cup.color, str1);
            cup.r = (double)strtoimax(str2, &endptr, BASE);
            cups[i] = cup;

        // otherwise str1 is diameter, str2 is color
        } else {
            strcpy(cup.color, str2);
            cup.r = (double)strtoimax(str1, &endptr, BASE)/2;
            cups[i] = cup;
        }

    }

    SortCup(cups, cups_num);

    for (int i = 0; i < cups_num; ++i) {
        printf("%s\n", cups[i].color);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Sort cups based on radius

 @param cups The array of cups.
 @param size The number of cups.
 */
static void SortCup(Cup *cups, int size) {
    for (int i = 1; i < size; ++i) {
        for (int j = i; j > 0 && cups[j].r < cups[j-1].r; --j) {
            Cup tmp = cups[j];
            cups[j] = cups[j-1];
            cups[j-1] = tmp;
        }
    }
}
