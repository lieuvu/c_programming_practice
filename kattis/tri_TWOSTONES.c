/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_TWOSTONES.c
 Example:     gcc -Wall -o prog ez_TWOSTONES.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Alice and Bob are playing a new game of stones. There are N stones placed
 on the ground, forming a sequence. The stones are labeled from 1 to N.

 Alice and Bob in turns take exactly two consecutive stones on the ground
 until there are no consecutive stones on the ground. That is, each player
 can take stone i and stone i+1, where 1≤i≤N−1. If the number of stone
 left is odd, Alice wins. Otherwise, Bob wins.

 Assume both Alice and Bob play optimally and Alice plays first, do you
 know who the winner is?

 Input:
    * The input contains an integer N (1≤N≤10000000), the number of stones.

 Output:
    * Output the winner, “Alice” or “Bob” (without the quotes), on a line.

 Example:
    Input:
    1

    Output:
    Alice

    Input:
    2

    Output:
    Bob

    Input:
    5

    Output
    Alice

****************************************************************************/

#include <stdio.h>

#define ALICE  "Alice"
#define BOB    "Bob"

int main () {
    int num_of_stones;

    if (scanf("%d", &num_of_stones) == 1) {
        // get the number of stone
         if (num_of_stones %2) {
            printf("%s", ALICE);
        } else {
            printf("%s", BOB);
        }
    } else {
        printf("Read error!");
        return 1;
    }

    return 0;
}