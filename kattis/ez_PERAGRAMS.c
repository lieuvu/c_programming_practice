/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_PERAGRAMS.c
 Example:     gcc -Wall -o prog ez_PERAGRAMS.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 Per recently learned about palindromes. Now he wants to tell us about it
 and also has more awesome scientific news to share with us.

 “A palindrome is a word that is the same no matter whether you read it
 backward or forward”, Per recently said in an interview. He continued: “For
 example, add is not a palindrome, because reading it backwards gives dda
 and it’s actually not the same thing, you see. However, if we reorder the
 letters of the word, we can actually get a palindrome. Hence, we say that
 add is a Peragram, because it is an anagram of a palindrome”.

 Per gives us a more formal definition of Peragrams: “Like I said, if a word
 is an anagram of at least one palindrome, we call it a Peragram. And recall
 that an anagram of a word w contains exactly the same letters as w,
 possibly in a different order.”

 Input:
    * Given a string, find the minimum number of letters you have to remove
      from it, so that the string becomes a Peragram.

 Output:
    * Input consists of a string on a single line. The string will contain
      at least 1 and at most 1000 characters. The string will only contain
      lowercase letters a-z.

 Example:
    Input:
    abc

    Output:
    2

    Input:
    aab

    Output:
    0

****************************************************************************/

/****************************************************************************
 * Discussion:
    * It is easy to see that the peragram must consist:
        + either only one odd number of a charcter and the rest must be even
          number of characters.
        + or only even number of characters.

****************************************************************************/

#include <stdio.h>

#define MAX_CHAR         1001    // max char of string (null-terminating char)
#define MAX_LETTER       26      // max number of letters from a to z
#define START_LET_CODE   97      // start aschii code of letter a

int main () {
    char str[MAX_CHAR] = {0};           // input string
    int l_count[MAX_LETTER] = {0};      // count of each letters in string
    int odd_num_of_char = 0;            // odd number of chars
    int i = 0;                          // index of string
    int removed_char = 0;               // number of removed char

    scanf("%s", str);

    while (str[i]) {
        l_count[str[i]-START_LET_CODE]++;
        i++;
    }

    for (int i = 0; i < MAX_LETTER; ++i) {

        // if a string containts one odd number of certain char
        // and a current number of chars is odd
        if (odd_num_of_char > 0 && l_count[i] % 2 != 0 && l_count[i] != 0) {
            removed_char++; // remove one char for a current number of chars
        }

        // if the string containts the odd number of a character
        if (odd_num_of_char == 0 && l_count[i] % 2 != 0) {
            // set the flag to true
            odd_num_of_char++;
        }
    }

    printf("%d\n", removed_char);

    return 0;
}
