/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_RIJECI.c
 Example:     gcc -Wall -o prog ez_RIJECI.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 One day, little Mirko came across a funny looking machine! It consisted of
 a very very large screen and a single button. When he found the machine,
 the screen displayed only the letter A. After he pressed the button, the
 letter changed to B. The next few times he pressed the button, the word
 transformed from B to BA, then to BAB, then to BABBA... When he saw this,
 Mirko realized that the machine alters the word in a way that all the
 letters B get transformed to BA and all the letters A get transformed to
 B.

 Amused by the machine, Mirko asked you a very difficult question! After K
 times of pressing the button, how many letters A and how much letters B
 will be displayed on the screen?

 Input:
    * The first line of input contains the integer K (1≤K≤45), the number of
      times Mirko pressed the button.

 Output:
    * The first and only line of output must contain two space-separated
      integers, the number of letters A and the number of letter B.

 Example:
    Input:
    1

    Output:
    0 1

    Input:
    4

    Output:
    2 3

    Input:
    10

    Output:
    34 55

****************************************************************************/

#include <stdio.h>

// ------------------
// Definition of data
// ------------------

/**
 @struct LetterCount data structure

 @field a The number of letter A.
 @field b The number of letter B.
 */
typedef struct LetterCount {
    int a;
    int b;
} LetterCount;

// --------------------------------
// Declaration of private functions
// --------------------------------

static LetterCount CountLetters(int);

// -------------
// Main function
// -------------

int main () {
    int k;                         // number of times button is pressed
    LetterCount l_count = {0};     s// letter count

    scanf("%d", &k);

    l_count = CountLetters(k);

    printf("%d %d\n", l_count.a, l_count.b);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Count letters after number of k times
 the button is pressed

 @param k The number of times the button is pressed.
 @return The LetterCount data structure.
 */
static LetterCount CountLetters(int k) {

    if (k == 0) {
        return (LetterCount){1,0};
    }

    if (k == 1) {
        return (LetterCount){0,1};
    }

    LetterCount prev_l_count = CountLetters(k-1);   // get previous count

    return (LetterCount){prev_l_count.b, prev_l_count.a + prev_l_count.b};
}
