/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_ACM.c
 Example:     gcc -Wall -o prog tri_ACM.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Our new contest submission system keeps a chronological log of all
 submissions made by each team during the contest. With each entry, it
 records the number of minutes into the competition at which the submission
 was received, the letter that identifies the relevant contest problem, and
 the result of testing the submission (designated for the sake of this
 problem simply as right or wrong). As an example, the following is a
 hypothetical log for a particular team:

 3 E right
 10 A wrong
 30 C wrong
 50 B wrong
 100 A wrong
 200 A right
 250 C wrong
 300 D right

 The rank of a team relative to others is determined by a primary and
 secondary scoring measure calculated from the submission data. The primary
 measure is the number of problems that were solved. The secondary measure
 is based on a combination of time and penalties. Specifically, a team’s
 time score is equal to the sum of those submission times that resulted in
 right answers, plus a 20-minute penalty for each wrong submission of a
 problem that is ultimately solved. If no problems are solved, the time
 measure is 0.

 In the above example, we see that this team successfully completed three
 problems: E on their first attempt (3 minutes into the contest); A on their
 third attempt at that problem (200 minutes into the contest); and D on
 their first attempt at that problem (300 minutes into the contest). This
 team’s time score (including penalties) is 543. This is computed to include
 3 minutes for solving E, 200 minutes for solving A with an additional 40
 penalty minutes for two earlier mistakes on that problem, and finally 300
 minutes for solving D. Note that the team also attempted problems B and C,
 but were never successful in solving those problems, and thus received no
 penalties for those attempts.

 According to contest rules, after a team solves a particular problem, any
 further submissions of the same problem are ignored (and thus omitted from
 the log). Because times are discretized to whole minutes, there may be more
 than one submission showing the same number of minutes. In particular there
 could be more than one submission of the same problem in the same minute,
 but they are chronological, so only the last entry could possibly be
 correct. As a second example, consider the following submission log:

 7 H right
 15 B wrong
 30 E wrong
 35 E right
 80 B wrong
 80 B right
 100 D wrong
 100 C wrong
 300 C right
 300 D wrong

 This team solved 4 problems, and their total time score (including
 penalties) is 502, with 7 minutes for H, 35+20 for E, 80+40 for B, and
 300+20 for C.

 Input:
    * The input contains n lines for 0≤n≤100, with each line describing a
      particular log entry. A log entry has three parts: an integer m, with
      1≤m≤300, designating the number of minutes at which a submission was
      received, an uppercase letter designating the problem, and either the
      word right or wrong. The integers will be in nondecreasing order and
      may contain repeats. After all the log entries is a line containing
      just the number −1.

 Output:
    * Output two integers on a single line: the number of problems solved
      and the total time measure (including penalties).

 Example:
    Input:
    3 E right
    10 A wrong
    30 C wrong
    50 B wrong
    100 A wrong
    200 A right
    250 C wrong
    300 D right
    -1

    Output:
    3 543

    Input:
    7 H right
    15 B wrong
    30 E wrong
    35 E right
    80 B wrong
    80 B right
    100 D wrong
    100 C wrong
    300 C right
    300 D wrong
    -1

    Output:
    4 502

****************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define MAX_LOG         100         // max number of logs
#define MAX_S           26          // max solutions in the problem set
#define MAX_TEST_CHAR   6           // max chars of test result (null-ter char)
#define RIGHT           "right"     // right
#define PENALTY         20          // penalty minutes for wrong solution

// ------------------
// Definition of data
// ------------------

/**
 @struct Log data structure

 @field m The minute of the submission.
 @field p The problem to solve.
 @field r RIGHTesult of the submission.
 */
typedef struct Log {
    int m;
    char p;
    char r[MAX_TEST_CHAR];
} Log;

/**
 @struct Solution data structure

 @field m The minute to solve the problem.
 @field p The problem to solve.
 @field t The attempted times to solve the problem.
 */
typedef struct Solution {
    int m;
    char p;
    int t;
} Solution;

// -------------------------------
// Declaration of private funtions
// -------------------------------

static void UpdateSolutionsStatus(Solution *, Log);
static bool IsProblemAttempted(Solution *, Log);
static int CountAttemptedSolutions(Solution *);
static void PrintResult(Solution *);
static void PrintSolution(Solution *);

// -------------
// Main function
// -------------

int main () {
    Log logs[MAX_LOG] = {0};        // logs
    Solution sols[MAX_S] = {0};     // solutions
    int l_idx = 0;                  // log index
    int m = 0;                      // submission minute
    char p = 0;                     // attempted problem
    char r[MAX_TEST_CHAR] = {0};    // result of submission

    do {
        scanf("%d %c %s", &m, &p, r);

        if (m < 0) break;               // stop reading log if m equals -1

        /* otherwise having submission log */

        logs[l_idx].m = m;
        logs[l_idx].p = p;
        strcpy(logs[l_idx].r, r);

        UpdateSolutionsStatus(sols, logs[l_idx]);

        l_idx++;    // next log

    } while (m > 0);

    PrintResult(sols);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Update the solution status when receiving submission.

 @param sols The array of solutions.
 @param log  The log of submission info.
 */
static void UpdateSolutionsStatus(Solution *sols, Log log) {
    int att_sols;   // recorded solutions

    /* if the problem is not attemped */
    if (!IsProblemAttempted(sols, log)) {
        att_sols = CountAttemptedSolutions(sols); // count attempted solutions

        sols[att_sols].p = log.p;               // record the problem

        /* if the problem solved from submission */
        if (strcmp(log.r, RIGHT) == 0) {
            sols[att_sols].m = log.m;           // record the minute
            sols[att_sols].t++;                 // increase the attempted time

        /* otherwise the problem not solved from submission */
        } else {
            sols[att_sols].t++;                 // increase the attempted time
        }

    /* otherwise the problem has been attemped */
    } else {

        att_sols = CountAttemptedSolutions(sols); // count attempted solutions

        for (int i = 0; i < att_sols ; ++i) {
            // if attempted solutions having different
            // problem to the submission, go to next solution
            if (sols[i].p != log.p) continue;

            /* otherwise attempted solution having the same
               problem with the submission */

            /* if solution is reached, stop updating */
            if (sols[i].m > 0) break;

            /* otherwise solution is not reached */

            /* if the problem solved from submission */
            if (strcmp(log.r, RIGHT) == 0) {
                sols[i].m = log.m;  // record the minute
                sols[i].t++;        // increase the attempted time

            /* otherwise the problem not solved from submission */
            } else {
                sols[i].t++;        // increase the attempted time
            }
        }
    }
}

/**
 Check is a problem is attemped in solutions.

 @param sols The solution recorded.
 @param log  The log of submission info.

 @return true if the problem is attempted, otherwise false.
 */
static bool IsProblemAttempted(Solution *sols, Log log) {
    for (int i = 0; i < MAX_S; ++i) {
        if (sols[i].p == log.p) return true;
    }

    return false;
}

/**
 Count number of attempted solutions.

 @param sols The attempted solutions.

 @return The number of the attemped solutions.
 */
static int CountAttemptedSolutions(Solution *sols) {
    int count = 0;

    for (int i = 0; i < MAX_S; ++i) {
        // if solution is not attempted
        if (!sols[i].p) {
            break;  // stop counting
        }
        else {
            count++;    // increase count
        }
    }

    return count;
}

/**
 Print result based on the solutions.

 @param sols The solutions recorded.
 */
static void PrintResult(Solution *sols) {
    int solved_p = 0;   // solved problems
    int score = 0;      // sccores

    for (int i = 0; i < MAX_S; ++i) {
        // if solution is not attempted
        // stop printing
        if (!sols[i].p) break;

        /* otherwise solution is attempted */

        // if solution is reached
        if (sols[i].m) {
            solved_p++;             // increase soved problems
            score += sols[i].m;     // add the minute of solving the problem

            // add penalties minutes
            score += PENALTY * (sols[i].t - 1);
        }
    }

    printf("%d %d\n", solved_p, score);
}

/**
 Print solutions for debugginf purpose.

 @param sols The solutions recorded.
 */
static void PrintSolution(Solution *sols) {
    for (int i = 0; i < MAX_S; ++i) {
        if (sols[i].p) {
            printf("m: %d p: %c t: %d\n", sols[i].m, sols[i].p, sols[i].t);
        }
    }
}
