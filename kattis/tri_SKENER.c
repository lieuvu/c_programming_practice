/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_SKENER.c
 Example:     gcc -Wall -o prog tri_SKENER.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Having solved the match issue, Mirko faced another challenging problem. His
 mom ordered him to read an article about the newest couples on the Croatian
 show-biz scene in the “Moja Tajna” magazine. The article is written in a
 very small font size which Mirko cannot read. Luckily, Mirko has a scanner
 in the closet which will enlarge the article for him.

 The article is a matrix of characters with R rows and C columns. The
 characters are letters of the English alphabet, digits and the character
 ‘.’ (period). Mirko’s scanner supports two parameters, ZR and ZC. It
 substitutes each character it scans with a matrix of ZR rows and ZC
 columns, where all entries are equal to the scanned character.

 Mirko realized his scanner’s software no longer works so he needs your
 help.

 Input:
    * The first row contains four integers, R, C, ZR and ZC. R and C are
      between 1 and 50, while ZR and ZC are between 1 and 5.
    * Each of the following R rows contains C characters each, representing
      the article from “Moja Tajna”.

 Output:
    * The output should be a matrix of characters with R⋅ZR rows and C⋅ZC
      columns, the enlarged article.

 Example:
    Input:
    3 3 1 2
    .x.
    x.x
    .x.

    Output:
    ..xx..
    xx..xx
    ..xx..

    Input:
    3 3 2 1
    .x.
    x.x
    .x.

    Output:
    .x.
    .x.
    x.x
    x.x
    .x.
    .x.

****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main () {
    int row;        // number of rows
    int col;        // number of colums
    int z_row;      // ZR
    int z_col;      // ZC
    char **article; // article
    char *output;  // output

    scanf("%d %d %d %d", &row, &col, &z_row, &z_col);

    // allocate memory for the article
    article = malloc(sizeof(*article) * row);

    // allocate memory for each row of the artilce
    // and read each row of article
    for (int i = 0; i < row; ++i) {
        article[i] = calloc(col+1, sizeof(**article));
        scanf("%s", article[i]);
    }

    // iterate through each row of the article
    // and print the output
    for (int i = 0; i < row*z_row; ++i) {
        output = calloc(col*z_col+1, sizeof(*output));
        for (int j = 0; j < col*z_col; ++j) {
            output[j] = article[i/z_row][j/z_col];
        }
        printf("%s\n", output);
        free(output);
    }

    // deallocate memory of each row of the article
    for (int i = 0; i < row; ++i) {
        free(article[i]);
    }

    // deallocate memory of the article
    free(article);

    return 0;
}
