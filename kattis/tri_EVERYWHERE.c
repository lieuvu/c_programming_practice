/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_EVERYWHERE.c
 Example:     gcc -Wall -o prog tri_EVERYWHERE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Alice travels a lot for her work. Each time she travels, she visits a
 single city before returning home.

 Someone recently asked her “how many different cities have you visited for
 work?” Thankfully Alice has kept a log of her trips. Help Alice figure out
 the number of cities she has visited at least once.

 Input:
    * The first line of input contains a single positive integer T≤50
      indicating the number of test cases. The first line of each test
      case also contains a single positive integer n indicating the number
      of work trips Alice has taken so far. The following n lines describe
      these trips. The ith such line simply contains the name of the city
      Alice visited on her i th trip.
    * Alice’s work only sends her to cities with simple names: city names
      only contain lowercase letters, have at least one letter, and do not
      contain spaces.
    * The number of trips is at most 100 and no city name contains more
      than 20 characters.

 Output:
    * For each test case, simply output a single line containing a single
      integer that is the number of distinct cities that Alice has visited
      on her work trips.

 Example:
    Input:
    2
    7
    saskatoon
    toronto
    winnipeg
    toronto
    vancouver
    saskatoon
    toronto
    3
    edmonton
    edmonton
    edmonton

    Output:
    4
    1

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_TRIP    100
#define MAX_CHAR    100

// ------------------------
// Declaration of functions
// ------------------------

static void AddCityToVisited(char [][MAX_CHAR], int *, char *);

// -------------
// Main function
// -------------

int main () {
    int num_test;       // a number of test
    int num_trip;       // a number of trip

    scanf("%d", &num_test);

    while (num_test) {
        char visited[MAX_TRIP][MAX_CHAR];
        char city[MAX_CHAR];
        int idx = 0;    // index of visited

        scanf("%d", &num_trip);

        for (int i = 0; i < num_trip; ++i) {
            scanf("%s", city);
            AddCityToVisited(visited, &idx, city);
        }

        printf("%d\n", idx);

        num_test -= 1;  // decrease the number of test
    }

    return 0;
}

// --------------------------
// Implementation of function
// --------------------------

/**
 Add city to visited city array if the city
 is not visited

 @param visited The visited city array.
 @param idx The pointer to current index of the array.
 @param city The name of the city to add.
 */
static void AddCityToVisited(char visited[][MAX_CHAR], int *idx, char *city) {
    // if no visited city
    if (*idx == 0) {
        strcpy(visited[*idx], city);    // add first city
        (*idx)++;                       // increase the index
    } else {
        int result_cmp = 1;             // result compare

        for (int i = 0; i < *idx; ++i) {
            // if the city is visited
            if ((result_cmp = strcmp(visited[i], city)) == 0) {
                return;  // stop checking and return
            }
        }

        // if city is not visited
        if (result_cmp != 0) {
            strcpy(visited[*idx], city);    // add city
            (*idx)++;                       // increase the current index
        }
    }
}