/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_TRIK.c
 Example:     gcc -Wall -o prog tri_TRIK.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Note:
    * See pic_problems/tri_TRIK.pdf for visual presentation of the problem.

 Jealous of Mirko’s position as head of the village, Borko stormed into his
 tent and tried to demonstrate Mirko’s incompetence for leadership with a
 trick.

 Borko puts three opaque cups onto the table next to each other (opening
 facing down) and a small ball under the leftmost cup. He then swaps two
 cups in one of three possible ways a number of times. Mirko has to tell
 which cup the ball ends up under.

 Wise Mirko grins with his arms crossed while Borko struggles to move the
 cups faster and faster. What Borko does not know is that programmers in
 the back are recording all his moves and will use a simple program to
 determine where the ball is. Write that program.

 Input:
    * The first and only line contains a non-empty string of at most 50
      characters, Borko’s moves. Each of the characters is ‘A’, ‘B’ or ‘C’
      (without quote marks).

 Output:
    * Output the index of the cup under which the ball is: 1 if it is under
      the left cup, 2 if it is under the middle cup or 3 if it is under the
      right cup.

 Example:
    Input:
    AB

    Output:
    1

    Input:
    CBABCACCC

    Output:
    1

****************************************************************************/

#include <stdio.h>

#define MAX_MOVE    55
#define LEFT        1
#define MID         2
#define RIGHT       3

int main () {
    int result_idx;               // the index of the cup holding the ball
    char moves[MAX_MOVE] = {0};   // the moves
    int idx;                      // move index

    scanf("%s", moves);     // get all the moves

    idx = 0;                // set first move index
    result_idx = LEFT;      // put the ball to the left most cup

    while (moves[idx]) {
        switch (moves[idx]) {
            case 'A':
                if (result_idx == LEFT) {
                    result_idx = MID;
                } else if (result_idx == MID) {
                    result_idx = LEFT;
                }
                break;
            case 'B':
                if (result_idx == MID) {
                    result_idx = RIGHT;
                } else if (result_idx == RIGHT) {
                    result_idx = MID;
                }
                break;
            case 'C':
                if (result_idx == RIGHT) {
                    result_idx = LEFT;
                } else if (result_idx == LEFT) {
                    result_idx = RIGHT;
                }
                break;
        }

        idx++;      // increase the move index
    }

    printf("%d", result_idx);

    return 0;
}