/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_SIDEWAYSSORTING.c
 Example:     gcc -Wall -o prog ez_SIDEWAYSSORTING.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 2.0

 We are so accustomed to reading strings from left to right, it can be a
 little difficult to think of strings that go right to left or even
 vertically. Here’s your chance to get some practice.

 Input:
    * Input consists of up to 100 test cases. Each test case starts with
      a line containing a pair of integers, 1≤r,c≤15. This is followed by
      r lines each containing c characters chosen from A–Z (lowercase and
      uppercase). The last test case is followed by values of 0 for both
      r and c.

 Output:
    * For each test case, print out a block of text containing the same
      columns as the input block but in sorted order. Think of each column
      as a string reading from top to bottom. Your output block should have
      the columns sorted lexicographically from left to right. Your sort
      should be a stable sort and it should ignore case. Print a blank line
      between each pair of adjacent output blocks.

 Example:
    Input:
    3 3
    oTs
    nwi
    eox
    3 4
    xAxa
    yByb
    zCyc
    0 0

    Output:
    osT
    niw
    exo

    Aaxx
    Bbyy
    Ccyz

 Explanation:
    * It is easily confused with the problem sorting string case insensitive
    in each line, actually only the first row is sorted in that way and
    the following rows just sorted based on the first row.

****************************************************************************/

/************************************************
 Notes:
    * If passing a pointer as parameter, a memory
    block holding a pointer will not be passed but
    another memory block holding the same pointer.
    * When allocate memory for a string of
    length n, we should allocate a memory block
    of n+1 size to hold one extra '\0' character.
    For example: a string "abcde" (length: 5)
        char *str = malloc(sizeof(*str) * 6);
        str = ['a', 'b', 'c', 'd','e','\0']
    * It is better to clear string before use.
    Note the real length of the memory block.
        str = memset(str, 0, n+1);
    * Both above steps can be achieved using calloc()
        char *str = (char *)calloc(n+1, sizeof(*str));

************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STR_LEN 20

// ----------------------------------
// Declararation of private functions
// ----------------------------------

static void SortStringInRows(char **, int, int);
static int StrCaseInCmp(const char * const, const char * const);
static void StrToLower(char * const);
static void SwapStr(char **, char **);
static void ClearStrArr(char **, int);
static void PrintStrArr(char **, int);

// -------------
// Main function
// -------------

int main () {
    int row, col;

    do {
        scanf("%d %d", &row, &col);

        // allocate a pointer to array of chars
        char **str_rows = malloc(sizeof(*str_rows) * row);

        // allocate arrays of chars
        for (int i = 0; i < row; ++i) {
            str_rows[i] = calloc(col+1, sizeof(**str_rows));
            // read string to each row
            scanf("%s", str_rows[i]);
        }

        // sort string in rows
        SortStringInRows(str_rows, row, col);

        for (int i = 0; i < row; ++i) {
            printf("%s\n", str_rows[i]);
        }

        // deallocate a pointer to array of chars
        // str_rows and its arrays of chars
        ClearStrArr(str_rows, row);

        if (row) printf("\n");

    } while (row > 0 && col > 0);

    return 0;
}

// ----------------------------------
// Implementation of private function
// ----------------------------------

/**
 Sort string in all rows case insensitive based
 on the first row.

 @param str_roww The string rows.
 @param row The number of rows.
 @param col The length of a column.
*/
static void SortStringInRows(char **str_rows, int row, int col) {
    // allocate a pointer to array of chars
    // to hold new strings
    char **str_arr = malloc(sizeof(*str_arr) * col);

    // point to arrays of chars
    for (int i = 0; i < col; ++i) {
        str_arr[i] = calloc(row+1, sizeof(**str_arr));
    }

    // form every column string
    for (int i = 0; i < col; ++i) {
        for (int j = 0; j < row; ++j) {
            str_arr[i][j] = str_rows[j][i];
        }
    }

    // sort string
    for (int i = 1; i < col; ++i) {
        for (int j = i; j > 0 && StrCaseInCmp(str_arr[j], str_arr[j-1]) < 0; --j) {
            SwapStr(&str_arr[j], &str_arr[j-1]);
        }
    }

    // copy back to str_rows
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            str_rows[i][j] = str_arr[j][i];
        }
    }

    // deallocate a pointer to array of chars
    // str_arr and its array of chars
    ClearStrArr(str_arr, col);
}

/**
 Compare two string case insensitive.

 @param str1 The first string.
 @param str2 The second string.

 @return
    <0 if the first string is before second string,
    >0 if the first string is after second string
    0 if two string is at the same position.
*/
static int StrCaseInCmp(const char * const str1, const char * const str2) {
    char str_tmp_1[STR_LEN];
    char str_tmp_2[STR_LEN];

    // copy strings
    strcpy(str_tmp_1, str1);
    strcpy(str_tmp_2, str2);

    // change two strings to lower cases
    StrToLower(str_tmp_1);
    StrToLower(str_tmp_2);

    return strcmp(str_tmp_1, str_tmp_2);
}

/**
 Convert a string to lower case.

 @param str The string to convert.
*/
static void StrToLower(char * const str) {
    for (int i = 0; i < strlen(str); ++i) {
        str[i] = (char)tolower(str[i]);
    }
}

/**
 Swap two strings

 @param str1 The pointer to first string.
 @param str2 The pointer to second string.
*/
static void SwapStr(char **str1, char **str2) {
    char *tmp;
    tmp = *str1;
    *str1 = *str2;
    *str2 = tmp;
}

/**
 Clear string array. It will deallocate
 memory used for string array.

 @param str_arr The array to clear.
 @param size The size of the array.
*/
static void ClearStrArr(char **str_arr, int size) {
    for (int i = 0; i < size; ++i) {
        free(str_arr[i]);
    }
    free(str_arr);
}

/**
 Print contents of string array.
 For debugging only.

 @param str_arr The string array.
 @param size The size of an array.
*/
static void PrintStrArr(char **str_arr, int size) {
    for (int i = 0; i < size; ++i) {
        printf("%s\n", str_arr[i]);
    }
}