/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_ONECHICKEN.c
 Example:     gcc -Wall -o prog ez_ONECHICKEN.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Dr. Chaz is hosting a programming contest wrap up dinner. Dr. Chaz has
 severe OCD and is very strict on rules during dinner, specifically, he
 needs to be sure that everyone take exactly 1 piece of chicken at his
 buffet, even if that will result in an enormous amount of leftovers. This
 is why every year before the dinner, Dr. Chaz would give a powerful speech:
 “Everyone, one chicken per person!”

 However, Dr. Chaz does not always have an idea how many pieces of chicken
 he needs, he believes if there are N people at the buffet and everyone
 takes exactly 1 piece of chicken, providing M pieces of chicken will be
 perfect, i.e., is enough and will have no leftovers. Help Dr. Chaz find out
 whether his decision is good or not!

 Input:
    * The first line contain integers 0≤N≤1000, 0≤M≤1000, N≠M , the number
      of people at the buffet and the number of pieces of chicken Dr. Chaz
      is providing.

 Output:
    * Output a single line of the form “Dr. Chaz will have P piece[s] of
      chicken left over!”, if Dr. Chaz has enough chicken and P pieces of
      chicken will be left over, or “Dr. Chaz needs Q more piece[s] of
      chicken!” if Dr. Chaz does not have enough pieces of chicken and needs
      Q more.

 Example:
    Input:
    20 100

    Output:
    Dr. Chaz will have 80 pieces of chicken left over!

    Input:
    2 3

    Output:
    Dr. Chaz will have 1 piece of chicken left over!

    Input:
    10 1

    Output:
    Dr. Chaz needs 9 more pieces of chicken!

****************************************************************************/

#include <stdio.h>

int main () {
    int n = 0;      // number of people
    int m = 0;      // number of chicken pieces

    scanf("%d %d", &n, &m);

    if (m+1 < n) {
        printf("Dr. Chaz needs %d more pieces of chicken!\n", n-m);
    } else if (m+1 == n) {
        printf("Dr. Chaz needs %d more piece of chicken!\n", n-m);
    } else if (m > n+1) {
        printf("Dr. Chaz will have %d pieces of chicken left over!\n", m-n);
    } else {
        printf("Dr. Chaz will have %d piece of chicken left over!\n", m-n);
    }

    return 0;
}
