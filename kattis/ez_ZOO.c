/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_ZOO.c
 Example:     gcc -Wall -o prog ez_ZOO.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 In his free time, when he’s not busy hacking computers, Dr. Back runs a
 zoo. Every morning he gets up and makes sure that none of the animals have
 escaped. He has a huge list of all the animals and checks each animal off
 as he sees it, but thinks this is really inefficient. He only cares about
 what animal they are, since all similar animals share a cage. So, if he has
 a white tiger and a siberian tiger, Dr. Back only wants to know that he has
 2 tigers.

 Given an integer n, and n lines describing animals, output in alphabetical
 order the animals Dr. Back has in his zoo, followed by their count.

 Input:
    * The input will contain multiple test cases, up to 5. Each test case
      contains a line containing a single integer n (0≤n≤103), followed by n
      lines of animals with at least one word on every line. An animal name
      may consist of multiple lowercase or uppercase words, with the last
      one describing the kind of animal. Animal names may contain
      apostrophes, hyphens, and periods; e.g., St. Vincent’s Agouti would be
      a valid animal name. The input is terminated when n is 0.

 Output:
    * For each test case, output the list number, followed by the animals
      Dr. Back has in his zoo in lowercase and alphabetical order, with each
      animal followed by one space and the delimiter | and then another
      space and their count.

 Example:
    Input:
    6
    African elephant
    White tiger
    Indian elephant
    Siberian tiger
    Tiger
    Panda bear
    1
    Blue Russian Penguin
    0

    Output:
    List 1:
    bear | 1
    elephant | 2
    tiger | 3
    List 2:
    penguin | 1

****************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DES     81  // max chars of anmial description (null-terminating)
#define MAX_NAME    21  // max chars of anminal name (null-terminating)

// ------------------
// Definition of data
// ------------------

/**
 @struct Animal data structure

 @field name  The name of an animal.
 @field quant The quantiy of an animal.
 */
typedef struct Animal {
    char name[MAX_NAME];
    int quant;
} Animal;

// --------------------------------
// Declaration of private functions
// --------------------------------

static void AddAnimalToList(Animal *, int, int *, const char *);
static void GetAnimalName(char *, const char *);
static int AnimalCompare(const void *p1, const void *p2);
static void PrintAnimalList(Animal *, int);

// -------------
// Main function
// -------------

int main () {
    int line_num = 0;           // number of lines
    Animal *animals;            // list of animals
    int test_case = 0;          // test case
    int size;                   // the size of the list

    // read number of lines in list
    scanf("%d\n", &line_num);

    while (line_num) {
        animals = calloc(line_num, sizeof(*animals)); // allocate memory
        size = 0;                                     // set initial size

        for (int i = 0; i < line_num; ++i) {
            char animal_des[MAX_DES] = {0};

            fgets(animal_des, MAX_DES, stdin);
            AddAnimalToList(animals, line_num, &size, animal_des);
        }

        test_case++; // increase test case

        // sort the list
        qsort(animals, size, sizeof(*animals), AnimalCompare);

        // print output
        printf("List %d:\n", test_case);
        PrintAnimalList(animals, size);

        free(animals); // dealocate memory for animals

        scanf("%d\n", &line_num);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Add animal to a list.

 @param animals     The list of animal.
 @param m_size      The max size of the list.
 @param r_size      The real size of the list
 @param animal_des  The description of animal.
 */
static void AddAnimalToList(Animal *animals, int m_size, int *r_size,
                            const char *animal_des) {
    char animal_name[MAX_NAME] = {0};

    GetAnimalName(animal_name, animal_des);

    // go through the list
    for (int i = 0; i < m_size; ++i) {
        // if there is no that kind of animal in the list
        if (strlen(animals[i].name) == 0) {
            strcpy(animals[i].name, animal_name);   // add animal
            animals[i].quant++;                     // increase quantity
            (*r_size)++;                            // count the real size
            break;                                  // stop updating
        }

        // if there is that kind of animal in the list
        if (strcmp(animals[i].name, animal_name) == 0) {
            animals[i].quant++;                     // increase quantity
            break;                                  // stop updating
        }
    }
}

/**
 Get the name of an anmial from animal description.

 @param animal_name The name of animal.
 @param animal_des  The description of animal.
 */
static void GetAnimalName(char *animal_name, const char *animal_des) {
    // point to location of last space in animal description
    char *last_str = strrchr(animal_des, ' ') ;

    // if the location of space is not found
    if (!last_str) {
        // the animal description is animal name
        // get the animal name
        strncpy(animal_name, animal_des, MAX_NAME);

    // otherwise the location is of white space is found
   } else {

        // point to the word after the white space
        last_str = last_str + 1;

        // get the animal name
        strncpy(animal_name, last_str, MAX_NAME);
    }

    // set the last char of animal name
    // to null if it is new line char
    int len = strlen(animal_name);
    if (animal_name[len-1] == '\n') {
        animal_name[len-1] = '\0';
    }

    //convert name to lower case
    for (int i = 0; animal_name[i]; ++i) {
        // convert it to lower case
        animal_name[i] = tolower(animal_name[i]);
    }
}

/**
 Animal Compare using in qsort() built-in function.

 @param a1 The first animal.
 @param a2 The second animal.

 @return
    <0 The animal a1 goes before animal a2.
    0  The animal a1 is equivalent to animal a2.
    >0 The animal a1 goes after the animal a2.
 */
static int AnimalCompare(const void *a1, const void *a2) {
    return strcmp(((Animal *)a1)->name, ((Animal *)a2)->name);
}

/**
 Print the animal list.

 @param animals The animal list.
 @param size    The size of animal list
 */
static void PrintAnimalList(Animal *animals, int size) {
    // go from the beginning of the list
    // and if there is animal in list
    for (int i =0; i < size; ++i) {
        // output the animal and its count
        printf("%s | %d\n", animals[i].name, animals[i].quant);
    }
}
