/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_SOYLENT.c
 Example:     gcc -Wall -o prog ez_SOYLENT.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Yraglac recently decided to try out Soylent, a meal replacement drink
 designed to meet all nutritional requirements for an average adult. Soylent
 not only tastes great but is also low-cost, which is important for Yraglac
 as he is currently on a budget. Each bottle provides 400 calories, so it is
 recommended that an individual should consume 5 bottles a day for 2000
 total calories. However, Yraglac is wondering how many bottles he should
 consume if his daily calorie requirement is not the same as an average
 adult. He can only consume an integer number of bottles, and needs to
 consume at least his daily calorie requirement.

 Input:
    * The first line contains a single integer T≤1000 giving the number of
      test cases. Each test case consists of a single line with an integer N
      (0≤N≤10000), the number of calories Yraglac needs in a day.

 Output:
    * For each test case, output a single line containing the number of
      bottles Yraglac needs to consume for the day.

 Example:
    Input:
    2
    2000
    1600

    Output:
    5
    4

****************************************************************************/

#include <math.h>
#include <stdio.h>

#define BOT_CAL     400     // calories per bottle

int main () {
    int test_num = 0;       // number of test
    int need_cal = 0;       // calories needed

    scanf("%d", &test_num);

    while (test_num) {
        scanf("%d", &need_cal);

        printf("%d\n", (int)ceil((double)need_cal/BOT_CAL));

        test_num -= 1;
    }

    return 0;
}
