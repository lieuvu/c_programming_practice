/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_ODDITIES.c
 Example:     gcc -Wall -o prog tri_ODDITIES.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Some numbers are just, well, odd. For example, the number 3 is odd, because
 it is not a multiple of two. Numbers that are a multiple of two are not odd,
 they are even. More precisely, if a number n can be expressed as n=2∗k for
 some integer k, then n is even. For example, 6=2*3 is even.

 Some people get confused about whether numbers are odd or even. To see a
 common example, do an internet search for the query “is zero even or odd?”
 (Don’t search for this now! You have a problem to solve!)

 Write a program to help these confused people.

 Input:
    * Input begins with an integer 1≤n≤20 on a line by itself, indicating
      the number of test cases that follow. Each of the following n lines
      contain a test case consisting of a single integer −10≤x≤10.

 Output:
    * For each x, print either ‘x is odd’ or ‘x is even’ depending on
      whether x is odd or even.

 Example:
    Input:
    3
    10
    9
    -5

    Output:
    10 is even
    9 is odd
    -5 is odd


****************************************************************************/

#include <stdio.h>

int main () {
    int num_int;    // number of integers
    int check_num;  // number to check for oddity

    scanf("%d", &num_int);

    while (num_int) {
        scanf("%d", &check_num);

        if (check_num % 2) {
            printf("%d is odd\n", check_num);
        } else {
            printf("%d is even\n", check_num);
        }

        num_int -= 1;
    }

    return 0;
}