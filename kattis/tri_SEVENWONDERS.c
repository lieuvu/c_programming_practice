/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_SEVENWONDERS.c
 Example:     gcc -Wall -o prog tri_SEVENWONDERS.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Seven Wonders is a card drafting game in which players build structures to
 earn points. The player who ends with the most points wins. One winning
 strategy is to focus on building scientific structures. There are three
 types of scientific structure cards: Tablet (‘T’), Compass (‘C’), and Gear
 (‘G’). For each type of cards, a player earns a number of points that is
 equal to the squared number of that type of cards played. Additionally,
 for each set of three different scientific cards, a player scores 7 points.

 For example, if a player plays 3 Tablet cards, 2 Compass cards and 1 Gear
 card, she gets 3^2 + 2^2 + 1^2 + 7 = 21 points.

 It might be tedious to calculate how many scientific points a player gets
 by the end of each game. Therefore, you are here to help write a program
 for the calculation to save everyone’s time.

 Input:
    * The input has a single string with no more than 50 characters. The
      string contains only letters ‘T’, ‘C’ or ‘G’, which denote the
      scientific cards a player has played in a Seven Wonders game.

 Output:
    * Output the number of scientific points the player earns.

 Example:
    Input:
    TCGTTC

    Output:
    21

    Input:
    CCC

    Output.
    9

    Input:
    TTCCGG

    Output:
    26

****************************************************************************/

#include <stdio.h>
#include <math.h>

#define MIN(a,b,c) (a) < (b) ? (a) < (c) ? (a) : (c) : (b) < (c) ? (b) : (c)

#define MAX_CARD    55
#define SET_POINT   7
#define POWER2      2

int main () {
    char card[MAX_CARD];    // array of cards
    int num_T = 0;          // number of card Tablet
    int num_C = 0;          // number of card Compass
    int num_G = 0;          // number of card Gear
    int i = 0;              // card index
    int num_TCG = 0;        // number of set TCG
    int point;

    scanf("%s", card);

    while (card[i]) {
        if (card[i] == 'T') {
            num_T++;
        }
        if (card[i] == 'C') {
            num_C++;
        }
        if (card[i] == 'G') {
            num_G++;
        }

        i++;
    }

    num_TCG = MIN(num_T, num_C, num_G);
    point = (int)pow(num_T, POWER2) + (int)pow(num_C, POWER2) +
            (int)pow(num_G, POWER2) + num_TCG*SET_POINT;

    printf("%d", point);

    return 0;
}