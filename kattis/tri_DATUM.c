/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_DATUM.c
 Example:     gcc -Wall -o prog tri_DATUM.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Write a program that, given a date in 2009, determines the day of week on
 that date.

 Input:
    * The first line contains two positive integers D (day) and M (month)
      separated by a space. The numbers will be a valid date in 2009.

 Output:
    * Output the day of the week on day D of month M in 2009. The output
      should be one of the words “Monday”, “Tuesday”, “Wednesday”,
      “Thursday”, “Friday”, “Saturday” or “Sunday”.

 Example:
    Input:
    1 1

    Output:
    Thursday

    Input:
    17 1

    Output:
    Saturday

    Input:
    25 9

    Output:
    Friday

****************************************************************************/

#include <stdio.h>

#define DAYS_OF_WEEK 7

int main () {
    // an array of days of month in 2009
    int d_month[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int day;        // input day
    int month;      // input month
    int days = 0;   // days of year

    scanf("%d %d", &day, &month);

    /* calculate days of year */
    days += day;                        // add input day
    for (int i = 1; i < month; ++i) {
        days += d_month[i-1];           // add days of previous months
    }

    switch (days % DAYS_OF_WEEK) {
        case 0:
            printf("Wednesday\n");
            break;
        case 1:
            printf("Thursday\n");
            break;
        case 2:
            printf("Friday\n");
            break;
        case 3:
            printf("Saturday\n");
            break;
        case 4:
            printf("Sunday\n");
            break;
        case 5:
            printf("Monday\n");
            break;
        case 6:
            printf("Tuesday\n");
            break;
    }

    return 0;
}
