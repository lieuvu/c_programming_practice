/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_KORNISLAV.c
 Example:     gcc -Wall -o prog tri_KORNISLAV.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Kornislav the turtle never has anything interesting to do. Since he’s going
 to live for three hundred years, he keeps trying to find way to kill time.
 This weekend he started playing "enclose the largest rectangle".

 To start with, Kornislav needs four positive integers. He tries to enclose
 a rectangle by moving in one direction, then turning 90 degrees, then
 walking in the new direction etc. Kornislav makes a total of three
 90-degree turns and walks four segments.

 When walking in some direction, the number of steps he takes must be equal
 to one of the four chosen integers and each integer must be used exactly
 once. Depending on the order in which Kornislav uses the integers, his walk
 will make various shapes, some of which don’t contain enclosed rectangles.
 Write a program that calculates the largest rectangle the turtle can
 enclose with its walk.

 Input:
    * The first line contains four positive integers A,B,C and D
      (0<A,B,C,D<100), the four chosen integers.

 Output:
    * Output the largest area.

 Example:
    Input:
    1 2 3 4

    Output:
    3

    Input:
    4 4 3 4

    Output:
    12

****************************************************************************/

#include <stdio.h>

#define N       4   // the number of input
#define MAX_VAL 100 // the max value of input
#define T       3   // the number of times to find min number in a sequence

int main () {
    int a = 0;          // side of a rectangle
    int b = 0;          // another side of a rectangle
    int seq[N];         // a sequence

    // read input sequence
    for (int i = 0; i < N; ++i) {
        scanf("%d", seq+i);
    }

    // find the first and the third min number
    // in a sequence
    for (int i = 1; i <= T; ++i) {
        int min = MAX_VAL;   // initialize min as MAX_VAL
        int idx = -1;        // the index of the min number

        for (int j = 0; j < N; ++j) {
            if (seq[j] != 0 && seq[j] < min) {
                min = seq[j];
                idx = j;
            }
        }

        seq[idx] = 0;

        if (i == 1) a = min;
        if (i == 3) b = min;
    }

    printf("%d\n", a*b);

    return 0;
}
