/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_ZAMKA.c
 Example:     gcc -Wall -o prog tri_ZAMKA.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 The impossible has happened. Bear G. has fallen into his own trap. Lured by
 a delicious box of Domaćica, without even thinking, he rushed and fell into
 his trap. In order to get out of the trap, he must solve the following task
 with your help. You are given three integers L, D and X

    * determine the minimal integer N such that L≤N≤D and the sum of its
      digits is X.
    * determine the maximal integer M such that L≤M≤D and the sum of its
      digits is X.

 Bear will be able to escape from the trap if he correctly determines
 numbers N and M. The numbers N and M will always exist.

 Input:
    * The first line of input contains the integer L (1≤L≤10000), the number
      from the task. The second line of input contains the integer D
      (1≤D≤10000, L≤D), the number from the task. The third line of input
      contains the integer X (1≤X≤36), the number from the task.

 Output:
    * The first line of output must contain the integer N from the task. The
      second line of output must contain the integer M from the task.

 Example:
    Input:
    1
    100
    4

    Output:
    4
    40

    Input.
    100
    500
    12

    Output:
    129
    480

    Input:
    1
    10000
    1

    Output:
    1
    10000

****************************************************************************/

#include <stdio.h>

#define BASE 10

// --------------------------------
// Declaration of private functions
// --------------------------------

static int DigitsSumOfNumber(int);

// -------------
// Main function
// -------------

int main () {
    int l;          // number L
    int d;          // number D
    int x;          // number X
    int n = -1;     // number N
    int m = -1;     // number M

    scanf("%d %d %d", &l, &d, &x);

    for (int i = l; i <=d; ++i) {
        if (DigitsSumOfNumber(i) == x) {
            n = i;
            break;
        }
    }

    for (int i = d; i >=l; --i) {
        if (DigitsSumOfNumber(i) == x) {
            m = i;
            break;
        }
    }

    if (n == -1 || m == -1) {
        printf("Solution not found!\n");
        return 1;
   }


    printf("%d\n", n);
    printf("%d\n", m);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Calculate the sum of digits of a given number

 @param num The number to calculate the sum.

 @return The sum of all digits of the number.
 */
static int DigitsSumOfNumber(int num) {
    int digits_sum = 0;

    while (num != 0) {
        digits_sum += num % BASE;
        num /= BASE;
    }

   return digits_sum;
}
