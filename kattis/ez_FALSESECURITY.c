/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_FALSESECURITY.c
 Example:     gcc -Wall -o prog ez_FALSESECURITY.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Freddy discovered a new procedure to grow much bigger cauliflowers. He
 wants to share this finding with his fellow gardener Tommy but he does not
 want anyone to steal the procedure. So the two gardeners agreed upon using
 a simple encryption technique proposed by M. E. Ohaver.

 The encryption is based on the Morse code, which represents characters as
 variable-length sequences of dots and dashes. The following table shows the
 Morse code sequences for all letters:

            A .-   | H .... | O ---  | V ...-
            B -... | I ..   | P .--. | W .--
            C -.-. | J .--- | Q --.- | X -..-
            D -..  | K -.-  | R .-.  | Y -.--
            E .    | L .-.. | S ...  | Z --..
            F ..-. | M --   | T -    |
            G --.  | N -.   | U ..-

 Note that four possible dot-dash combinations are unassigned. For the
 purposes of this problem we will assign them as follows (note these are not
 the assignments for actual Morse code):

            underscore (“_”) ..-- | period (“.”)        ---.
            comma (“,”)      .-.- | question mark (“?”) ----

 In practice, characters in a message are delimited by short pauses,
 typically displayed as spaces. Thus, the message “ACM_GREATER_NY_REGION” is
 encoded as:

  .- -.-. -- ..-- --. .-. . .- - . .-. ..-- -. -.-- ..-- .-. . --. .. --- -.

 The Ohaver’s encryption scheme is based on mutilating Morse code, namely by
 removing the pauses between letters. Since the pauses are necessary
 (because Morse is a variable-length encoding that is not prefix-free), a
 string is added that identifies the number of dots and dashes in each
 character. For example, consider the message “.--.-.--”. Without knowing
 where the pauses should be, this could be “ACM”, “ANK”, or several other
 possibilities. If we add length information, such as “.--.-.-- 242”, then
 the code is unambiguous.

 Ohaver’s scheme has three steps, the same for encryption and decryption:

    1. Convert the text to Morse code without pauses but with a string of
       numbers to indicate code lengths.
    2. Reverse the string of numbers.
    3. Convert the dots and dashes back into the text using the reversed
       string of numbers as code lengths.

 As an example, consider the encrypted message “AKADTOF_IBOETATUK_IJN”.
 Converting to Morse code with a length string yields:

 .--.-.--..----..-...--..-...---.-.--..--.-..--...----. 232313442431121334242

 By reversing the numbers and decoding, we get the original message
 “ACM_GREATER_NY_REGION”.

 The security of this encoding scheme is not too high but Freddy believes it
 is sufficient for his purposes. Will you help Freddy to implement this
 encoding algorithm and to protect his sensitive information?

 Input:
    * The input will consist of several messages encoded with Ohaver’s
      algorithm, each of them on one line. There will be at most 3000
      messages.
    * Each message will use only the twenty-six capital letters,
      underscores, commas, periods, and question marks. Messages will not
      exceed 1000 characters in length.

 Output:
    * For each message in the input, output the decoded message on one
      line.

 Example:
    Input:
    FENDSVTSLHW.EDATS,EULAY
    TRDNWPLOEF
    NTTTGAZEJUIIGDUZEHKUE
    QEWOISE.EIVCAEFNRXTBELYTGD.
    ?EJHUT.TSMYGW?EJHOT
    DSU.XFNCJEVE.OE_UJDXNO_YHU?VIDWDHPDJIKXZT?E
    ADAWEKHZN,OTEATWRZMZN_IDWCZGTEPION

    Output:
    FALSE_SENSE_OF_SECURITY
    CTU_PRAGUE
    TWO_THOUSAND_THIRTEEN
    QUOTH_THE_RAVEN,_NEVERMORE.
    TO_BE_OR_NOT_TO_BE?
    THE_QUICK_BROWN_FOX_JUMPS_OVER_THE_LAZY_DOG
    ADAPTED_FROM_ACM_GREATER_NY_REGION

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_ENCPT   5       // max chars of encryption (null-terminating char)
#define MAX_ENTRY   30      // max entries of code table
#define MAX_MSG     1001    // max chars of one message (null-terminating char)

// ------------------
// Definition of data
// ------------------

/**
 @struct Code data structure

 @field c       The character.
 @field encpt   The encription of the character.
 */
typedef struct Code {
  char c;
  char encpt[MAX_ENCPT];
} Code;

// ----------------
// Global constants
// ----------------

static const Code CODE_TABLE[MAX_ENTRY] = { {'A', ".-"}, {'B', "-..."} ,
                                            {'C', "-.-."}, {'D', "-.."},
                                            {'E', "."}, {'F', "..-."},
                                            {'G', "--."}, {'H', "...."},
                                            {'I', ".."}, {'J', ".---"},
                                            {'K', "-.-"}, {'L', ".-.."},
                                            {'M', "--"}, {'N', "-."},
                                            {'O', "---"}, {'P', ".--."},
                                            {'Q', "--.-"}, {'R', ".-."},
                                            {'S', "..."}, {'T', "-"},
                                            {'U', "..-"}, {'V', "...-"},
                                            {'W', ".--"}, {'X', "-..-"},
                                            {'Y', "-.--"}, {'Z', "--.."},
                                            {'_', "..--"}, {',', ".-.-"},
                                            {'.', "---."}, {'?', "----"} };

// --------------------------------
// Declaration of private functions
// --------------------------------

static void DecodeMessage(const char *, char *);
static const char * Encryption(const char);
static char Decryption(const char *);

// -------------
// Main function
// -------------

int main () {
    char msg[MAX_MSG] = {0};        // input message

    while (scanf("%s", msg) != EOF) {
        char de_msg[MAX_MSG] = {0};           // decoded message

        DecodeMessage(msg, de_msg);
        printf("%s\n", de_msg);
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Decode the message.

 @param msg     The message to decode.
 @param de_msg  The message after decoding.
 */
static void DecodeMessage(const char *msg, char *de_msg) {
    char encpt_str[4*MAX_MSG] = {0};    // encryption string
    int len_info[MAX_MSG] = {0};        // encryption length info
    int msg_len = strlen(msg);          // message length
    int j = 0;                          // index of the encryption string

    // go through each character of the message
    for (int i = 0; i < msg_len; ++i) {
        char encpt[MAX_ENCPT] = {0};        // encryption of a character
        int encpt_len;                      // length of encryption

        strcpy(encpt, Encryption(msg[i]));  // get encryption of each char
        encpt_len = strlen(encpt);          // get the length of encryption

        strcpy(encpt_str+j, encpt);     // form the encryption string
        j += encpt_len;                 // point to next position of the string
        len_info[i] = encpt_len;        // form the encryption length info
    }


    j = 0;  // set the index to zero

    // go through each number in reverse order
    // of the encryption length info
    for (int i = msg_len-1; i >= 0; --i) {
        char encpt[MAX_ENCPT] = {0};        // encryption of a character

        // extract an encryption with a length
        // that equals each number
        strncpy(encpt, encpt_str+j, len_info[i]);
        de_msg[msg_len-i-1] = Decryption(encpt); // get a decoded char
        j += len_info[i];                    // point to next encryption
    }
}

/**
 Encrypt the given character based on the code table.

 @param c The character to look up for encryption.

 @return The encryption of the character.
 */
static const char * Encryption(const char c) {
    int i;
    for (i = 0; i < MAX_ENTRY; ++i) {
        if (CODE_TABLE[i].c == c) break;
    }

    return CODE_TABLE[i].encpt;
}

/**
 Decrypt given encryption based on the code table.

 @param encpt The encryption.

 @return The character coresponding to the encryption.
 */
static char Decryption(const char *encpt) {
    int i;
    for (i = 0; i < MAX_ENTRY; ++i) {
        if (strcmp(encpt, CODE_TABLE[i].encpt) == 0) break;
    }

    return CODE_TABLE[i].c;
}
