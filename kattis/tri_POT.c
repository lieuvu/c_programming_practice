/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_POT.c
 Example:     gcc -Wall -o prog tri_POT.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.2

 The teacher has sent an e-mail to her students with the following task:
 “Write a program that will determine and output the value of X if given
 the statement:X = number1^pow1 + numbe2r^pow2 + ... + numberN^powN
 and it holds that number1, number2 to numberN are integers, and pow1, pow2
 to powN are one-digit integers.” Unfortunately, when the teacher downloaded
 the task to her computer, the text formatting was lost so the task
 transformed into a sum of N integers: X = P1 + P2 + ... + PN

 For example, without text formatting, the original task in the form of
 X = 21^2+125^3 became a task in the form of X = 212 + 1253. Help the teacher
 by writing a program that will, for given N integers from P1 to PN determine
 and output the value of X from the original task.

 Input:
    * The first line of input contains the integer N (1≤N≤10), the number
      of the addends from the task. Each of the following N lines contains
      the integer Pi (10≤Pi≤9999, i=1,..,N) from the task.

 Output:
    * The first and only line of output must contain the value of X
      (X≤1 000 000 000) from the original task.

 Example:
    Input:
    2
    212
    1253

    Output:
    1953566

    Input:
    5
    23
    17
    43
    52
    22

    Output:
    102

    Input:
    3
    213
    102
    45

    Output:
    10385

****************************************************************************/

#include <stdio.h>
#include <math.h>

int main () {
    const int BASE_10 = 10;
    int num_int;          // number of integers
    int wrong_num;       // wrong number
    int exp;
    int c_num;
    int sum = 0;

    scanf("%d", &num_int);

    while (num_int) {
        scanf("%d", &wrong_num);
        exp = wrong_num % BASE_10;
        c_num = wrong_num/BASE_10;

        sum += (int)pow(c_num, exp);

        num_int -= 1;
    }

    printf("%d", sum);

    return 0;
}