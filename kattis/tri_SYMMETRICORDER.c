/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_SYMMETRICORDER.c
 Example:     gcc -Wall -o prog tri_SYMMETRICORDER.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 In your job at Albatross Circus Management (yes, it’s run by a bunch of
 clowns), you have just finished writing a program whose output is a list of
 names in nondescending order by length (so that each name is at least as
 long as the one preceding it). However, your boss does not like the way the
 output looks, and instead wants the output to appear more symmetric, with
 the shorter strings at the top and bottom and the longer strings in the
 middle. His rule is that each pair of names belongs on opposite ends of the
 list, and the first name in the pair is always in the top part of the list.
 In the first example set below, Bo and Pat are the first pair, Jean and
 Kevin the second pair, etc.

 Input:
    * The input consists of one or more sets of strings, followed by a final
      line containing only the value 0. Each set starts with a line
      containing an integer, n, which is the number of strings in the set,
      followed by n strings, one per line, sorted in nondescending order by
      length. None of the strings contain spaces. There is at least one and
      no more than 15 strings per set. Each string is at most 25 characters
      long.

 Output:
    * For each input set print “SET n” on a line, where n starts at 1,
      followed by the output set as shown in the sample output.

 Example:
    Input:
    7
    Bo
    Pat
    Jean
    Kevin
    Claude
    William
    Marybeth
    6
    Jim
    Ben
    Zoe
    Joey
    Frederick
    Annabelle
    5
    John
    Bill
    Fran
    Stan
    Cece
    0

    Output:
    SET 1
    Bo
    Jean
    Claude
    Marybeth
    William
    Kevin
    Pat
    SET 2
    Jim
    Zoe
    Frederick
    Annabelle
    Joey
    Ben
    SET 3
    John
    Fran
    Cece
    Stan
    Bill

****************************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAX_STR 20  // max strings in set
#define MAX_CHR 30  // max characters per string

int main () {
    int set_num = 0;    // number of set
    int str_num;        // number of strings in set

    scanf("%d", &str_num);

    while (str_num) {
        char str_set[MAX_STR][MAX_CHR];  // set of strings
        char out_set[MAX_STR][MAX_CHR];  // ouput set of strings
        int str_pair;                    // number of string pairs in set

        str_pair = ceil((double)str_num/2); // calculate string pairs number

        // read input set of strings to the set
        for (int i = 0; i < str_num; ++i) {
            scanf("%s", str_set[i]);
        }

        set_num++;  // increase the number of set

        // copy each pair of string in a set
        // to out put set
        for (int i = 0; i < str_pair; ++i) {
            // copy first string from a pair
            // to output set
            strcpy(out_set[i], str_set[2*i]);

            // if a pair has second string
            if (2*i + 1 < str_num) {
                // copy second string from a pair
                // to output set
                strcpy(out_set[str_num-1-i], str_set[2*i+1]);
            }
        }

        // print output
        printf("SET %d\n", set_num);
        for (int i = 0; i < str_num; ++i) {
            printf("%s\n", out_set[i]);
        }

        scanf("%d", &str_num);  // read next line
    }

    return 0;
}
