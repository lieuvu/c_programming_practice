/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_AUTORI.c
 Example:     gcc -Wall -o prog tri_AUTORI.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Great scientific discoveries are often named by the last names of
 scientists that made them. For example, the most popular asymmetric
 cryptography system, RSA was discovered by Rivest, Shamir and Adleman.
 Another notable example is the Knuth-Morris-Pratt algorithm, named by
 Knuth, Morris and Pratt.

 Scientific papers reference earlier works a lot and it’s not uncommon
 for one document to use two different naming conventions: the short
 variation (e.g. KMP) using only the first letters of authors last names
 and the long variation (e.g. Knuth-Morris-Pratt) using complete last names
 separated by hyphens.

 We find mixing two conventions in one paper to be aesthetically unpleasing
 and would like you to write a program that will transform long variations
 into short.

 Input:
    * The first and only line of input will contain at most 100 characters,
      uppercase and lowercase letters of the English alphabet and hyphen
      (‘-’ ASCII 45). The first character will always be an uppercase
      letter. Hyphens will always be followed by an uppercase letter. All
      other characters will be lowercase letters.

 Output:
    * The first and only line of output should contain the appropriate short
      variation.

 Example:
    Input:
    Knuth-Morris-Pratt

    Output:
    KMP

    Input:
    Mirko-Slavko

    Output:
    MS

    Input:
    Pasko-Patak

    Output:
    PP

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_CHAR 150
#define HYPHEN 45

int main () {
    char str[MAX_CHAR] = {0};  // array of characters
    int str_len;
    char result[MAX_CHAR] = {0};
    int idx = 0;

    scanf("%s", str);
    str_len = strlen(str);

    // get in first character
    result[idx] = str[idx];

    for (int i = 1; i < str_len; ++i) {
        // if catch hypehn
        if (str[i] == HYPHEN) {
            // add next character to result
            result[++idx] = str[i+1];
            // skip two characters
            i += 2;
        }
    }

    for (int i = 0; i <= idx; ++i) {
        printf("%c", result[i]);
    }

    return 0;
}