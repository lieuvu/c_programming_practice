/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_VOLIM.c
 Example:     gcc -Wall -o prog ez_VOLIM.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Note:
    * See pic_problems/ez_VOLIM.pdf for visual presentation of the problem.

 Croatia’s national television programme is broadcasting an entertainment
 show titled “I Love Croatia”, modeled on the licensed format I love my
 country. In this show two teams of celebrities and public figures play
 various games which require knowledge about Croatia. One of the games is
 Happy Birthday, which will be used in this task, although somewhat altered.

 Eight players numbered one through eight are seated in a circle (see
 picture). One of them is holding a box which will explode after 3 minutes
 and 30 seconds from the beginning of the game when some colorful confetti
 will be blown out. The game begins with a question to the player holding
 the box. If the players answers incorrectly or skips the question, he is
 immediately given the next question. If the player answers correctly, he
 passes the box to the first player seated on his left and then that
 player gets the next question.

 You are given the numbered label of the player who has the box in the
 beginning and the outcomes of the first N questions asked. Determine the
 numbered label of the player who had the box when it finally exploded. The
 question outcome is described with the following data - time passed from
 the beginning of the question being asked to the moment an answer was given
 and whether the answer was true (“T”), false (“N”) or skipped (“P”). The
 time between giving the answer and asking the next question shouldn’t be
 taken into consideration, as well as time necessary for the box to be
 passed to the next player. The box will surely explode on a player’s
 turn.

 Input:
    * The first line of input contains a positive integer K (1≤K≤8), the
      numbered label of the player who has the box initially. The second
      line of input contains a positive integer N (1≤N≤100), the number of
      questions asked during the game. Each of the following N lines
      contains a positive integer T (1≤T≤100), time passed from the
      beginning of the i-th question being asked to the moment an answer was
      given, measured in seconds, and a single character Z (“T”, “N” or
      “P”), the type of answer given.

 Output:
    * The first and only line of output must contain the numbered label of
      the player who had the box when it finally exploded.

 Example:
    Input:
    1
    5
    20 T
    50 T
    80 T
    50 T
    30 T

    Output:
    5

    Input:
    3
    5
    100 T
    100 N
    100 T
    100 T
    100 N

    Output:
    4

    Input:
    5
    6
    70 T
    50 P
    30 N
    50 T
    30 P
    80 T

    Output:
    7
****************************************************************************/

#include <stdio.h>

#define MAX_PLAYER 8        // max player
#define MAX_TIME   210      // max time until box explodes

int main () {
    int player_id = 0;      // player number
    int q_num = 0;          // number of question
    int time = 0;           // time passed to answer questions

    scanf("%d %d", &player_id, &q_num);

    for (int i = 0; i < q_num; ++i) {
        int ans_t = 0;  // answer time
        char ans_r;     // answer result

        scanf("%d %c", &ans_t, &ans_r);

        time += ans_t;      // update time

        // if time exceed MAX_TIME, the box explodes
        if (time > MAX_TIME) {
            printf("%d", player_id);    // print player id the box explodes
            break;                      // stop checking
        }

        // if answer is correct
        if (ans_r == 'T') {
            // pass the box to next player
            if (player_id == MAX_PLAYER) {
                player_id = (player_id+1) % MAX_PLAYER;
            } else {
                player_id++;
            }
        }
    }

    return 0;
}
