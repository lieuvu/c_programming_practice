/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_LADDER.c
 Example:     gcc -Wall -o prog tri_LADDER.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 You are attempting to climb up the roof to fix some leaks, and have to go
 buy a ladder. The ladder needs to reach to the top of the wall, which is
 h centimeters high, and in order to be steady enough for you to climb it,
 the ladder can be at an angle of at most v degrees from the ground. How
 long does the ladder have to be?

 Input:
    * The input consists of a single line containing two integers h and v,
      with meanings as described above. You may assume that 1≤h≤10000 and
      that 1≤v≤89.

 Output:
    * Write a single line containing the minimum possible length of the
      ladder in centimeters, rounded up to the nearest integer.

 Example:
    Input:
    500 70

    Output:
    533

    Input:
    1000 10

    Output:
    5759

****************************************************************************/

#include <stdio.h>
#include <math.h>

#ifndef M_PI
#define M_PI            acos(-1.0)
#endif

#define STRAIGHT_ANGLE  180

int main () {
    int h;      // height of the wall
    int v;      // degree from by the ladder and the wall in degree
    int len;    // length of the ladder

    scanf("%d %d", &h, &v);

    len = (int)ceil(h/sin(v*M_PI/STRAIGHT_ANGLE));

    printf("%d", len);

    return 0;
}