/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_HUMANCANNONBALL2.c
 Example:     gcc -Wall -o prog tri_HUMANCANNONBALL2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Note:
    * See pic_problems/tri_HUMMANCANNONBALL2.pdf for visual presentation of
      the problem.

 The amazing human cannonball show is coming to town, and you are asked to
 double-check their calculations to make sure no one gets injured! The human
 cannonball is fired from a cannon that is a distance x1 from a vertical
 wall with a hole through which the cannonball must fly. The lower edge of
 the hole is at height h₁ and the upper edge is at height h₂. The initial
 velocity of the cannonball is given as v0 and you also know the angle
 θ of the cannon relative to the ground.

 Thanks to their innovative suits, human cannonballs can fly without air
 resistance, and thus their trajectory can be modeled using the following
 formulas:
            x(t) = v₀tcosθ
            y(t) = v₀tsinθ − 1/2gt²

 where x(t),y(t) provides the position of a cannon ball at time t that is
 fired from point (0,0). g is the acceleration due to gravity
 (g=9.81 m/s²).

 Write a program to determine if the human cannonball can make it safely
 through the hole in the wall. To pass safely, there has to be a vertical
 safety margin of 1 m both below and above the point where the ball’s
 trajectory crosses the centerline of the wall.

 Input:
    * The input will consist of up to 100 test cases. The first line
      contains an integer N, denoting the number of test cases that follow.
      Each test case has 5 parameters: v₀ θ x₁ h₁ h₂, separated by spaces.
      v₀ (0<v₀≤200) represents the ball’s initial velocity in m/s. θ is an
      angle given in degrees (0<θ<90), x₁ (0<x₁<1000) is the distance from
      the cannon to the wall, h1 and h2 (0<h₁<h₂<1000) are the heights of
      the lower and upper edges of the wall. All numbers are floating
      point numbers.

 Output:
    * If the cannon ball can safely make it through the wall, output “Safe”.
      Otherwise, output “Not Safe”!

 Example:
    Input:
    11
    19 45 20 9 12
    20 45 20 9 12
    25 45 20 9 12
    20 43 20 9 12
    20 47.5 20 9 12
    20 45 17 9 12
    20 45 24 9 12
    20 45 20 10 12
    20 45 20 9 11
    20 45 20 9.0 11.5
    20 45 18.1 9 12

    Output:
    Not Safe
    Safe
    Not Safe
    Not Safe
    Not Safe
    Not Safe
    Not Safe
    Not Safe
    Not Safe
    Safe
    Safe

****************************************************************************/

#include <stdio.h>
#include <math.h>

#ifndef M_PI
#define M_PI            acos(-1.0)
#endif

#define STRAIGHT_ANGLE  180
#define g               9.81
#define SAFETY_M        1

// --------------------------------
// Declaration of private functions
// --------------------------------

static void CheckSafety(double, double, double, double, double);

// -------------
// Main function
// -------------

int main () {
    int test_num;   // number of test
    double v0;      // initial velocity
    double theta;   // angle θ
    double x1;      // the distance x₁
    double h1;      // the lower edge height h₁
    double h2;      // the upper edge height h₂

    scanf("%d", &test_num);

    while (test_num) {
        scanf("%lf %lf %lf %lf %lf", &v0, &theta, &x1, &h1, &h2);

        CheckSafety(v0, theta, x1, h1, h2);

        test_num -= 1; // decrease number of test
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Check safety of the human cannonball.

 @param v0     The initial velocity.
 @param theta  The angle.
 @param x1     The distance x₁
 @param h1     The lower edge height h₁
 @param h2     The upper edge height h₂
 */
static void CheckSafety(double v0, double theta, double x1,
                        double h1, double h2) {
    double theta_radn = theta * M_PI/STRAIGHT_ANGLE; // theta in radian
    double t1 = x1/(v0*cos(theta_radn));
    double min_h = h1 + SAFETY_M;
    double max_h = h2 - SAFETY_M;
    double y1 = x1*tan(theta_radn) - 0.5*g*pow(t1,2);

    if (y1 >= min_h && y1 <= max_h) {
        printf("Safe\n");
    } else {
        printf("Not Safe\n");
    }
}
