/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> template.c
 Example:     gcc -Wall -o prog template.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 The 19th century German mathematician Hermann Minkowski investigated a
 non-Euclidian geometry, called the taxicab geometry. In taxicab geometry
 the distance between two points T1(x1,y1) and T2(x2,y2) is defined as:

            D(T1,T2)=|x1−x2|+|y1−y2|

 All other definitions are the same as in Euclidian geometry, including that
 of a circle:

 A circle is the set of all points in a plane at a fixed distance (the
 radius) from a fixed point (the centre of the circle).

 We are interested in the difference of the areas of two circles with radius
 R, one of which is in normal (Euclidian) geometry, and the other in taxicab
 geometry. The burden of solving this difficult problem has fallen onto you.

 Input:
    * The first and only line of input will contain the radius R, a positive
      integer smaller than or equal to 10000.

 Output:
    * On the first line you should output the area of a circle with radius R
      in normal (Euclidian) geometry. On the second line you should output
      the area of a circle with radius R in taxicab geometry.
    * Note: Outputs within ±0.0001 of the official solution will be
      accepted.

 Example:
    Input:
    1

    Output:
    3.141593
    2.000000

    Input:
    21

    Output:
    1385.442360
    882.000000

    Input:
    42

    Output:
    5541.769441
    3528.000000

****************************************************************************/

#include <stdio.h>
#include <math.h>

#ifndef M_PI
#define M_PI    acos(-1)
#endif

int main () {
    double r;               // input radius
    double c_area_eu;       // circle area Euclidian
    double c_area_ta;       // circle area taxicab geometry

    scanf("%lf", &r);

    c_area_eu = M_PI*r*r;
    c_area_ta = 0.5*(2*r)*(2*r);

    printf("%.6lf\n%.6lf\n", c_area_eu, c_area_ta);

    return 0;
}
