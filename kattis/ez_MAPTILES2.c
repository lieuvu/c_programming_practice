/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_MAPTITLES2.c
 Example:     gcc -Wall -o prog ez_MAPTITLES2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Note:
    * See pic_problems/ez_MAPTITLES2.pdf for visual presentation of the
      problem.

 Map websites such as Bing Maps and Google Maps often store their maps as
 many different image files, called tiles. The lowest zoom level (level 0)
 consists of a single tile with a low-detail image of the whole map, zoom
 level 1 consists of four tiles each containing a slightly more detailed
 version of a quarter of the map, and in general zoom level n contains 4n
 different tiles that each contain a part of the map.

 One way of identifying a tile is by means of a quadkey. A quadkey is a
 string of digits uniquely identifying a tile at a certain zoom level. The
 first digit specifies in which of the four quadrants of the whole map the
 tile lies: 0 for the top-left quadrant, 1 for the top-right quadrant, 2 for
 the bottom-left quadrant and 3 for the bottom-right quadrant. The
 subsequent digits specify in which sub quadrant of the current quadrant the
 tile is.

 Another way of identifying a tile is to give the zoom level and x and y
 coordinates, where (0,0) is the left-top corner. The coordinates for the
 tiles of zoom level 3 are shown in Figure 1(b). Given the quadkey of a
 tile, output the zoom level and x and y coordinates of that tile.

 Input:
    * The input consists of one line with a string s (1≤length(s)≤30), the
      quadkey of the map tile.
    * The string s consists of only the digits ‘0’, ‘1’, ‘2’ and ‘3’.

 Output:
    * Output three integers, the zoom level and the x and y coordinates of
      the tile.

 Example:
    Input:
    3

    Output:
    1 1 1

    Input:
    130

    Output:
    3 6 2

****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <string.h>

#define MAX_CHAR        31  // max keys of quadkey (null-terminating char)
#define MAP_SIDE_BASE    2  // the base size of one side of a map

// ------------------
// Definition of data
// ------------------

/**
 @struct Point data structure

 @field x The x coordinate of the point.
 @field y The y coordinate of the point.
 */
typedef struct Point {
    int x;
    int y;
} Point;

// --------------------------------
// Declaration of private functions
// --------------------------------

static Point FindOriginOfQuadrant(Point, int, int);

// -------------
// Main function
// -------------

int main () {
    char quadkey[MAX_CHAR] = {0};  // the quadkey
    int zoom_levels = 0;           // the zoom levels
    int side = 0;                  // the size of one side of a map
    Point point = {0};             // the point to find, starting from origin

    scanf("%s", quadkey);
    zoom_levels = strlen(quadkey);              // zoom levels
    side = pow(MAP_SIDE_BASE, zoom_levels);     // size of whole maps

    // iterate through each zoom level
    for (int i = 0; i < zoom_levels; ++i) {
        // find the origin quadrant at certain zoom level
        point = FindOriginOfQuadrant(point, side, quadkey[i] - '0');

        // divide the map by side base(2)
        side = side/MAP_SIDE_BASE;
    }

    printf("%d %d %d\n", zoom_levels, point.x, point.y);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Find the origin point of a quadrant base on
 the origin of the map and the size of one
 side of the map. The origin of the map is
 defined as the top left point of the map.

 @param origin The origin point of the map
 @param side   The size of one side of the map.
 @param quad   The quadrant of the point.

 @return The origin point of the quadrant.
 */
static Point FindOriginOfQuadrant(Point origin, int side, int quad) {
    Point quad_origin = {0};

    switch (quad) {
        case 0:
            quad_origin.x = origin.x;
            quad_origin.y = origin.y;
            break;
        case 1:
            quad_origin.x = origin.x + side/2;
            quad_origin.y = origin.y;
            break;
        case 2:
            quad_origin.x = origin.x;
            quad_origin.y = origin.y + side/2;
            break;
        case 3:
            quad_origin.x = origin.x + side/2;
            quad_origin.y = origin.y + side/2;
            break;
    }

    return quad_origin;
}
