/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_PARKING.c
 Example:     gcc -Wall -o prog ez_PARKING.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Having dropped out of school because of chemistry, Luka got a job driving
 trucks. One evening he parked his three trucks in a rest area which charges
 for parking in an unusual way – they give a discount on quantity.

 When only one truck is parked, the driver pays A kuna per minute. When two
 trucks are parked, the drivers each pay B kuna per minute. When three
 trucks are parked, the drivers each pay C kuna per minute.

 Given the numbers A, B and C, as well as the intervals in which Luka’s
 three trucks are parked, determines how much Luka needs to pay the owner of
 the rest area.

 Input:
    * The first line contains three integers A, B and C (1≤C≤B≤A≤100), the
      prices of parking as defined above. Each of the following three lines
      contains two integers each. These are the arrival and departure times
      (in minutes) of one of Luka’s trucks. The arrival time will always be
      earlier than the departure time. All time indexes will be between 1
      and 100.

 Output:
    * Output the overall cost of Luka’s parking his three trucks.

 Example:
    Input:
    5 3 1
    1 6
    3 5
    2 8

    Output:
    33

    Input:
    10 8 6
    15 30
    25 50
    70 80

    Ouput:
    480

****************************************************************************/

#include <stdio.h>

#define MAX_TRUCK   3       // max number of trucks
#define MIN_TIME    1       // min time to arrive
#define MAX_TIME    100     // max time to leave

// ------------------
// Definition of data
// ------------------

/**
 @struct ParkingTime data structure.

 @field t1 The arrival time.
 @field t2 The departure time.
 */
typedef struct ParkingTime {
    int t1;
    int t2;
} ParkingTime;

// -------------
// Main function
// -------------

int main () {
    int prices[MAX_TRUCK+1] = {0};      // prices for parking
    ParkingTime p_t[MAX_TRUCK] = {0};   // array of parking time
    int min_t1 = MAX_TIME;              // min arrival time
    int max_t2 = MIN_TIME;              // max departure time
    int cost = 0;                       // cost for parking

    // read prices
    for (int i = 1; i <= MAX_TRUCK; ++i) {
        scanf("%d", prices+i);
        // update price/min for parking
        // based on number of trucks
        prices[i] = prices[i]*i;
    }

    // read parking time data
    for (int i = 0; i < MAX_TRUCK; ++i) {
        scanf("%d %d", &p_t[i].t1, &p_t[i].t2);

        // if arrival time is less than
        // min arrival time
        if (p_t[i].t1 < min_t1) {
            min_t1 = p_t[i].t1;     // update min arrival time
        }

        // if departure time is larger than
        // max departure time
        if (p_t[i].t2 > max_t2) {
            max_t2 = p_t[i].t2;     // update max departure time
        }
    }

    // scan from in arrvial time to max
    // departure time
    for (int i = min_t1; i < max_t2; ++i) {
        int tr_count = 0;      // number of parking trucks

        // go through the parking time data
        for (int j = 0; j < MAX_TRUCK; ++j) {
            // if the time is within
            // the parking time range
            if (p_t[j].t1 <= i && i < p_t[j].t2) {
                tr_count++;     // count the truck
            }
        }

        cost += prices[tr_count];   // calculate parking cost
    }

    printf("%d\n", cost);

    return 0;
}
