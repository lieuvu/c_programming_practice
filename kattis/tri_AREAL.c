/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_AREAL.c
 Example:     gcc -Wall -o prog tri_AREAL.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Old MacDonald had a farm, and on that farm she had a square-shaped pasture,
 and on that pasture she had a cow that was prone to escape. So now Old
 MacDonald wants to set up a fence around the pasture. Given the area of the
 pasture, how long a fence does Old MacDonald need to buy?

 Input:
    * The input consists of a single integer a (1≤a≤10¹⁸), the area in
      square meters of Old MacDonald’s pasture.

 Output:
    * Output the total length of fence needed for the pasture, in meters.
      The length should be accurate to an absolute or relative error of at
      most 10⁻⁶.

 Example:
    Input:

    Output:


****************************************************************************/

#include <stdio.h>
#include <math.h>

int main () {
    long area;           // the area of the pasture
    double fence_len;    // the length of fence

    scanf("%ld", &area);
    fence_len = sqrt(area)*4;

    printf("%.20f\n", fence_len);

    return 0;
}
