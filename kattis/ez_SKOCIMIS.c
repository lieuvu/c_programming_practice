/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_SKOCIMIS.c
 Example:     gcc -Wall -o prog ez_SKOCIMIS.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Three kangaroos are playing in the desert. They are playing on a number
 line, each occupying a different integer. In a single move, one of the
 outer kangaroos jumps into the space between the other two. At no point may
 two kangaroos occupy the same position. Help them play as long as possible.

 Input:
    * Three integers A, B and C (0<A<B<C<100), the initial positions of the
      kangaroos.

 Output:
    * Output the largest number of moves the kangaroos can make.

 Example:
    Input:
    2 3 5

    Output:
    1

    Input:
    3 5 9

    Output:
    3

****************************************************************************/

/***************************************************************************
 * Discussion:
    * Let num_btw_A_B the number of numbers between two number A and B
    * Let num_btw_B_C the number of numbers between two number B and C.
    * It is easy to see that the largest move is the largest number of
      num_btw_A_B and num_btw_B_C.

****************************************************************************/

#include <math.h>
#include <stdio.h>

int main () {
    int a = 0;
    int b = 0;
    int c = 0;
    int num_btw_a_b = 0;    // number of numbers between a and b
    int num_btw_b_c = 0;    // number of numbers between b and c
    int max_moves = 0;      // max moves

    scanf("%d %d %d", &a, &b, &c);

    num_btw_a_b = b-a-1;
    num_btw_b_c = c-b-1;
    max_moves = fmax(num_btw_a_b, num_btw_b_c);

    printf("%d\n", max_moves);

    return 0;
}
