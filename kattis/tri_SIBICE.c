/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_SIBICE.c
 Example:     gcc -Wall -o prog tri_SIBICE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Young Mirko threw matches all over the floor of his room.

 His mom did not like that and ordered him to put all the matches in a box.
 Mirko soon noticed that not all of the matches on the floor fit in the box,
 so he decided to take the matches that don’t fit and throw them in the
 neighbour’s garbage, where his mom (hopefully) won’t find them.

 Help Mirko determine which of the matches fit in the box his mom gave him.
 A match fits in the box if its entire length can lie on the bottom of the
 box. Mirko examines the matches one by one.

 Input:
    * The first line of input contains an integer N (1≤N≤50), the number of
      matches on the floor, and two integers W and H, the dimensions of the
      box (1≤W≤100, 1≤H≤100).
    * Each of the following N lines contains a single integer between 1 and
      1000 (inclusive), the length of one match.

 Output:
    * For each match, in the order they were given in the input, output on
      a separate line “DA” if the match fits in the box or “NE” if it does
      not.

 Example:
    Input:
    5 3 4
    3
    4
    5
    6
    7

    Output:
    DA
    DA
    DA
    NE
    NE

    Input:
    2 12 17
    21
    20

    Output:
    NE
    DA

****************************************************************************/

#include <stdio.h>
#include <math.h>

int main () {
    int num_m;      // number of matches
    int w;          // width of the box
    int h;          // height of the box
    double hyp;     // the hypotenuse of the box
    int check_m;    // the checked match

    scanf("%d %d %d", &num_m, &w, &h);

    hyp = sqrt((double)w*w+h*h);

    while (num_m) {
        scanf("%d", &check_m);

        if (check_m <= hyp) {
            printf("DA\n");
        } else {
            printf("NE\n");
        }

        num_m -= 1; // reduce the number of matches
    }

    return 0;
}