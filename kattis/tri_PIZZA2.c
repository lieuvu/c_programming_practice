/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_PIZZA2.c
 Example:     gcc -Wall -o prog tri_PIZZA2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 George has bought a pizza. George loves cheese. George thinks the pizza
 does not have enough cheese. George gets angry.

 George’s pizza is round, and has a radius of R cm. The outermost C cm is
 crust, and does not have cheese. What percent of George’s pizza has cheese?

 Input:
    * The input consists of a single line with two space separated integers,
      R and C. 1≤C≤R≤100

 Output:
    * Output the percentage of the pizza that has cheese. Your answer must
      have an absolute or relative error of at most 10−6.

 Example:
    Input:
    1 1

    Output:
    0.000000000

    Input:
    2 1

    Output:
    25.000000

****************************************************************************/

#include <stdio.h>

int main () {
    int r;      // radius
    int c;      // crust
    double res; // result

    scanf("%d %d", &r, &c);

    res = (double)(r-c)*(r-c)/r/r*100;

    printf("%.6f\n", res);

    return 0;
}
