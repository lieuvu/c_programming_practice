/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_DICEGAME.c
 Example:     gcc -Wall -o prog tri_DICEGAME.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Gunnar and Emma play a lot of board games at home, so they own many dice
 that are not normal 6-sided dice. For example they own a die that has 10
 sides with numbers 47,48,…,56 on it.

 There has been a big storm in Stockholm, so Gunnar and Emma have been stuck
 at home without electricity for a couple of hours. They have finished
 playing all the games they have, so they came up with a new one. Each
 player has 2 dice which he or she rolls. The player with a bigger sum wins.
 If both sums are the same, the game ends in a tie.

 Given the description of Gunnar’s and Emma’s dice, which player has higher
 chances of winning?

 All of their dice have the following property: each die contains numbers a,
 a+1,...,b, where a and b are the lowest and highest numbers respectively on
 the die. Each number appears exactly on one side, so the die has b−a+1
 sides.

 Input:
    * The first line contains four integers a1,b1,a2,b2 that describe
      Gunnar’s dice. Die number i contains numbers ai,ai+1,…,bi on its
      sides. You may assume that 1≤ai≤bi≤100. You can further assume that
      each die has at least four sides, so ai+3≤bi.
    * The second line contains the description of Emma’s dice in the same
      format.

 Output:
    * Output the name of the player that has higher probability of winning.
      Output “Tie” if both players have same probability of winning.

 Example:
    Input:
    1 4 1 4
    1 6 1 6

    Output:
    Emma

    Input:
    1 8 1 8
    1 10 2 5

    Output:
    Tie

    Input:
    2 5 2 7
    1 5 2 5

    Output:
    Gunnar

****************************************************************************/

/***************************************************************************
 Discussion:
    * Solution:
        + For each player, calculate the probability distribution of his/her
          throws (calculate the probability of each possible outcome).
        + Using this information, determine the probability of winning for
          both players.
    * Shorter solution:
        + Insight: both distributions are symmetric around the mean.
        + Therefore it’s enough to compare the expected values of both
          pobability distributions – compare the sum of both lines of
          input.

****************************************************************************/

#include <stdio.h>

int main () {
    int ga1, gb1, ga2, gb2;     // Gunnar's dices
    int ea1, eb1, ea2, eb2;     // Emma's dices
    int gunnar_sum;             // sum of Gunnar's dices
    int emma_sum;               // sum of Emma's dices

    scanf("%d %d %d %d", &ga1, &gb1, &ga2, &gb2);
    scanf("%d %d %d %d", &ea1, &eb1, &ea2, &eb2);

    gunnar_sum = ga1 + gb1 + ga2 + gb2;
    emma_sum = ea1 + eb1 + ea2 + eb2;

    if (gunnar_sum > emma_sum) {
        printf("Gunnar\n");
    } else if (emma_sum > gunnar_sum) {
        printf("Emma\n");
    } else {
        printf("Tie\n");
    }

    return 0;
}
