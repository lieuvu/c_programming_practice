/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_BEAVERGNAW.c
 Example:     gcc -Wall -o prog tri_BEAVERGNAW.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Note:
    * See pic_problems/tri_BEAVERGNAW.pdf for visual presentation of the
      problem.

 When chomping a tree the beaver cuts a very specific shape out of the tree
 trunk. What is left in the tree trunk looks like two frustums of a cone
 joined by a cylinder with the diameter the same as its height. A very
 curious beaver tries not to demolish a tree but rather sort out what should
 be the diameter of the cylinder joining the frustums such that he chomped
 out certain amount of wood. You are to help him to do the calculations.

 We will consider an idealized beaver chomping an idealized tree. Let us
 assume that the tree trunk is a cylinder of diameter D and that the beaver
 chomps on a segment of the trunk also of height D. What should be the
 diameter d of the inner cylinder such that the beaver chomped out V cubic
 units of wood?

 Input:
    * Input contains multiple cases each presented on a separate line. Each
      line contains two space separated integers D,V (1≤D≤100,1≤V≤1000000).
      D is the linear units and V is in cubic units. V will not exceed the
      maximum volume of wood that the beaver can chomp. A line with D=0 and
      V=0 follows the last case.

 Output:
    * For each case, one line of output should be produced containing a
      floating point number giving the value of d measured in linear units.
      Your output will be considered correct if it is within relative or
      absolute error 10−6.

 Example:
    Input:
    10 250
    20 2500
    25 7000
    50 50000
    0 0

    Output:
    8.054498576
    14.774938880
    13.115314879
    30.901188723

****************************************************************************/

/***************************************************************************
 Discussion:
    * Let V₁ the volume of a segment, V₂ the volume of the inner cylinder,
      V₃ the volumn of a conical frustum and V the chomped volume. We have:
        V₁ = Π*(D/2)²*D = Π/4*D³
        V₂ = Π*(d/2)²*d = Π/4*d³
        V₃ = Π/24*(D³-d³)
        V = V₁ - V₂ - 2*V₃ = Π/6*(D³-d³)
    * The chomped volume can also be calculated using calculus to calculate
      volumes of solids of revolution - methods of cylinders.
        V = 2*Π∫x²dx (x ∈ [d/2 - D/2])

****************************************************************************/

#include <math.h>
#include <stdio.h>

#ifndef M_PI
#define M_PI acos(-1)
#endif

int main () {
    int outer_d;        // outer diameter
    int chomped_v;      // chomped volume

    scanf("%d %d", &outer_d, &chomped_v);

    while (outer_d != 0 && chomped_v != 0) {
        double inner_d = cbrt(pow(outer_d,3) - (double)6*chomped_v/M_PI);

        printf("%.9f\n", inner_d);

        scanf("%d %d", &outer_d, &chomped_v);
    }

    return 0;
}
