/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_KARTE.c
 Example:     gcc -Wall -o prog tri_KARTE.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Recently, Pero has been into robotics, so he decided to make a robot that
 checks whether a deck of poker cards is complete.

 He’s already done a fair share of work—he wrote a programme that recognizes
 the suits of the cards. For simplicity’s sake, we can assume that all cards
 have a suit and a number.

 The suit of the card is one of the characters P, K, H, T, and the number of
 the card is an integer between 1 and 13. The robot labels each card in the
 format TXY where T is the suit and XY is the number. If the card’s number
 consists of one digit, then X=0. For example, the card of suit P and number
 9 is labelled P09.

 A complete deck has 52 cards in total—for each of the four suits there is
 exactly one card with a number between 1 and 13.

 The robot has read the labels of all the cards in the deck and combined
 them into the string S. Help Pero finish the robot by writing a programme
 that reads the string made out of card labels and outputs how many cards
 are missing for each suit. If there are two exact same cards in the deck,
 output GRESKA (Croatian for ERROR).

 Input:
    * The first and only line of input contains the string S (1≤|S|≤1000),
      containing all the card labels.

 Output:
    * If there are two exact same cards in the deck, output “GRESKA”.
      Otherwise, the first and only line of output must consist of 4
      space-separated numbers: how many cards of the suit P, K, H, T are
      missing, respectively.

 Example:
    Input:
    P01K02H03H04

    Output:
    12 12 11 13

    Input:
    H02H10P11H02

    Output:
    GRESKA

    Input:
    P10K10H10T01

    Output:
    12 12 12 12

****************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define MAX_SUIT    13      // max number of suit
#define MAX_DECK    52      // max cards in a deck
#define MAX_STR     1001    // max chars in input string including null-char
#define CARD_INFO   4       // max chars in card info including null-char

// -------------------
// Definition of data
// -------------------

/**
 @struct Card data structure.

 @field info   The info of the card.
 */
typedef struct Card {
    char info[CARD_INFO];
} Card;


// --------------------------------
// Declaration of private functions
// --------------------------------

static bool IsCardExisted(Card *, Card);
static void AddCard(Card *, Card);
static int NumberOfCardsOfSpecificSuit(Card *, char);

// -------------
// Main function
// -------------

int main () {
    Card deck[MAX_DECK] = {0};  // array of cards in a deck
    char str[MAX_STR];          // array of input string
    int str_len;                // lenght of input string

    scanf("%s", str);           // read input string
    str_len = strlen(str);      // calculate lenght of input string

    // if input string has less than 52 cards
    if (str_len/3 <= MAX_DECK) {

        for (int i = 0; i < str_len; i += 3) {
            Card curr_card = {.info = {0}};     // current card

            strncpy(curr_card.info, str+i, 3);  // get info of current card

            if (IsCardExisted(deck, curr_card)) {
                printf("GRESKA\n");
                return 0;
            }

            AddCard(deck, curr_card);
        }

        // missing P-suit cards
        int pm = MAX_SUIT - NumberOfCardsOfSpecificSuit(deck, 'P');
        // missing K-suit cards
        int km = MAX_SUIT - NumberOfCardsOfSpecificSuit(deck, 'K');
        // missing H-suit cards
        int hm = MAX_SUIT - NumberOfCardsOfSpecificSuit(deck, 'H');
        // missing T-suit cards
        int tm = MAX_SUIT - NumberOfCardsOfSpecificSuit(deck, 'T');

        printf("%d %d %d %d\n", pm, km, hm, tm);

    // otherwise input string has more than 52 cards
    } else {
        // there must be at least two duplicate cards
        printf("GRESKA\n");
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Check if a card is existed in a deck

 @param deck The array of card for reference.
 @param card The card to check for existence.

 @return true if the card is existed in an array,
 otherwise false.
 */
static bool IsCardExisted(Card *deck, Card card) {
    for (int i = 0; i < MAX_DECK; ++i) {
        if (strcmp(deck[i].info, card.info) == 0) {
            return true;
        }
    }

    return false;
}

/**
 Add a card to a deck

 @param deck The deck to add.
 @param card  The added card.
 */
static void AddCard(Card *deck, Card card) {
    for (int i = 0; i < MAX_DECK; ++i) {
        if (strlen(deck[i].info) == 0) {
            deck[i] = card;
            return;
        }
    }
}

/**
 Count number of cards of specific suit
 to a deck.

 @param deck The array to count cards.

 @return The number of missing deck.
 */
static int NumberOfCardsOfSpecificSuit(Card *deck, char suit) {
    int count = 0;

    for (int i = 0; i < MAX_DECK; ++i) {
        if (deck[i].info[0] == suit) {
            count++;
        }
    }

    return count;
}
