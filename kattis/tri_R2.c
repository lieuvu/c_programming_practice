/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tr_R2.c
 Example:     gcc -Wall -o prog tr_R2.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.2

 The number S is called the mean of two numbers R1 and R2 if S is equal to
 (R1+R2)/2. Mirko’s birthday present for Slavko was two integers R1 and R2.
 Slavko promptly calculated their mean which also happened to be an integer
 but then lost R2! Help Slavko restore R2.

 Input:
    * The first and only line of input contains two integers R1 and S,
      both between −1000 and 1000.

 Output:
    * Output R2 on a single line.

 Example:
    Input:
    11 15

    Output:
    19

    Input:
    4 3

    Output:
    2

****************************************************************************/

#include <stdio.h>

int main () {
    int r1; // number R1
    int s;  // number s

    scanf("%d %d", &r1, &s);

    printf("%d", (2*s)-r1);

    return 0;
}