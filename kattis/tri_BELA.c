/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_BELA.c
 Example:     gcc -Wall -o prog tri_BELA.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Young Mirko is a smart, but mischievous boy who often wanders around parks
 looking for new ideas. This time he’s come across pensioners playing the
 card game Belote. They’ve invited him to help them determine the total
 number of points in a game.

 Each card can be uniquely determined by its number and suit. A set of four
 cards is called a hand. At the beginning of a game one suit that “trumps”
 any other is chosen, and it is called the dominant suit. The number of
 points in a game is equal to the sum of values of each card from each hand
 in the game. Mirko has noticed that the pensioners have played N hands and
 that suit B was the dominant suit.

 The value of each card depends on its number and whether its suit is
 dominant, and is given in Table 1.

    Number    |         Value
              | Dominant  |  Not dominant
              |           |
      A       |    11     |     11
      K       |     4     |      4
      Q       |     3     |      3
      J       |    20     |      2
      T       |    10     |     10
      9       |    14     |      0
      8       |     0     |      0
      7       |     0     |      0

        Table 1: Scores

 Write a programme that will determine and output the number of points in
 the game.

 Input:
    * The first line contains the number of hands N (1≤N≤100) and the value
      of suit B (S, H, D, C) from the task. Each of the following 4N lines
      contains the description of a card (the first character is the number
      of the i-th card (A, K, Q, J, T, 9, 8, 7), and the second is the suit
      (S, H, D, C)).

 Output:
    * The first and only line of output must contain the number of points
      from the task.

 Example:
    Input:
    2 S
    TH
    9C
    KS
    QS
    JS
    TD
    AD
    JH

    Output:
    60

    Input:
    4 H
    AH
    KH
    QH
    JH
    TH
    9H
    8H
    7H
    AS
    KS
    QS
    JS
    TS
    9S
    8S
    7S

    Output:
    92

****************************************************************************/

#include <stdio.h>

#define MAX_CHAR 2   // max two characters of card description
#define MAX_CARD 8   // max number of cards
#define HAND     4   // number of cards per hand

// -------------------
// Definition of data
// -------------------

/**
 @struct Card data structure

 @field num    The card number.
 @field p_domt The point dominant.
 @field p      The point not dominant.
 */
typedef struct Card {
    char num;
    int p_domt;
    int p;
} Card;

// --------------------------------
// Declaration of private functions
// --------------------------------

static int LookupCard(Card *, char *, char);

// -------------
// Main function
// -------------

int main () {
    // initialize cards
    Card cards[] = { {'A', 11, 11}, {'K', 4, 4}, {'Q', 3, 3},
                     {'J', 20, 2}, {'T', 10, 10}, {'9', 14, 0},
                     {'8', 0, 0}, {'7', 0, 0} };

    int h_num;       // number of hands
    char d_suit;     // dominant suit
    int sum = 0;   // sum of points

    scanf("%d %c", &h_num, &d_suit);

    for (int i = 0; i < HAND*h_num; ++i) {
        char card_des[MAX_CHAR] = {0};  // card description
        scanf("%s", card_des);
        sum += LookupCard(cards, card_des, d_suit);
    }

    printf("%d\n", sum);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Look up a card base on card description

 @param cards    The array of cards.
 @param card_des The card description.
 @param d_suit   The dominant suit.

 @return The point of the card.
 */
static int LookupCard(Card cards[], char *card_des, char d_suit) {
    char card_num = card_des[0];
    char card_suit = card_des[1];
    int point = -1;

    for (int i = 0; i < MAX_CARD; ++i) {
        if (cards[i].num == card_num) {
            if (card_suit == d_suit ) {
                point = cards[i].p_domt;
            } else {
                point = cards[i].p;
            }
        }
    }

    return point;
}