/***************************************************************************
 VAuthor: Lieu vu

 Compilation: gcc -Wall -o <output_file> tri_MODULO.c
 Example:     gcc -Wall -o prog tri_MODULO.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 Given two integers A and B, A modulo B is the remainder when dividing A by
 B. For example, the numbers 7, 14, 27 and 38 become 1, 2, 0 and 2, modulo
 3. Write a program that accepts 10 numbers as input and outputs the number
 of distinct numbers in the input, if the numbers are considered modulo 42.

 Input:
    * The input will contain 10 non-negative integers, each smaller than
      1000, one per line.

 Output:
    * Output the number of distinct values when considered modulo 42 on a
      single line.

 Example:
    Input:
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10

    Output:
    10

    Input.
    2
    84
    252
    420
    840
    126
    42
    84
    420
    126

    Output.
    1

    Input:
    39
    40
    41
    42
    43
    44
    82
    83
    84
    85

    Output:
    6

 Explanation.
    * In sample input 1, the numbers modulo 42 are 1,2,3,4,5,6,7,8,9 and 10.
    * In sample input 2, all numbers modulo 42 are 0.
    * In sample input 3, the numbers modulo 42 are 39,40,41,0,1,2,40,41,0
    and 1. There are 6 distinct numbers.

****************************************************************************/

#include <stdio.h>

#define NUM_TEST    10
#define MOD         42

int main () {
    int unique_num[MOD];       // an array to mark unique numbers after modulo
    int count = 0;             // count unique numbers

    // set all mark to -1
    for (int i = 0; i < MOD; ++i) {
        unique_num[i] = -1;
    }

    for (int i = 0; i < NUM_TEST; ++i) {
        int num;                            // check number
        int re;                             // remainder

        scanf("%d", &num);
        re = num % MOD;

        // if the remainder is not marked
        if (unique_num[re] == -1) {
            unique_num[re] = 1;         // mark that remainder
            count++;                    // count the unique number
        }
    }

    printf("%d", count);

    return 0;
}