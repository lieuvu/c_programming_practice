/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_MIRROR.c
 Example:     gcc -Wall -o prog tri_MIRROR.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.4

 Atrebla is developing a new smartphone camera app that she believe will be
 the next big hit on the app store. Camera apps usually display the image
 mirrored left-to-right on the screen, so that what you see on the screen
 matches the movement of your face. To help her camera app stand out from
 the rest, Atrebla will be adding a unique effect that mirrors an image both
 left-to-right and top-to-bottom. Help her implement this feature.

 Input:
    * The first line contains a single integer T≤100 giving the number of
      test cases. Each test case starts with a line containing an integer R
      (1≤R≤20), the number of rows in the image, and an integer C (1≤C≤20),
      the number of columns. The next R lines contain C characters, all of
      which are either . or *.

 Output:
    * For each test case, output a line containing “Test x” where x is the
      test case number starting from 1. After that, output the mirrored image.

 Example:
    Input:
    2
    2 2
    .*
    ..
    4 4
    ***.
    **..
    ....
    ....

    Output:
    Test 1
    ..
    *.
    Test 2
    ....
    ....
    ..**
    .***

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_R   20      // max number of rows
#define MAX_C   21      // max number of column (1 extra to hold null char)

// --------------------------------
// Declaration of private functions
// --------------------------------

static void MirrorImage(char [][MAX_C], int, int);

// -------------
// Main function
// -------------

int main () {
    int test_num;               // test number
    int row;                    // number of row
    int col;                    // number of column
    int case_num = 0;           // test case number

    scanf("%d", &test_num);

    while (test_num) {
        char img[MAX_R][MAX_C] = {0};     // the input image

        // read number of row and column
        scanf("%d %d", &row, &col);

        // read input image
        for (int i = 0; i < row; ++i) {
            scanf("%s", img[i]);
        }

        MirrorImage(img, row, col); // mirror the image
        case_num++;                 // increase case number

        // print result
        printf("Test %d\n", case_num);
        for (int i = 0; i < row; ++i) {
            printf("%s\n", img[i]);
        }

        test_num -= 1;  // decrease test number
    }

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Mirror the image

 @param img The image to mirror.
 @param row The number of row in the image.
 @param col The number of column in the image.
 */
static void MirrorImage(char img[][MAX_C], int row, int col) {
    char mirrored_img[MAX_R][MAX_C] = {0};

    // form the mirroed image
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            mirrored_img[i][j] = img[row-1-i][col-1-j];
        }
    }

    // copy mirrored image to image
    for (int i = 0; i < row; ++i) {
        strcpy(img[i], mirrored_img[i]);
    }
}
