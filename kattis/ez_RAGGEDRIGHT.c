/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_RAGGEDRIGHT.c
 Example:     gcc -Wall -o prog ez_RAGGEDRIGHT.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 Word wrapping is the task of deciding how to break a paragraph of text into
 lines. For aesthetic reasons, we’d like all the lines except the last one
 to be about the same length. For example, we would say the text on the left
 looks less ragged than the text on the right:

             This is a                 This
             paragraph                 is a paragraph
             of text.                  of text.

 Your job is to compute a raggedness value for an arbitrary paragraph of
 text. We’ll measure raggedness in a way similar to the TeX  typesetting
 system. Let n be the length, measured in characters, of the longest line of
 the paragraph. If some other line contains only m characters, then we’ll
 charge a penalty score of (n−m)² for that line. The raggedness will be the
 sum of the penalty scores for every line except the last one.

 Input:
    * Input consists of a single paragraph of text containing at most 100
      lines. Each line of the paragraph contains a sequence of between 1 and
      80 characters (letters, punctuation characters, decimal digits and
      spaces). No line starts or ends with spaces. The paragraph ends at end
      of file.

 Output:
    * Print out a single integer, the raggedness score for paragraph.

 Example:
    Input:
    some blocks
    of text line up
    well on the right,
    but
    some don't.

    Output:
    283

    Input:
    this line is short
    this one is a bit longer
    and this is the longest of all.

    Output:
    218

****************************************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_LINE    100     // max lines
#define MAX_CHAR    81      // max char of one line (null-terminating string)

int main () {
    int pernalties[MAX_LINE] = {0};  // array of penalties
    char line[MAX_CHAR] = {0};      // string of line
    int max_str_len = 0;            // the max string lenght of all line
    int count = 0;                  // number of lines
    int rag = 0;                    // raggedness

    while (fgets(line, MAX_CHAR, stdin)) {
        int len = strlen(line);

        // trimming new line char
        if (line[len-1] == '\n') {
            line[len-1] = '\0';
        }

        pernalties[count] = strlen(line);

        if (max_str_len < pernalties[count]) {
            max_str_len = pernalties[count];
        }

        count++;
    }

    // go from the first line to the line before
    // last line
    for (int i = 0; i < count-1; ++i) {
        rag += (max_str_len - pernalties[i]) * (max_str_len - pernalties[i]);
    }

    printf("%d\n", rag);

    return 0;
}
