/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_PET.c
 Example:     gcc -Wall -o prog tri_PET.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 In the popular show “Dinner for Five”, five contestants compete in
 preparing culinary delights. Every evening one of them makes dinner
 and each of other four then grades it on a scale from 1 to 5. The
 number of points a contestant gets is equal to the sum of grades
 they got. The winner of the show is of course the contestant that
 gets the most points.

 Write a program that determines the winner and how many points they got.

 Input:
    * Five lines, each containing 4 integers, the grades a contestant got.
      The contestants are numbered 1 to 5 in the order in which their grades
      were given.

 Output:
    * Output on a single line the winner’s number and their points,
      separated by a space. The input data will guarantee that the solution
      is unique.

 Example:
    Input:
    5 4 4 5
    5 4 4 4
    5 5 4 4
    5 5 5 4
    4 4 4 5

    Output:
    4 19

    Input:
    4 4 3 3
    5 4 3 5
    5 5 2 4
    5 5 5 1
    4 4 4 4

    Output:
    2 17

****************************************************************************/

#include <stdio.h>

#define MAX_R   5   // max row
#define MAX_C   4   // max column

int main () {
    int max_points = 0;                      // max point of all contestants
    int idx_winner = 0;                     // the index of the winner

    for (int i = 0; i < MAX_R; ++i) {
        int c_points = 0;                    // grade of contestant

        for (int j = 0; j < MAX_C; ++j) {
            int grade;
            scanf("%d", &grade);
            c_points += grade;
        }

        if (max_points < c_points) {
            max_points = c_points;
            idx_winner = i;
        }
    }

    printf("%d %d", (idx_winner +1), max_points);

    return 0;
}