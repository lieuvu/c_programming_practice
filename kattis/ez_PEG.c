/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_PEG.c
 Example:     gcc -Wall -o prog ez_PEG.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 In the famous logic game Peg, pieces jump over other pieces to remove them
 from the game, until only one piece is left.

 Here is the initial layout of the board:

                            ooo
                            ooo
                          ooooooo
                          ooo.ooo
                          ooooooo
                            ooo
                            ooo

 The lowercase letter ’o’ represents a piece, while the character ’.’ is an
 empty square. In one move, a player may choose one piece and one of the
 four main directions (up, down, left, right), if there is another piece in
 that direction, and an empty square behind it. The chosen piece jumps over
 the other piece and settles in the empty square behind it, while the piece
 being jumped over is removed from the game.

 Write a program that calculates the number of legal moves, given the state
 of the board.

 Input:
    * The board is represented by seven lines containing seven characters
      each. The first two and last two characters on the first two and last
      two lines are always spaces, and all remaining characters are either
      ’o’ (lowercase letter) or ’.’ (period character).

 Output:
    * Output the number of legal moves.

 Example:
    Input:
      ooo
      ooo
    ooooooo
    ooo.ooo
    ooooooo
      ooo
      ooo

    Output:
    4

    Input:
      ooo
      ooo
    ..ooo..
    oo...oo
    ..ooo..
      ooo
      ooo

    Output:
    12

****************************************************************************/

#include <stdio.h>

#define MAX_PEG     7      // max pegs
#define PEG         1      // the peg
#define EMPTY       0      // the empty square
#define SPACE       -1     // the space

int main () {
    int board[MAX_PEG][MAX_PEG] = {0};  // the board
    int moves = 0;                      // number of moves

    // read the board
    for (int i = 0 ; i < MAX_PEG; ++i) {
        char tmp;
        for (int j = 0; j < MAX_PEG; ++j) {
            scanf("%c", &tmp);
            if (tmp == ' ') {
                board[i][j] = SPACE;
            } else if (tmp == 'o') {
                board[i][j] = PEG;
            } else if (tmp == '.') {
                board[i][j] = EMPTY;
            }
        }
        scanf("%c", &tmp);
    }

    // check four directions of each empty board cell
    for (int i = 0; i < MAX_PEG; ++i) {
        for (int j = 0; j < MAX_PEG; ++j) {
            if (board[i][j] == EMPTY) {
                // check left direction
                if (j >= 2) {
                    if (board[i][j-1] == PEG && board[i][j-2] == PEG) {
                        moves++;
                    }
                }

                // check right direction
                if (j <= 4) {
                    if (board[i][j+1] == PEG && board[i][j+2] == PEG) {
                        moves++;
                    }
                }

                // check up direction
                if (i >= 2) {
                    if (board[i-1][j] == PEG && board[i-2][j] == PEG) {
                        moves++;
                    }
                }

                // check down direction
                if (i <= 4) {
                    if (board[i+1][j] == PEG && board[i+2][j] == PEG) {
                        moves++;
                    }
                }
            }
        }
    }

    printf("%d\n", moves);

    return 0;
}
