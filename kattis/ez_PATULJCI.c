/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_PALTUJCI.c
 Example:     gcc -Wall -o prog ez_PALTUJCI.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.5

 Every day, while the dwarves are busy in the mines, Snow White prepares
 dinner for them; seven chairs, seven plates, seven forks and seven knives
 for seven hungry dwarves.

 One day nine dwarves came from the mines instead of seven (nobody knows how
 or why), each of them claiming to be one of Snow White’s seven dwarves.

 Luckily, each dwarf wears a hat with a positive integer less than 100
 written on it. Snow White, a famous mathematician, realised long ago that
 the sum of numbers on the hats of her seven dwarves was exactly 100.

 Write a program which determines which dwarves are legit, i.e., pick seven
 of nine numbers that add to 100.

 Input:
    * There are 9 lines of input. Each contains an integer between 1 and 99
      (inclusive). All of the numbers will be distinct.
    * The test data will be such that there exists a unique solution.

 Output:
    * Your program must produce exactly seven lines of output – the numbers
      on the hats of Snow White’s seven dwarves. Output the numbers in the
      same order they are given in the input.

 Example:
    Input:
    7
    8
    10
    13
    15
    19
    20
    23
    25

    Output:
    7
    8
    10
    13
    19
    20
    23

    Input:
    8
    6
    5
    1
    37
    30
    28
    22
    36

    Output:
    8
    6
    5
    1
    30
    28
    22

****************************************************************************/

#include <stdio.h>

#define MAX_IN          9      // max number of input
#define VALID_SUM     100      // valid sum of hats

int main () {
    int hats[MAX_IN] = {0};     // hats of dwarves
    int sum = 0;                // sum of hats

    // read hat numbers and calculate the sum
    // of all hats
    for (int i = 0; i < MAX_IN; ++i) {
        scanf("%d", hats+i);
        sum += hats[i];
    }

    // go through hats and form
    // the combination of two distinct hats
    for (int i = 0; i < MAX_IN-1; ++i) {
        for (int j = i+1; j < MAX_IN; ++j) {
            // if the sum after deducting
            // two selected hats equals VALID_SUM
            if (sum - hats[i] - hats[j] == VALID_SUM) {
                // go through hats
                for (int k = 0; k < MAX_IN; ++k) {
                    // if the hat is different from
                    // two selected hat
                    if ( k != i && k != j) {
                        printf("%d\n", hats[k]);    // print the hat
                    }
                }
            }
        }
    }

    return 0;
}
