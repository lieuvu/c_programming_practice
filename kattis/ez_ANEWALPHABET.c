/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> ez_ANEWALPHABET.c
 Example:     gcc -Wall -o prog ez_ANEWALPHABET.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.6

 A New Alphabet has been developed for Internet communications. While the
 glyphs of the new alphabet don’t necessarily improve communications in any
 meaningful way, they certainly make us feel cooler.

 You are tasked with creating a translation program to speed up the switch
 to our more elite New Alphabet by automatically translating ASCII plaintext
 symbols to our new symbol set.

 The new alphabet is a one-to-many translation (one character of the English
 alphabet translates to anywhere between 1 and 6 other characters), with
 each character translation as follows:

 Original   |    New    |   English Description
    a       |     @     |   at symbol
    b       |     8     |   digit eight
    c       |     (     |   open parenthesis
    d       |     |)    |   bar, close parenthesis
    e       |     3     |   digit three
    f       |     #     |   number sign (hash)
    g       |     6     |   digit six
    h       |    [-]    |   bracket, hyphen, bracket
    i       |     |     |   bar
    j       |     _ |   |   underscore, bar
    k       |     |<    |   bar, less than
    l       |     1     |   digit one
    m       |   []\/[]  |   brackets, slashes, brackets
    n       |   []\[]   |   brackets, backslash, brackets
    o       |     0     |   digit zero
    p       |     |D    |   bar, capital D
    q       |    (,)    |   parenthesis, comma, parenthesis
    r       |    |Z     |   bar, capital Z
    s       |    $      |   dollar sign
    t       |    ']['   |   quote, brackets, quote
    u       |    |_|    |   bar, underscore, bar
    v       |    \/     |   backslash, forward slash
    w       |   \/\/    |   four slashes
    x       |    }{     |   curly braces
    y       |    `/     |   backtick, forward slash
    z       |    2      |   digit two

 For instance, translating the string “Hello World!” would result in:

                    [-]3110 \/\/0|Z1|)!

 Note that uppercase and lowercase letters are both converted, and any other
 characters remain the same (the exclamation point and space in this
 example).

 Input:
    * Input contains one line of text, terminated by a newline. The text may
      contain any characters in the ASCII range 32–126 (space through
      tilde), as well as 9 (tab). Only characters listed in the above table
      (A–Z, a–z) should be translated; any non-alphabet characters should be
      printed (and not modified). Input has at most 10000 characters.

 Output:
    * Output the input text with each letter (lowercase and uppercase)
      translated into its New Alphabet counterpart.

 Example:
    Input:
    All your base are belong to us.

    Output:
    @11 `/0|_||Z 8@$3 @|Z3 8310[]\[]6 ']['0 |_|$.

    Input:
    What's the Frequency, Kenneth?

    Output:
    \/\/[-]@'][''$ ']['[-]3 #|Z3(,)|_|3[]\[](`/, |<3[]\[][]\[]3']['[-]?

    Input:
    A new alphabet!

    Output:
    @ []\[]3\/\/ @1|D[-]@83']['!

****************************************************************************/

#include <stdio.h>
#include <ctype.h>

#define MAX_CHAR_NEW_ALPHABET   7       // max char in new alphabet
#define MAX_ENTRY               26      // max entry in new alphabet dictionary
#define MAX_CHAR                10001   // max char in input

// ------------------
// Definition of data
// ------------------

/**
 @struct Entry data structure

 @field c      The original character.
 @field new_c  The new character.
 */
typedef struct Entry {
    char c;
    char new_c[MAX_CHAR_NEW_ALPHABET];
} Entry;

// ----------------
// Global constants
// ----------------

static const Entry dict[MAX_ENTRY] = { {'a',"@"}, {'b',"8"}, {'c',"("},
                                       {'d',"|)"}, {'e',"3"}, {'f',"#"},
                                       {'g',"6"}, {'h', "[-]"}, {'i', "|"},
                                       {'j',"_|"}, {'k',"|<"}, {'l',"1"},
                                       {'m',"[]\\/[]"}, {'n', "[]\\[]"},
                                       {'o',"0"}, {'p',"|D"}, {'q',"(,)"},
                                       {'r',"|Z"}, {'s',"$"}, {'t',"']['"},
                                       {'u',"|_|"}, {'v',"\\/"},
                                       {'w',"\\/\\/"}, {'x',"}{"}, {'y',"`/"},
                                       {'z',"2"} };

// --------------------------------
// Declaration of private functions
// --------------------------------

static void TranslateToNewAlphabet(const char *);
static const char * LookupNewAlphabet(char);

// -------------
// Main function
// -------------

int main () {
    char input[MAX_CHAR] = {0};

    fgets(input, MAX_CHAR, stdin);

    TranslateToNewAlphabet(input);

    return 0;
}

// -----------------------------------
// Implementation of private functions
// -----------------------------------

/**
 Translate the input string to new alphabet
 string

 @param input The input string.
 */
static void TranslateToNewAlphabet(const char *input) {
    int i = 0;

    while (input[i]) {
        /* if not alphabetic */
        if (!isalpha(input[i])) {
            printf("%c",input[i]);

        /* otherwise is alphabetic */
        } else {
            printf("%s", LookupNewAlphabet(input[i]));
        }

        i++;
    }
}

/**
 Look up new alphabet in the dictionary for the
 given character.

 @param c The character to look up.

 @return The new alphabet in the dictionary.
 */
static const char * LookupNewAlphabet(char c) {
    /* if character is upper case */
    if (isupper(c)) {
        c = tolower(c);
    }

    /* go through each entry in the dictionary */
    int i;
    for (i = 0; i < MAX_ENTRY; ++i) {
        /* if the entry is found */
        if (dict[i].c == c) {
            break;  // stop finding
        }
    }

    return dict[i].new_c;
}
