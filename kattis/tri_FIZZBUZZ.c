/***************************************************************************
 Author: Lieu Vu

 Compilation: gcc -Wall -o <output_file> tri_FIZZBUZZ.c
 Example:     gcc -Wall -o prog tri_FIZZBUZZ.c

 Execution:  <output_file>
 Example:    prog

 Problem Source: kattis

 Difficulty: 1.3

 According to Wikipedia, FizzBuzz is a group word game for children to teach
 them about division. This may or may not be true, but this question is
 generally used to torture screen young computer science graduates during
 programming interviews.

 Basically, this is how it works: you print the integers from 1 to N,
 replacing any of them divisible by X with Fizz or, if they are divisible
 by Y, with Buzz. If the number is divisible by both X and Y, you print
 FizzBuzz instead.

 Check the samples for further clarification.

 Input:
    * Input file will contain a single test case. Each test case will
      contain three integers on a single line, X, Y and N (1≤X<Y≤N≤100).

 Output:
    * Print integers from 1 to N in order, each on its own line, replacing
      the ones divisible by X with Fizz, the ones divisible by Y with Buzz
      and ones divisible by both X and Y with FizzBuzz.

 Example:
    Input:
    2 3 7

    Output:
    1
    Fizz
    Buzz
    Fizz
    5
    FizzBuzz
    7

    Input:
    2 4 7

    Output:
    1
    Fizz
    3
    FizzBuzz
    5
    Fizz
    7

    Input:
    3 5 7

    Output:
    1
    2
    Fizz
    4
    Buzz
    Fizz
    7

****************************************************************************/

#include <stdio.h>

#define FZ  "Fizz"
#define BZ  "Buzz"
#define FBZ "FizzBuzz"

int main () {
    int x;     // number x
    int y;     // number y
    int n;     //  number n

    scanf("%d %d %d", &x, &y, &n);

    for (int i = 1; i <= n; ++i) {
        if (i % x == 0 && i % y == 0) {
            printf("%s\n", FBZ);
        } else if (i % x == 0) {
            printf("%s\n", FZ);
        } else if (i % y == 0) {
            printf("%s\n", BZ);
        } else {
            printf("%d\n", i);
        }
    }

    return 0;
}